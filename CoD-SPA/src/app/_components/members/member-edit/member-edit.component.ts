import { Component, OnInit, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AccountService } from 'src/app/_services/account.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Account } from 'src/app/_models/account';
import { AuthService } from 'src/app/_services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateAccountModel } from 'src/app/_models/update-account-model';
import { AccountForUpdate } from 'src/app/_models/account-for-update';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { GroupService } from 'src/app/_services/group.service';
import { CreateGroupModel } from 'src/app/_models/create-group-model';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  isPromoter: boolean;
  isStudent: boolean;
  canCreateGroup: boolean;
  creatingGroup: boolean;
  account: Account;
  issueSemesters: any[];
  studyFields: any[];

  createGroupForm: FormGroup;
  memberEdit: FormGroup;
  submitted = false;

  issueSemestersDropdownList = [];
  issueSemestersSelectedItems = [];

  studyFieldsDropdownList = [];
  studyFieldsSelectedItems = [];

  dropdownSettings = {};

  constructor(
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private authService: AuthService,
    private groupService: GroupService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.isPromoter = this.authService.isPromoter();
    this.isStudent = this.authService.isStudent();

    this.memberEdit = this.formBuilder.group(
      {
        name: ['', Validators.required],
        secondName: [''],
        surname: ['', Validators.required],
        indexNumber: [''],
        scienceTitle: [''],
        studyFields: [''],
        issueSemesters: ['']
      },
      {}
    );

    this.createGroupForm = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
      },
      {
        validator: MustMatch('password', 'confirmPassword')
      }
    );

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.route.data.subscribe(data => {
      this.account = data.accountData.account;
      this.canCreateGroup = data.accountData.account.canCreateGroup;
      this.issueSemesters = data.issueSemesters;
      this.studyFields = data.studyFieldsWithFacultiesInfos;
    });

    this.issueSemestersDropdownList = this.issueSemesters;
    this.studyFieldsDropdownList = this.fixStudyFields();

    this.memberEdit.patchValue({
      name: this.account.name,
      secondName: this.account.secondName,
      surname: this.account.surname,
      indexNumber: this.account.indexNumber,
      scienceTitle: this.account.scienceTitle,
      studyFields: this.fixStudyFields()
    });

    this.canCreateGroup = true;
  }

  createGroup() {
    this.submitted = true;

    if (this.createGroupForm.invalid) {
      return;
    }

    const createGroupModel: CreateGroupModel = {
      accountId: this.authService.decodedToken.nameid,
      password: this.createGroupForm.value.password,
      confirmPassword: this.createGroupForm.value.confirmPassword
    };

    this.groupService.createGroup(createGroupModel).subscribe(
      next => {
        this.createGroupForm.reset(this.createGroupForm.value);
        this.alertify.success('Grupa została założona!');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
        window.location.reload();
      }
    );
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  updateAccount() {
    this.submitted = true;

    if (this.memberEdit.invalid) {
      return;
    }

    const accountForUpdate: AccountForUpdate = {
      name: this.memberEdit.value.name,
      secondName: this.memberEdit.value.secondName,
      surname: this.memberEdit.value.surname,
      scienceTitle: this.memberEdit.value.scienceTitle,
      indexNumber: this.memberEdit.value.indexNumber
    };

    const updateAccountModel: UpdateAccountModel = {
      accountId: this.authService.decodedToken.nameid,
      accountForUpdate,
      issueSemesterInfos: this.memberEdit.value.issueSemesters,
      studyFieldWithFacultyInfos: this.memberEdit.value.studyFields
    };

    this.accountService.updateAccount(updateAccountModel).subscribe(
      next => {
        this.memberEdit.reset(this.account); // nie jest brudny itp
        this.alertify.success('Dane zostały zaaktualizowane!');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
        window.location.reload();
      }
    );
  }

  private fixStudyFields(): any[] {
    return this.studyFields.map(studyField => {
      return { id: studyField.id, name: studyField.studyFieldWithFacultyName };
    });
  }

  get f() {
    return this.memberEdit.controls;
  }

  get g() {
    return this.createGroupForm.controls;
  }
}
