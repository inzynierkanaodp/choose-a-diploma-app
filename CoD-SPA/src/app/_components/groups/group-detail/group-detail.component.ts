import { Component, OnInit } from '@angular/core';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AccountService } from 'src/app/_services/account.service';
import { AuthService } from 'src/app/_services/auth.service';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { GroupService } from 'src/app/_services/group.service';
import { CreateGroupModel } from 'src/app/_models/create-group-model';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {
  showMemeberGroupList = false;
  wantJoinToGroup: boolean;
  canManageGroup = false;
  founderAccountId: number;
  canJoinToGroup = false;
  isStudent = false;
  group: any;
  joinToGroupForm: FormGroup;
  submitted = false;
  currentGroupId: number;
  groupMembers: any;

  constructor(
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private groupService: GroupService
  ) {}

  ngOnInit() {
    this.groupService
      .canJoinGroup(this.authService.decodedToken.nameid)
      .subscribe(data => {
        this.canJoinToGroup = data.canJoinGroup;
      });

    this.currentGroupId = Number(this.route.snapshot.paramMap.get('id'));

    this.groupService.getGroupMembers(this.currentGroupId).subscribe(data => {
      this.groupMembers = data.accounts;
    });

    this.isStudent = this.authService.isStudent();
    this.route.data.subscribe(data => {
      this.group = data.group.groupTable;
    });

    this.joinToGroupForm = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
      },
      {
        validator: MustMatch('password', 'confirmPassword')
      }
    );

    this.groupService
      .isGroupLeader(this.authService.decodedToken.nameid, this.currentGroupId)
      .subscribe(data => {
        this.canManageGroup = data;
        if (this.canManageGroup) {
          this.founderAccountId = this.authService.decodedToken.nameid;
        }
      });
  }

  isFounderGroup(currentAccountId: number) {
    if (this.founderAccountId === currentAccountId) {
      return true;
    }

    return false;
  }

  isFounder(groupMember: any) {
    const fullName = groupMember.name + ' ' + groupMember.surname;

    if (fullName === this.group.founder) {
      return true;
    }
    return false;
  }

  removeMember(groupMember: any) {
    this.groupService.removeGroupMember(groupMember.id).subscribe();
  }

  joinGroup() {
    this.submitted = true;

    if (this.joinToGroupForm.invalid) {
      return;
    }

    const joinGroupModel: any = {
      groupId: this.currentGroupId,
      accountId: this.authService.decodedToken.nameid,
      password: this.joinToGroupForm.value.password,
      confirmPassword: this.joinToGroupForm.value.confirmPassword
    };

    this.groupService.joinGroup(joinGroupModel).subscribe(
      next => {
        this.joinToGroupForm.reset(this.joinToGroupForm.value); // nie jest brudny itp
        this.alertify.success('Dołączono!');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
        window.location.reload();
      }
    );
  }

  archiveGroup() {
    this.alertify.confirm(
      'Pytanie',
      'Czy na pewno chcesz dokonać archiwizacji grupy? Proces ten jest nieodwracalny.',
      () => {
        this.groupService.archiveGroup(this.currentGroupId).subscribe();
      }
    );
  }
}
