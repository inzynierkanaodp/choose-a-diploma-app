import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-edit',
  templateUrl: './login-edit.component.html',
  styleUrls: ['./login-edit.component.css']
})
export class LoginEditComponent implements OnInit {
  @Output() cancelReset = new EventEmitter();

  loginChangeFormGroup: FormGroup;
  emailChangeFormGroup: FormGroup;
  passwordChangeFormGroup: FormGroup;

  submitted = false;

  loginChangeMode = true;
  passwordChangeMode = false;
  emailChangeMode = false;

  account: any;

  constructor(
    private authService: AuthService,
    private alertify: AlertifyService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loginChangeFormGroup = this.formBuilder.group(
      {
        oldLogin: ['', Validators.required],
        newLogin: ['', Validators.required],
        confirmNewLogin: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
      },
      {
        validator: [
          MustMatch('newLogin', 'confirmNewLogin'),
          MustMatch('password', 'confirmPassword')
        ]
      }
    );

    this.emailChangeFormGroup = this.formBuilder.group(
      {
        oldEmail: ['', [Validators.required, Validators.email]],
        newEmail: ['', [Validators.required, Validators.email]],
        confirmNewEmail: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
      },
      {
        validator: [
          MustMatch('newMail', 'confirmNewEmail'),
          MustMatch('password', 'confirmPassword')
        ]
      }
    );

    this.passwordChangeFormGroup = this.formBuilder.group(
      {
        login: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        oldPassword: ['', Validators.required],
        newPassword: ['', [Validators.required, Validators.minLength(6)]],
        confirmNewPassword: ['', [Validators.required, Validators.minLength(6)]]
      },
      {
        validator: MustMatch('newPassword', 'confirmNewPassword')
      }
    );

    this.route.data.subscribe(data => {
      this.account = data.user.account;
    });
  }

  loginChange() {
    this.submitted = true;

    if (this.loginChangeFormGroup.invalid) {
      return;
    }

    this.authService.changeLogin(this.loginChangeFormGroup.value).subscribe(
      () => {
        window.location.reload();
        this.setLoginMode();
        this.alertify.success('Reset successfully');
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  emailChange() {
    this.submitted = true;

    if (this.emailChangeFormGroup.invalid) {
      return;
    }

    this.authService.changeEmail(this.emailChangeFormGroup.value).subscribe(
      () => {
        window.location.reload();
        this.setEmailMode();
        this.alertify.success('Reset successfully');
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  passwordChange() {
    this.submitted = true;

    if (this.passwordChangeFormGroup.invalid) {
      return;
    }

    this.authService.reset(this.passwordChangeFormGroup.value).subscribe(
      () => {
        window.location.reload();
        this.setPasswordMode();
        this.alertify.success('Reset successfully');
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  setLoginMode() {
    this.setFalseValuesAndClearFormStatus();
    this.loginChangeMode = true;
  }

  setEmailMode() {
    this.setFalseValuesAndClearFormStatus();
    this.emailChangeMode = true;
  }

  setPasswordMode() {
    this.setFalseValuesAndClearFormStatus();
    this.passwordChangeMode = true;
  }

  setFalseValuesAndClearFormStatus() {
    this.submitted = false;

    this.loginChangeMode = false;
    this.passwordChangeMode = false;
    this.emailChangeMode = false;
  }

  cancel() {
    this.cancelReset.emit(false);
    this.submitted = false;
  }

  get fLogin() {
    return this.loginChangeFormGroup.controls;
  }

  get fEmail() {
    return this.emailChangeFormGroup.controls;
  }

  get fPassword() {
    return this.passwordChangeFormGroup.controls;
  }
}
