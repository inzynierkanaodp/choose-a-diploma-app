import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { TopicOfWorkService } from 'src/app/_services/topic-of-work.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {

  public columns: Array<any> = [
    {
      title: 'Id Zgłoszenia',
      name: 'id'
      //filtering: { filterString: '', placeholder: 'Sortuj po numerze' }
    },
    {
      title: 'Nazwa użytkownika',
      name: 'ticketOwnerName',
      filtering: { filterString: '', placeholder: 'Sortuj po nazwie' }
    },
    {
      title: 'Uprawnienie',
      name: 'permissionName',
      filtering: { filterString: '', placeholder: 'Sortuj po uprawnieniu' }
    },
    {
      title: 'Tytuł',
      name: 'title',
      filtering: { filterString: '', placeholder: 'Sortuj po tytule' }
    },
    {
      title: 'Status',
      name: 'state',
      filtering: { filterString: '', placeholder: 'Sortuj po statusie' }
    }
  ];
  tickets: any[];
  isPromoter = false;
  quantityOfUnhiddenTopics: number;
  quantityUniqueOfPromoters: number;

  private data: Array<any>;
  public rows: Array<any> = [];

  public page = 1;
  public itemsPerPage = 10;
  public maxSize = 5;
  public numPages = 1;
  public length = 0;

  public config: any = {
    paging: true,
    sorting: { columns: ['ticketOwnerName', 'permissionName', 'title', 'state'] },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.isPromoter = this.authService.isPromoter();

    this.route.data.subscribe(data => {
      this.tickets = data.tickets.ticketTables;
      this.data = this.tickets;
      this.length = this.data.length;

      if (this.length !== undefined) {
        this.onChangeTable(this.config);
      }
    });
  }

  private fixIds(): any[] {
    return this.tickets.map(ticket => {
      return {
        id: ticket.id.toString(),
        tickerOwnerName: ticket.tickerOwnerName,
        permissionName: ticket.permissionName,
        title: ticket.title,
        state: ticket.state
      };
    });
  }

  public onCellClick(data: any): any {
    if (data.row.id !== undefined) {
      this.router.navigate(['adminPanel/ticket/' + data.row.id]);
    }
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(
          this.config.filtering.filterString
        )
      );
    }

    const tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (
          item[column.name].toString().match(this.config.filtering.filterString)
        ) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(
    config: any,
    page: any = { page: this.page, itemsPerPage: this.itemsPerPage }
  ): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    const filteredData = this.changeFilter(this.data, this.config);
    const sortedData = this.changeSort(filteredData, this.config);
    this.rows =
      page && config.paging ? this.changePage(page, sortedData) : sortedData;
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    const start = (page.page - 1) * page.itemsPerPage;
    const end =
      page.itemsPerPage > -1 ? start + page.itemsPerPage : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    const columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (const iterator of columns) {
      columnName = columns.name;
      sort = columns.sort;
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

}
