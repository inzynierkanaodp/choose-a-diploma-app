import { Component, OnInit } from '@angular/core';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { AccountService } from 'src/app/_services/account.service';
import { AuthService } from 'src/app/_services/auth.service';
import { TopicOfWorkService } from 'src/app/_services/topic-of-work.service';
import { HelpService } from 'src/app/_services/help.service';
import { PermissionService } from 'src/app/_services/permission.service';
import { PermissionName } from 'src/app/_models/_enums/permission-name.enum';
import { SetPermissionAccountModel } from 'src/app/_models/set-permission-account-model';
import { ChangeTicketStatusModel } from 'src/app/_models/change-ticket-status-model';
import { TicketStatus } from 'src/app/_models/_enums/ticket-status.enum';
import { TicketSaveModel } from 'src/app/_models/ticket-save-model';

@Component({
  selector: 'app-tickets-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit {
  ticket: any;
  dateObj: Date;
  date: string;
  permissionName = PermissionName;
  ticketStatus = TicketStatus;
  currentTicketId: number;

  constructor(
    private route: ActivatedRoute,
    private accountService: AccountService,
    private helpService: HelpService,
    private authService: AuthService,
    private permissionService: PermissionService,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.currentTicketId = Number(this.route.snapshot.paramMap.get('id'));
    this.permissionName = PermissionName;
    this.ticketStatus = TicketStatus;

    this.route.data.subscribe(data => {
      this.ticket = data.ticket.ticketTables[0];
    });

    this.dateObj = new Date(Date.parse(this.ticket.ticketDateTime));

    this.date =
      this.dateObj.getFullYear() +
      '/' +
      (this.dateObj.getMonth() + 1) +
      '/' +
      this.dateObj.getUTCDate();
  }

  changeTicketStatus(ticketStatus: TicketStatus) {
    const changeTicketStatusModel: ChangeTicketStatusModel = {
      ticketId: Number(this.route.snapshot.paramMap.get('id')),
      ticketStatus
    };

    this.helpService.changeTicketStatus(changeTicketStatusModel).subscribe(
      next => {
        this.alertify.success('Dane zostały zaaktualizowane!');
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  addPermissionToAccount(permissioneName: PermissionName) {
    const setPermissionAccountModel: SetPermissionAccountModel = {
      ticketId: this.currentTicketId,
      permissionName: permissioneName
    };

    this.permissionService.setPermissionAccount(setPermissionAccountModel).subscribe(
      next => {
        this.alertify.success('Dane zostały zaaktualizowane!');
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  activeAccount() {

    const activeAccountModel: any = {
      ticketId: this.currentTicketId
    };

    this.accountService.activeAccount(activeAccountModel).subscribe(
      next => {
        this.alertify.success('Dane zostały zaaktualizowane!');
      },
      error => {
        this.alertify.error(error);
      }
    );
  }
}
