import { Component, OnInit } from '@angular/core';
import { TopicOfWorkService } from 'src/app/_services/topic-of-work.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-thesis',
  templateUrl: './thesis.component.html',
  styleUrls: ['./thesis.component.css']
})
export class ThesisComponent implements OnInit {
  public columns: Array<any> = [
    {
      title: 'Zajęty',
      name: 'isReserved'
    },
    {
      title: 'Nazwa tematu',
      name: 'thesisName',
      filtering: { filterString: '', placeholder: 'Sortuj po nazwie' }
    },
    {
      title: 'Promotor',
      name: 'supervisorName',
      filtering: { filterString: '', placeholder: 'Sortuj po promotrze' }
    },
    {
      title: 'Max ilość osób',
      name: 'maxSize',
      sort: 'asc',
      filtering: { filterString: '', placeholder: 'Sortuj po ilości max osób' }
    },
    {
      title: 'Wydział',
      name: 'faculty',
      sort: '',
      filtering: { filterString: '', placeholder: 'Sortuj po wydziale' }
    }
  ];
  topicsOfWorks: any[];
  isPromoter = false;
  quantityOfUnhiddenTopics: number;
  quantityUniqueOfPromoters: number;

  private data: Array<any>;
  public rows: Array<any> = [];

  public page = 1;
  public itemsPerPage = 10;
  public maxSize = 5;
  public numPages = 1;
  public length = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  // Dodać w wolnej chwili "klasy modelowe, topicofworks i issusemester itd"
  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.isPromoter = this.authService.isPromoter();

    this.route.data.subscribe(data => {
      this.topicsOfWorks = data.thesis.topicOfWorkTables;
      this.quantityOfUnhiddenTopics = data.thesis.quantityOfUnhiddenTopics;
      this.quantityUniqueOfPromoters = data.thesis.quantityUniqueOfPromoters;
      this.data = this.fixTopicsOfWorks();
      this.length = this.data.length;

      if (this.length !== undefined) {
        this.onChangeTable(this.config);
      }
    });
  }

  public onCellClick(data: any): any {
    if (data.row.id !== undefined) {
      this.router.navigate(['thesis/' + data.row.id]);
    }
  }

  private fixTopicsOfWorks(): any[] {
    return this.topicsOfWorks.map(topicOfWork => {
      return {
        id: topicOfWork.id,
        thesisName: topicOfWork.name,
        supervisorName: topicOfWork.supervisorName,
        maxSize: topicOfWork.maxSize.toString(),
        faculty: topicOfWork.faculty,
        isReserved: this.fixState(topicOfWork)
      };
    });
  }

  fixState(topicOfWork: any) {
    if (topicOfWork.isReserved) {
      return 'Tak';
    } else {
      return 'Nie';
    }
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(
          this.config.filtering.filterString
        )
      );
    }

    const tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (
          item[column.name].toString().match(this.config.filtering.filterString)
        ) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(
    config: any,
    page: any = { page: this.page, itemsPerPage: this.itemsPerPage }
  ): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    const filteredData = this.changeFilter(this.data, this.config);
    const sortedData = this.changeSort(filteredData, this.config);
    this.rows =
      page && config.paging ? this.changePage(page, sortedData) : sortedData;
    // this.length = sortedData.length;
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    const start = (page.page - 1) * page.itemsPerPage;
    const end =
      page.itemsPerPage > -1 ? start + page.itemsPerPage : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    const columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (const iterator of columns) {
      columnName = columns.name;
      sort = columns.sort;
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }
}
