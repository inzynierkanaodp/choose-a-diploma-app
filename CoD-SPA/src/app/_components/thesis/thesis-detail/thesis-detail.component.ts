import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { AccountService } from 'src/app/_services/account.service';
import { TopicOfWorkService } from 'src/app/_services/topic-of-work.service';

@Component({
  selector: 'app-thesis-detail',
  templateUrl: './thesis-detail.component.html',
  styleUrls: ['./thesis-detail.component.css']
})
export class ThesisDetailComponent implements OnInit {
  thesis: any;
  isOwnerThisTopic = true;
  canReserveThisTopic = true;
  isStudent = false;

  constructor(
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private accountService: AccountService,
    private authService: AuthService,
    private topicOfWorkService: TopicOfWorkService
  ) {}

  ngOnInit() {
    this.accountService
      .canAccountReserveTopic(this.authService.decodedToken.nameid)
      .subscribe(data => {
        this.canReserveThisTopic = data;
      });

    this.isStudent = this.authService.isStudent();
    this.route.data.subscribe(data => {
      this.thesis = data.thesis;
    });

    const reserveTopicModel: any = {
      accountId: this.authService.decodedToken.nameid,
      topicId: this.thesis.id
    };

    this.topicOfWorkService
      .isOwnerThisTopic(reserveTopicModel)
      .subscribe(data => {
        this.isOwnerThisTopic = data;
      });
  }

  fixState() {
    if (this.thesis.isReserved) {
      return 'Tak';
    } else {
      return 'Nie';
    }
  }

  reserveTopic() {
    const reserveTopicModel: any = {
      accountId: this.authService.decodedToken.nameid,
      topicId: this.thesis.id
    };

    this.topicOfWorkService.reserveTopicOfWork(reserveTopicModel).subscribe(
      next => {
        this.alertify.success('Praca została zarezerwowana!');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
        window.location.reload();
      }
    );
  }

  resignTopic() {
    const reserveTopicModel: any = {
      accountId: this.authService.decodedToken.nameid,
      topicId: this.thesis.id
    };

    this.topicOfWorkService.resignTopicOfWork(reserveTopicModel).subscribe(
      next => {
        this.alertify.success('Pomyślna rezygnacja z pracy.');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
        window.location.reload();
      }
    );
  }
}
