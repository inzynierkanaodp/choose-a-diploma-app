import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { ActivatedRoute } from '@angular/router';
import { stringify } from 'querystring';
import { TopicOfWorkService } from 'src/app/_services/topic-of-work.service';
import { ThesisForAdd } from 'src/app/_models/thesis-for-add';
import { TopicOfWorkSaveModel } from 'src/app/_models/topic-of-work-save-model';

@Component({
  selector: 'app-thesis-add',
  templateUrl: './thesis-add.component.html',
  styleUrls: ['./thesis-add.component.css']
})
export class ThesisAddComponent implements OnInit {
  supervisor: any;
  issueSemesters: any[];
  studyFields: any[];

  issueSemestersDropdownList = [];
  issueSemestersSelectedItems = [];

  studyFieldsDropdownList = [];
  studyFieldsSelectedItems = [];

  dropdownSettings = {};

  @Output() cancelReset = new EventEmitter();
  thesisForm: FormGroup;
  submitted = false;

  constructor(
    private authService: AuthService,
    private alertify: AlertifyService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private topicOfWorkService: TopicOfWorkService
  ) {}

  ngOnInit() {
    this.thesisForm = this.formBuilder.group(
      {
        topicName: ['', Validators.required],
        supervisorFullName: ['', Validators.required],
        maxSize: ['', [Validators.required]],
        studyFields: ['', Validators.required],
        issueSemesters: ['', Validators.required],
        isReserved: [true, Validators.required],
        description: ['', Validators.required]
      },
      {}
    );

    this.route.data.subscribe(data => {
      this.supervisor = data.supervisor.account;
      this.issueSemesters = data.issueSemesters;
      this.studyFields = data.studyFieldsWithFacultiesInfos;
    });

    this.thesisForm.patchValue({
      supervisorFullName: this.setSupervisorFullName()
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.issueSemestersDropdownList = this.issueSemesters;
    this.studyFieldsDropdownList = this.fixStudyFields();
  }

  setSupervisorFullName(): string {
    const supervisorName = this.supervisor.name;
    const supervisorSecondName = this.supervisor.secondName;
    const supervisorSurname = this.supervisor.surname;

    if (supervisorSecondName !== undefined) {
      return (
        supervisorName + ' ' + supervisorSecondName + ' ' + supervisorSurname
      );
    } else {
      return supervisorName + ' ' + supervisorSurname;
    }
  }

  addThesis() {
    this.submitted = true;

    if (this.thesisForm.invalid) {
      return;
    }

    const thesisForAdd: ThesisForAdd = {
      topicName: this.thesisForm.value.topicName,
      description: this.thesisForm.value.description,
      maxSize: this.thesisForm.value.maxSize,
      supervisorFullName: this.thesisForm.value.supervisorFullName,
      isReserved: this.thesisForm.value.isReserved
    };

    const topicOfWorkSaveModel: TopicOfWorkSaveModel = {
      accountId: this.authService.decodedToken.nameid,
      issueSemesterInfos: this.thesisForm.value.issueSemesters,
      studyFieldWithFacultyInfos: this.thesisForm.value.studyFields,
      thesisForAdd
    };

    this.topicOfWorkService.saveTopicOfWork(topicOfWorkSaveModel).subscribe(
      () => {
        this.alertify.success('Praca została dodana.');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
        window.location.reload();
      }
    );
  }

  private fixStudyFields(): any[] {
    return this.studyFields.map(studyField => {
      return { id: studyField.id, name: studyField.studyFieldWithFacultyName };
    });
  }

  cancel() {
    this.cancelReset.emit(false);
    this.submitted = false;
  }

  get f() {
    return this.thesisForm.controls;
  }
}
