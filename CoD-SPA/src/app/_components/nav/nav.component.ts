import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { AuthService } from 'src/app/_services/auth.service';
import { GroupService } from 'src/app/_services/group.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {};

  groupId: number;
  isPromoter: boolean;
  isStudent: boolean;

  constructor(public authService: AuthService, private alertify: AlertifyService,
              private router: Router, private groupService: GroupService) { }

  ngOnInit() {
    if (this.loggedIn()) {
      this.isPromoter = this.authService.isPromoter();
      this.isStudent = this.authService.isStudent();
      this.groupService.getAccountGroupId(this.authService.decodedToken.nameid).subscribe(data => {
        this.groupId = data.idGroup;
      });
    }
  }

  goToGroup() {
    if (this.groupId !== undefined) {
      if (this.groupId != null) {
        this.router.navigate(['group/' + this.groupId]);
      } else {
        this.alertify.warning('Obecnie nie jesteś w żadnej grupie. Musisz do jakiejś dołączyć.');
      }
     }
  }

  login() {
    if (this.model.login !== undefined && this.model.login !== ''
    && this.model.password !== undefined && this.model.password !== '') {
      this.authService.login(this.model).subscribe(next => {
        this.alertify.success('Zalogowano.');
        this.router.navigate(['']);
      }, error => {
        this.alertify.error('Wystąpił błąd logowania.');
      }, () => {
        this.router.navigate(['']);
        if (this.loggedIn()) {
          this.isPromoter = this.authService.isPromoter();
          this.isStudent = this.authService.isStudent();
          this.groupService
          .getAccountGroupId(this.authService.decodedToken.nameid).subscribe(data => {
            this.groupId = data.idGroup;
          });
        }
      });
    }
  }

  loggedIn() {
    return this.authService.loggedIn(); // true is not null, false if token is null
  }

  logout() {
    localStorage.removeItem('token');
    this.alertify.message('Logged out.');
    this.router.navigate(['']);
    window.location.reload();
  }

  isAdmin() {
    return this.authService.isAdmin();
  }
}
