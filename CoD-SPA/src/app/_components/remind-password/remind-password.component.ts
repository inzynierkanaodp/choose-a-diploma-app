import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/_helpers/must-match.validator';

@Component({
  selector: 'app-remind-password',
  templateUrl: './remind-password.component.html',
  styleUrls: ['./remind-password.component.css']
})
export class RemindPasswordComponent implements OnInit {
  @Output() cancelReset = new EventEmitter();
  resetForm: FormGroup;
  submitted = false;

  constructor(
    private authService: AuthService,
    private alertify: AlertifyService,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      login: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      confirmNewPassword: ['', [Validators.required, Validators.minLength(6)]],
    }, {
      validator: MustMatch('newPassword', 'confirmNewPassword')
    });
  }

  get f() { return this.resetForm.controls; }

  reset() {
    this.submitted = true;

    if (this.resetForm.invalid) {
      return;
    }

    this.authService.reset(this.resetForm.value).subscribe(
      () => {
        this.alertify.success('Zresetowano');
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  cancel() {
    this.cancelReset.emit(false);
    this.submitted = false;
  }
}
