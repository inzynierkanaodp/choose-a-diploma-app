import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { AccountService } from 'src/app/_services/account.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  registerMode = false;

  isUserAcitve: boolean;
  showSpinner = false;

  constructor(
    private authService: AuthService,
    private accountService: AccountService
    ) {
    }

  ngOnInit() {
    if (this.authService.decodedToken && this.isUserAcitve === undefined && this.isLoggedIn()) {
      this.showSpinner = true;
      this.accountService.getIsActiveAccount(this.authService.decodedToken.nameid).subscribe(data => {
        this.isUserAcitve = data;
        this.showSpinner = false;
      });
    }
  }

  registerToggle() {
    this.registerMode = true;
  }

  isLoggedIn() {
    return this.authService.loggedIn();
  }

  cancelRegisterMode(registerMode: boolean) {
    this.registerMode = registerMode;
  }
}
