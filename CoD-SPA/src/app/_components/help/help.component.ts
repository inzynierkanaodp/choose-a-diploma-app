import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/_services/account.service';
import { AuthService } from 'src/app/_services/auth.service';
import { HelpService } from 'src/app/_services/help.service';
import { TicketSaveModel } from 'src/app/_models/ticket-save-model';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  helpForm: FormGroup;
  submitted = false;

  permissions: any[];

  dropdownSettings = {};

  permissionsDropdownList = [];
  permissionsSelectedItems = [];

  constructor(
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private helpService: HelpService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      noDataAvailablePlaceholderText: 'Brak danych'
    };

    this.route.data.subscribe(data => {
      this.permissions = data.permissions; // podaje nazwe tego co w routes.ts przekazuje dalej za pomoca route
    });

    this.helpForm = this.formBuilder.group(
      {
        requestTitle: ['', [Validators.required, Validators.minLength(5)]],
        permissionList: [''],
        helpMessage: ['', [Validators.required, Validators.minLength(14)]]
      },
      {}
    );

    this.permissionsDropdownList = this.permissions;
  }

  sendHelp() {
    this.submitted = true;

    if (this.helpForm.invalid) {
      return;
    }

    const ticket: TicketSaveModel = {
      title: this.helpForm.value.requestTitle,
      accountId: this.authService.decodedToken.nameid,
      helpMessage: this.helpForm.value.helpMessage,
      permissionInfos: this.helpForm.value.permissionList
    };

    this.helpService.sendTicket(ticket).subscribe(
      next => {
        this.helpForm.reset(); // nie jest brudny itp
        this.alertify.success('Zgłoszenie zostało wysłane.');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
        this.submitted = false;
      }
    );
  }

  clearValues() {

  }

  get f() {
    return this.helpForm.controls;
  }
}
