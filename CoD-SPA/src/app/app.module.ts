import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule, TabsModule, PaginationModule } from 'ngx-bootstrap';
import { JwtModule } from '@auth0/angular-jwt';
import { AppRoutingModule } from './app-routing.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { Ng2TableModule } from 'ng2-table/ng2-table';

import { AppComponent } from './app.component';
import { NavComponent } from './_components/nav/nav.component';
import { AuthService } from './_services/auth.service';
import { HomeComponent } from './_components/home/home.component';
import { RegisterComponent } from './_components/register/register.component';
import { ErrorInterceptorProvider } from './_services/error.interceptor';
import { AlertifyService } from './_services/alertify.service';
import { AuthGuard } from './_guards/auth.guard';
import { AccountService } from './_services/account.service';
import { MemberDetailResolver } from './_resolvers/member-detail.resolver';
import { MemberListResolver } from './_resolvers/member-list.resolver';
import { ThesisComponent } from './_components/thesis/thesis/thesis.component';
import { GroupsComponent } from './_components/groups/groups/groups.component';
import { AdminPanelComponent } from './_components/admin-panel/admin-panel/admin-panel.component';
import { HelpComponent } from './_components/help/help.component';
import { MemberEditComponent } from './_components/members/member-edit/member-edit.component';
import { LoadingSpinnerComponent } from './_ui/loading-spinner/loading-spinner.component';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { RemindPasswordComponent } from './_components/remind-password/remind-password.component';
import { TicketDetailComponent } from './_components/admin-panel/ticket-detail/ticket-detail.component';
import { LoginEditComponent } from './_components/login-edit/login-edit.component';
import { ThesisAddComponent } from './_components/thesis/thesis-add/thesis-add.component';
import { TopicOfWorkService } from './_services/topic-of-work.service';
import { IssueSemesterService } from './_services/issue-semester.service';
import { IssueSemesterListResolver } from './_resolvers/issue-semester-list.resolver';
import { StudyFieldListResolver } from './_resolvers/study-field-list.resolver';
import { FacultiesListResolver } from './_resolvers/faculties-list.resolver';
import { PermissionListResolver } from './_resolvers/permission-list.resolver';
import { MemberLoginDetailsResolver } from './_resolvers/member-login-details.resolver';
import { ThesisListResolver } from './_resolvers/thesis-list.resolver';
import { ThesisDetailComponent } from './_components/thesis/thesis-detail/thesis-detail.component';
import { ThesisDetailResolver } from './_resolvers/thesis-detail.resolver';
import { GroupListResolver } from './_resolvers/group-list.resolver';
import { GroupDetailResolver } from './_resolvers/group-detail.resolver';
import { GroupDetailComponent } from './_components/groups/group-detail/group-detail.component';
import { MyThesisComponent } from './_components/thesis/my-thesis/my-thesis.component';
import { MyThesisListResolver } from './_resolvers/my-thesis-list.resolver';
import { HelpInfoResolver } from './_resolvers/help-info.resolver';
import { TicketListComponent } from './_components/admin-panel/ticket-list/ticket-list.component';
import { TicketListResolver } from './_resolvers/ticket-list.resolver';
import { TicketDetailResolver } from './_resolvers/ticket-detail.resolver';

export function tokenGetter() {
   return localStorage.getItem('token');
}

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      HomeComponent,
      RegisterComponent,
      ThesisComponent,
      GroupsComponent,
      AdminPanelComponent,
      HelpComponent,
      MemberEditComponent,
      LoadingSpinnerComponent,
      RemindPasswordComponent,
      LoginEditComponent,
      ThesisAddComponent,
      ThesisDetailComponent,
      GroupDetailComponent,
      MyThesisComponent,
      TicketListComponent,
      TicketDetailComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      BsDropdownModule.forRoot(),
      TabsModule.forRoot(),
      JwtModule.forRoot({
         config: {
            tokenGetter,
            whitelistedDomains: ['localhost:5000'],
            blacklistedRoutes: ['localhost:5000/api/auth']
         }
      }),
      BrowserModule,
      ReactiveFormsModule,
      AppRoutingModule,
      NgMultiSelectDropDownModule.forRoot(),
      Ng2TableModule,
      PaginationModule.forRoot()
   ],
   providers: [
      AuthService,
      ErrorInterceptorProvider,
      AlertifyService,
      AuthGuard,
      AccountService,
      MemberDetailResolver,
      MemberListResolver,
      PreventUnsavedChanges,
      TopicOfWorkService,
      IssueSemesterService,
      IssueSemesterListResolver,
      StudyFieldListResolver,
      FacultiesListResolver,
      PermissionListResolver,
      MemberLoginDetailsResolver,
      ThesisListResolver,
      ThesisDetailResolver,
      GroupListResolver,
      GroupDetailResolver,
      MyThesisListResolver,
      HelpInfoResolver,
      TicketListResolver,
      TicketDetailResolver
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
