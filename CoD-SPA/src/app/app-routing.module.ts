import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_components/home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { ThesisComponent } from './_components/thesis/thesis/thesis.component';
import { HelpComponent } from './_components/help/help.component';
import { AdminPanelComponent } from './_components/admin-panel/admin-panel/admin-panel.component';
import { MemberEditComponent } from './_components/members/member-edit/member-edit.component';
import { RemindPasswordComponent } from './_components/remind-password/remind-password.component';
import { ThesisAddComponent } from './_components/thesis/thesis-add/thesis-add.component';
import { LoginEditComponent } from './_components/login-edit/login-edit.component';
import { MemberEditResolver } from './_resolvers/member-edit.resolver';
import { IssueSemesterListResolver } from './_resolvers/issue-semester-list.resolver';
import { StudyFieldListResolver } from './_resolvers/study-field-list.resolver';
import { PermissionListResolver } from './_resolvers/permission-list.resolver';
import { MemberLoginDetailsResolver } from './_resolvers/member-login-details.resolver';
import { ThesisListResolver } from './_resolvers/thesis-list.resolver';
import { ThesisDetailResolver } from './_resolvers/thesis-detail.resolver';
import { ThesisDetailComponent } from './_components/thesis/thesis-detail/thesis-detail.component';
import { GroupListResolver } from './_resolvers/group-list.resolver';
import { GroupDetailComponent } from './_components/groups/group-detail/group-detail.component';
import { GroupDetailResolver } from './_resolvers/group-detail.resolver';
import { GroupsComponent } from './_components/groups/groups/groups.component';
import { MyThesisComponent } from './_components/thesis/my-thesis/my-thesis.component';
import { MyThesisListResolver } from './_resolvers/my-thesis-list.resolver';
import { HelpInfoResolver } from './_resolvers/help-info.resolver';
import { TicketListComponent } from './_components/admin-panel/ticket-list/ticket-list.component';
import { TicketDetailComponent } from './_components/admin-panel/ticket-detail/ticket-detail.component';
import { TicketListResolver } from './_resolvers/ticket-list.resolver';
import { TicketDetailResolver } from './_resolvers/ticket-detail.resolver';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'remindPassword', component: RemindPasswordComponent },
  {
    path: '', // localhost:4200/members etc..
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'groups', component: GroupsComponent, resolve: {groups: GroupListResolver} },
      { path: 'group/:id', component: GroupDetailComponent, resolve: {group: GroupDetailResolver} },
      { path: 'help', component: HelpComponent, resolve: { permissions: PermissionListResolver} },
      { path: 'adminPanel', component: AdminPanelComponent, resolve: {info: HelpInfoResolver} },
      { path: 'adminPanel/tickets', component: TicketListComponent, resolve: {tickets: TicketListResolver} },
      { path: 'adminPanel/ticket/:id', component: TicketDetailComponent, resolve: {ticket: TicketDetailResolver} },
      { path: 'loginEdit', component: LoginEditComponent, resolve: {user: MemberLoginDetailsResolver} },
      { path: 'thesis', component: ThesisComponent , resolve: {thesis: ThesisListResolver}},
      { path: 'thesis/my-thesis', component: MyThesisComponent , resolve: {thesis: MyThesisListResolver}},
      { path: 'thesis/thesis-add', component: ThesisAddComponent,
      resolve: {supervisor: MemberEditResolver, issueSemesters: IssueSemesterListResolver,
        studyFieldsWithFacultiesInfos: StudyFieldListResolver}},
      { path: 'thesis/:id', component: ThesisDetailComponent, resolve: {thesis: ThesisDetailResolver}},
      {
        path: 'member/edit',
        component: MemberEditComponent,
        resolve: { accountData: MemberEditResolver, issueSemesters: IssueSemesterListResolver,
          studyFieldsWithFacultiesInfos: StudyFieldListResolver}
      }
    ]
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [MemberEditResolver]
})

export class AppRoutingModule {}
