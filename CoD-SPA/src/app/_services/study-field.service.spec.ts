/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { StudyFieldService } from './study-field.service';

describe('Service: StudyField', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudyFieldService]
    });
  });

  it('should ...', inject([StudyFieldService], (service: StudyFieldService) => {
    expect(service).toBeTruthy();
  }));
});
