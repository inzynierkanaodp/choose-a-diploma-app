import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SetPermissionAccountModel } from '../_models/set-permission-account-model';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {
  baseUrl = environment.apiUrl + 'Permission/';

  constructor(private http: HttpClient) {}

  getPermissions(): Observable<any> {
    return this.http.get<any>(
      this.baseUrl + 'GetPermissions'
    );
  }

  setPermissionAccount(setPermissionAccountModel: SetPermissionAccountModel) {
    return this.http.post(this.baseUrl + 'SetPermissionAccount/', setPermissionAccountModel);
  }
}
