import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TicketSaveModel } from '../_models/ticket-save-model';
import { ChangeTicketStatusModel } from '../_models/change-ticket-status-model';

@Injectable({
  providedIn: 'root'
})
export class HelpService {
  baseUrl = environment.apiUrl + 'Help/';

  constructor(private http: HttpClient) {}

  getInformationForAdminPanel(): Observable<any> {
    return this.http.get<any>(this.baseUrl + 'GetInformationForAdminPanel');
  }

  getTickets(): Observable<any> {
    return this.http.get<any>(this.baseUrl + 'GetTickets');
  }

  getTicket(id: number): Observable<any> {
    return this.http.get<any>(this.baseUrl + 'GetTicket/' + id);
  }

  sendTicket(ticketSaveModel: TicketSaveModel) {
    return this.http.post(this.baseUrl + 'SaveTicket/', ticketSaveModel);
  }

  changeTicketStatus(changeTicketStatusModel: ChangeTicketStatusModel) {
    return this.http.post(
      this.baseUrl + 'ChangeTicketStatus/',
      changeTicketStatusModel
    );
  }
}
