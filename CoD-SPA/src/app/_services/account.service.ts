import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Account } from '../_models/account';
import { AccountForUpdate } from '../_models/account-for-update';
import { UpdateAccountModel } from '../_models/update-account-model';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl = environment.apiUrl;

  isUserActive: boolean;

  constructor(private http: HttpClient) {}

  getAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(this.baseUrl + 'accounts');
  }

  getAccount(id: number): Observable<any> {
    return this.http.get<Account>(this.baseUrl + 'account/GetAccount/' + id);
  }

  canAccountReserveTopic(id: number): Observable<any> {
    return this.http.get<Account>(this.baseUrl + 'account/CanAccountReserveTopic/' + id);
  }

  getAccountLoginDetails(id: number): Observable<any> {
    return this.http.get<Account>(this.baseUrl + 'account/GetAccountLoginDetails/' + id);
  }

  getIsActiveAccount(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.baseUrl + 'account/IsActiveAccount/' + id);
  }

  updateAccount(updateAccountModel: UpdateAccountModel) {
    return this.http.put(this.baseUrl + 'account/', updateAccountModel);
  }

  activeAccount(ticket: any) {
    return this.http.put(this.baseUrl + 'account/ActiveAccount', ticket);
  }
}
