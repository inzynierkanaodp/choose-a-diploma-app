import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { Account } from '../_models/account';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.apiUrl + 'auth/';
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  constructor(private http: HttpClient) {}

  login(model: any) {
    return this.http.post(this.baseUrl + 'login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          this.decodedToken = this.jwtHelper.decodeToken(user.token);
        }
      })
    );
  }

  loggedIn() {
    const token = localStorage.getItem('token');

    return !this.jwtHelper.isTokenExpired(token);
  }

  register(model: Account) {
    return this.http.post(this.baseUrl + 'register', model);
  }

  reset(model: Account) {
    return this.http.put(this.baseUrl + 'reset', model);
  }

  changeEmail(model: Account) {
    return this.http.put(this.baseUrl + 'ChangeAccountEmail', model);
  }

  changeLogin(model: Account) {
    return this.http.put(this.baseUrl + 'ChangeAccountLogin', model);
  }

  isAdmin() {
    if (this.decodedToken.role.includes('Admin')) {
      return true;
    }
    return false;
  }

  isPromoter() {
    if (this.loggedIn()) {
      if (this.decodedToken.role.includes('Employee')) {
        return true;
      }
      return false;
    }
  }

  isStudent() {
    if (this.loggedIn()) {
      if (this.decodedToken.role.includes('Student')) {
        return true;
      }
      return false;
    }
  }
}
