/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { IssueSemesterService } from './issue-semester.service';

describe('Service: IssueSemester', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IssueSemesterService]
    });
  });

  it('should ...', inject([IssueSemesterService], (service: IssueSemesterService) => {
    expect(service).toBeTruthy();
  }));
});
