import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudyFieldService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getStudyFields(): Observable<any> {
    return this.http.get<any>(
      this.baseUrl + 'StudyField/GetStudyFields'
    );
  }

  getStudyFieldsWithFaculties(): Observable<any> {
    return this.http.get<any>(
      this.baseUrl + 'StudyField/GetStudyFieldsWithFacultiesInfo'
    );
  }
}
