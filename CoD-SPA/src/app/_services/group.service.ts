import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CreateGroupModel } from '../_models/create-group-model';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  baseUrl = environment.apiUrl + 'Group/';

constructor(private http: HttpClient) { }

getGroups(): Observable<any> {
  return this.http.get<any>(
    this.baseUrl + 'GetGroups'
  );
}

getGroup(id: number): Observable<any> {
  return this.http.get<any>(
    this.baseUrl + 'GetGroup/' + id
  );
}

createGroup(createGroupModel: CreateGroupModel) {
  return this.http.post(this.baseUrl + 'CreateGroup/', createGroupModel);
}

joinGroup(joinGroupModel: any) {
  return this.http.post(this.baseUrl + 'JoinGroup/', joinGroupModel);
}

canJoinGroup(accountId: number) {
  return this.http.get<any>(this.baseUrl + 'CanJoinGroup/' + accountId);
}

getAccountGroupId(accountId: number) {
  return this.http.get<any>(this.baseUrl + 'GetAccountGroup/' + accountId);
}

getGroupMembers(id: number) {
  return this.http.get<any>(this.baseUrl + 'GetGroupMembers/' + id);
}

archiveGroup(id: number) {
  return this.http.get<any>(this.baseUrl + 'ArchiveGroup/' + id);
}

removeGroupMember(accountId: number) {
  return this.http.get<any>(this.baseUrl + 'RemoveGroupMember/' + accountId);
}

isGroupLeader(accountId: number, groupId: number) {
  return this.http.get<any>(this.baseUrl + 'IsGroupLeader/' + accountId + '/' + groupId);
}

}
