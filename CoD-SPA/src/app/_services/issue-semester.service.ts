import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IssueSemesterService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getIssueSemesters(): Observable<any> {
    return this.http.get<any>(
      this.baseUrl + 'IssueSemester/GetIssueSemesters'
    );
  }
}
