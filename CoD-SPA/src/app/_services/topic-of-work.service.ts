import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TopicOfWorkService {

  baseUrl = environment.apiUrl + 'TopicOfWork/';

  constructor(private http: HttpClient) {}

  getTopicsOfWorks(): Observable<any> {
    return this.http.get<any>(
      this.baseUrl + 'GetTopicsOfWorks'
    );
  }

  getTopicsOfWorksByAccountId(accountId: number): Observable<any> {
    return this.http.get<any>(
      this.baseUrl + 'GetTopicsOfWorks/' + accountId
    );
  }

  getTopicOfWork(id: number): Observable<any> {
    return this.http.get<any>(
      this.baseUrl + 'GetTopicOfWork/' + id
    );
  }

  saveTopicOfWork(model: any) {
    return this.http.post(this.baseUrl + 'SaveTopicOfWork', model);
  }

  reserveTopicOfWork(model: any) {
    return this.http.post(this.baseUrl + 'ReserveTopicOfWork', model);
  }

  resignTopicOfWork(model: any) {
    return this.http.post(this.baseUrl + 'ResignTopicOfWork', model);
  }

  isOwnerThisTopic(model: any) {
    return this.http.post<boolean>(this.baseUrl + 'IsOwnerThisTopic', model);
  }
}
