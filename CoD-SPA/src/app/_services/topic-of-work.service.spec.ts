/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TopicOfWorkService } from './topic-of-work.service';

describe('Service: TopicOfWork', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TopicOfWorkService]
    });
  });

  it('should ...', inject([TopicOfWorkService], (service: TopicOfWorkService) => {
    expect(service).toBeTruthy();
  }));
});
