import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Account } from '../_models/account';
import { AccountService } from '../_services/account.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class MemberDetailResolver implements Resolve<Account> {
    constructor(private accountService: AccountService, private router: Router, private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Account> {
        return this.accountService.getAccount(route.params['id']).pipe(
            catchError(error => {
                this.alertify.error('Problem with retriving data.');
                this.router.navigate(['/members']);
                return of(null); // null ale jako observer
            })
        ); // Nie trzeba tu subskrypcji bo autmatycznie to robi, ale potrzebna jest obsluga bledow
    }
}
