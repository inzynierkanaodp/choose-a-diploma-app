import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AccountService } from '../_services/account.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FacultyService } from '../_services/faculty.service';

@Injectable()
export class FacultiesListResolver implements Resolve<any> {
    constructor(private router: Router, private alertify: AlertifyService, private facultyService: FacultyService) {}

    resolve(): Observable<any> {
        return this.facultyService.getFaculties().pipe(
            catchError((error) => {
                this.alertify.error('Problem with retriving your data.');
                this.router.navigate(['']);
                return of(null); // null ale jako observer
            })
        ); // Nie trzeba tu subskrypcji bo autmatycznie to robi, ale potrzebna jest obsluga bledow
    }
}