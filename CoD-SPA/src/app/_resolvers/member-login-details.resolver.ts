import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Account } from '../_models/account';
import { AccountService } from '../_services/account.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../_services/auth.service';

@Injectable()
export class MemberLoginDetailsResolver implements Resolve<any> {
    constructor(private accountService: AccountService, private router: Router,
                private alertify: AlertifyService, private authService: AuthService) {}

    resolve(): Observable<any> {
        return this.accountService.getAccountLoginDetails(this.authService.decodedToken.nameid).pipe(
            catchError((error) => {
                this.alertify.error('Problem with retriving your data.');
                this.router.navigate(['']);
                return of(null); // null ale jako observer
            })
        ); // Nie trzeba tu subskrypcji bo autmatycznie to robi, ale potrzebna jest obsluga bledow
    }
}
