import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AccountService } from '../_services/account.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { StudyFieldService } from '../_services/study-field.service';
import { TopicOfWorkService } from '../_services/topic-of-work.service';

@Injectable()
export class ThesisListResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private alertify: AlertifyService,
    private topicOfWorkService: TopicOfWorkService
  ) {}

  resolve(): Observable<any> {
    return this.topicOfWorkService.getTopicsOfWorks().pipe(
      catchError(error => {
        this.alertify.error('Problem z pobraniem danych.');
        this.router.navigate(['']);
        return of(null); // null ale jako observer
      })
    ); // Nie trzeba tu subskrypcji bo autmatycznie to robi, ale potrzebna jest obsluga bledow
  }
}
