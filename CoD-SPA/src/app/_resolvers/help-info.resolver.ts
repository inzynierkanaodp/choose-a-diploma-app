import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Account } from '../_models/account';
import { AccountService } from '../_services/account.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HelpService } from '../_services/help.service';

@Injectable()
export class HelpInfoResolver implements Resolve<Account> {
    constructor(private helpService: HelpService, private router: Router, private alertify: AlertifyService) {}

    resolve(): Observable<Account> {
        return this.helpService.getInformationForAdminPanel().pipe(
            catchError(error => {
                this.alertify.error('Problem with retriving data.');
                this.router.navigate(['/']);
                return of(null); // null ale jako observer
            })
        ); // Nie trzeba tu subskrypcji bo autmatycznie to robi, ale potrzebna jest obsluga bledow
    }
}
