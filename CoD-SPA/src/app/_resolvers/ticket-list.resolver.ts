import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AccountService } from '../_services/account.service';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { GroupService } from '../_services/group.service';
import { HelpService } from '../_services/help.service';

@Injectable()
export class TicketListResolver implements Resolve<any> {
  constructor(
    private router: Router,
    private alertify: AlertifyService,
    private helpService: HelpService
  ) {}

  resolve(): Observable<any> {
    return this.helpService.getTickets().pipe(
      catchError(error => {
        this.alertify.error('Problem z pobraniem danych.');
        this.router.navigate(['']);
        return of(null); // null ale jako observer
      })
    ); // Nie trzeba tu subskrypcji bo autmatycznie to robi, ale potrzebna jest obsluga bledow
  }
}
