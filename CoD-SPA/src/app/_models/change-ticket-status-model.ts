import { TicketStatus } from './_enums/ticket-status.enum';

export interface ChangeTicketStatusModel {
    ticketId: number;
    ticketStatus: TicketStatus;
}
