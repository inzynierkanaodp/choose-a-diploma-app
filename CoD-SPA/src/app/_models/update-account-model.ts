import { AccountForUpdate } from './account-for-update';

export interface UpdateAccountModel {
    accountId: number;
    accountForUpdate: AccountForUpdate;
    issueSemesterInfos: any[];
    studyFieldWithFacultyInfos: any[];
}
