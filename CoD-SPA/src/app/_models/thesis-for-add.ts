export interface ThesisForAdd {
    topicName: string;
    description: string;
    maxSize: number;
    supervisorFullName: string;
    isReserved: boolean;
}
