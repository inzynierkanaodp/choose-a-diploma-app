export interface Account {
    id: number;
    login: string;
    password: string;
    confirmPassword: string;
    email: string;
    name: string;
    secondName: string;
    surname: string;
    idGroup: number;
    indexNumber: number;
    isDeleted: boolean;
    notifications: boolean;
    idSemester: number;
    semesterName: string;
    facultyName: string;
    studyFieldName: string;
    scienceTitle: string;
    isEmployee: boolean;
}
