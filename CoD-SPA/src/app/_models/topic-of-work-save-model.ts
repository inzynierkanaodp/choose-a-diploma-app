import { ThesisForAdd } from './thesis-for-add';

export interface TopicOfWorkSaveModel {
    thesisForAdd: ThesisForAdd;
    issueSemesterInfos: any[];
    studyFieldWithFacultyInfos: any[];
    accountId: number;
}
