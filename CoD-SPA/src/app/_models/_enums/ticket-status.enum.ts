export enum TicketStatus {
  New = 1,
  Open = 2,
  Closed = 3
}
