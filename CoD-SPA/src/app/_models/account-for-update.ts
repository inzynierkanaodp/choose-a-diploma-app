export interface AccountForUpdate {
    name: string;
    secondName: string;
    surname: string;
    scienceTitle: string;
    indexNumber: number;
}
