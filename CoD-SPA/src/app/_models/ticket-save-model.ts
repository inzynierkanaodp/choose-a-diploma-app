import { AccountForUpdate } from './account-for-update';

export interface TicketSaveModel {
    permissionInfos: string[];
    title: string;
    helpMessage: string;
    accountId: number;
}