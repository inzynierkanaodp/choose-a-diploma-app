import { PermissionName } from './_enums/permission-name.enum';

export interface SetPermissionAccountModel {
    ticketId: number;
    permissionName: PermissionName;
}
