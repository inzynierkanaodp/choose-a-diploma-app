export interface CreateGroupModel {
    accountId: number;
    password: string;
    confirmPassword: string;
}
