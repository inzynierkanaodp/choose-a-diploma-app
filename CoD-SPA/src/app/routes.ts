/*
import { Routes } from '@angular/router';
import { HomeComponent } from './_components/home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { ThesisComponent } from './_components/thesis/thesis.component';
import { PromotersComponent } from './_components/promoters/promoters.component';
import { GroupsComponent } from './_components/groups/groups.component';
import { MessagesComponent } from './_components/messages/messages.component';
import { HelpComponent } from './_components/help/help.component';
import { AdminPanelComponent } from './_components/admin-panel/admin-panel.component';
import { MemberEditComponent } from './_components/members/member-edit/member-edit.component';
import { MemberEditResolver } from './_resolvers/member-edit.resolver';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  {
    path: '', // localhost:4200/members etc..
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      { path: 'home', component: HomeComponent},
      { path: 'thesis', component: ThesisComponent},
      { path: 'promoters', component: PromotersComponent},
      { path: 'groups', component: GroupsComponent},
      { path: 'messages', component: MessagesComponent},
      { path: 'help', component: HelpComponent},
      { path: 'adminPanel', component: AdminPanelComponent},
      { path: 'member/edit', component: MemberEditComponent},
      // { path: 'members', component: MemberListComponent, resolve: { users: MemberListResolver } },
      // { path: 'members/:id', component: MemberDetailComponent, resolve: { user: MemberDetailResolver } },
      { path: 'member/edit', component: MemberEditComponent,
      resolve: {memberEditResolver: MemberEditResolver} }
    ]
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];
*/