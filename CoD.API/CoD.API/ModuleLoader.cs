﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CoD.Common.Interfaces;

namespace CoD.WebApi
{
    public class ModuleLoader
    {
        public ModuleBase Load(ModuleItem moduleItem)
        {
            var moduleType = Type.GetType(moduleItem.Path, ResolveAssembly, null);

            if (!typeof(ModuleBase).IsAssignableFrom(moduleType))
            {
                throw new NotSupportedException($"Invalid module type: {moduleType.FullName}");
            }
            var module = (ModuleBase)Activator.CreateInstance(moduleType, moduleItem.Configuration);
            return module;
        }

        private Assembly ResolveAssembly(AssemblyName arg)
        {
            var directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            return Assembly.LoadFrom(Path.Combine(directory, arg.Name + ".dll"));
        }
    }
}
