﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoD.WebApi.Models
{
    public class AccountGroup
    {
        public int Id { get; set; }
        public int IdAccount { get; set; }
        public int IdGroup { get; set; }
    }
}
