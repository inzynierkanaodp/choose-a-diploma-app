﻿namespace CoD.WebApi.Models
{
    public class AccountForUpdate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public string ScienceTitle { get; set; }
        public int? IndexNumber { get; set; }
        public string SemesterName { get; set; }
        public string FacultyName { get; set; }
        public string StudyFieldName { get; set; }
        public bool CanCreateGroup { get; set; } 
    }
}