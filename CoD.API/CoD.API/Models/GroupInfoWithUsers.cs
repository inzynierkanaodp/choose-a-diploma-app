﻿using System;
using System.Collections.Generic;

namespace CoD.WebApi.Models
{
    public class GroupInfoWithUsers
    {
        public int Id { get; set; }
        public string TopicOfWorkName { get; set; }
        public string Founder { get; set; }
        public string MembersQuantity { get; set; }
        public string Faculty { get; set; }
        public string SupervisorName { get; set; }
        public string Semester { get; set; }

        public List<AccountForUpdate> AccountForUpdates { get; set; }
    }
}