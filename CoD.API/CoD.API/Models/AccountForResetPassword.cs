﻿namespace CoD.WebApi.Models
{
    public class AccountForResetPassword
    {
        public string Login { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
        public string Email { get; set; }

        public AccountForResetPassword(string login, string newPassword, string confirmNewPassword, string email, string oldPassword = null)
        {
            Login = login;
            Email = email;
            OldPassword = oldPassword;
            NewPassword = newPassword;
            ConfirmNewPassword = confirmNewPassword;
        }
    }
}