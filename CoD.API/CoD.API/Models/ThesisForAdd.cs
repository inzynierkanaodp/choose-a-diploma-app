﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models
{
    public class ThesisForAdd
    {
        [Required]
        public string TopicName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int MaxSize { get; set; }
        [Required]
        public bool IsReserved { get; set; }
    }
}