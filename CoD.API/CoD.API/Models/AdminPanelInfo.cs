﻿namespace CoD.WebApi.Models
{
    public class AdminPanelInfo
    {
        public int ClosedTicketsQuantity { get; set; }
        public int OpenTicketsQuantity { get; set; }
        public int UsersWithRolesQuantity { get; set; }
        public int UsersWithoutRolesQuantity { get; set; }
        public int ActiveAccountsQuantity { get; set; }
        public int NonActiveAccountsQuantity { get; set; }
    }
}