﻿namespace CoD.WebApi.Models
{
    public class FacultyInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
