﻿using System;

namespace CoD.WebApi.Models
{
    public class TicketTable
    {
        public int Id { get; set; }
        public string TicketOwnerName { get; set; }
        public string PermissionName { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
        public DateTime TicketDateTime { get; set; }
        public string Content { get; set; }
    }
}