﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoD.WebApi.Models
{
    public class Group
    {
        public int Id { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsArchived { get; set; }
        public int IdAccount { get; set; }
    }
}
