﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoD.WebApi.Models
{
    public class GroupTopic
    {
        public int Id { get; set; }
        public DateTime ReservationTime { get; set; }
        public bool? IsConfirmed { get; set; }
        public int IdGroup { get; set; }
        public int IdTopicOfWork { get; set; }
    }
}
