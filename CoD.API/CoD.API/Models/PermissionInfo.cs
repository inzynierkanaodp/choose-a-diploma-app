﻿namespace CoD.WebApi.Models
{
    public class PermissionInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
