﻿namespace CoD.WebApi.Models
{
    public class StudyFieldWithFacultyInfo
    {
        public int Id { get; set; }
        public string StudyFieldWithFacultyName { get; set; }
    }
}
