﻿namespace CoD.WebApi.Models
{
    public class AccountForResetLogin
    {
        public string Login { get; set; }

        public AccountForResetLogin(string login)
        {
            Login = login;
        }
    }
}