﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoD.WebApi.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime TicketDateTime { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public int IdPermission { get; set; }
        public int IdAccount { get; set; }
    }
}
