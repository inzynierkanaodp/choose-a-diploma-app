﻿namespace CoD.WebApi.Models
{
    public class StudyFieldInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
