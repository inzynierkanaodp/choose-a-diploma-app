﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoD.WebApi.Models
{
    public class TopicOfWork
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsReserved { get; set; }
        public bool IsHidden { get; set; }
        public int MaxSize { get; set; }
        public int IdAccount { get; set; }
        public int IdStudyField { get; set; }
        public int IdIssueSemester { get; set; }
    }
}
