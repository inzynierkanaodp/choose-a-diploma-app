﻿namespace CoD.WebApi.Models
{
    public class AccountLoginDetails
    {
        public string Login { get; set; }

        public string Email { get; set; }
    }
}