﻿namespace CoD.WebApi.Models
{
    public class AccountForResetEmail
    {
        public string Email { get; set; }

        public AccountForResetEmail(string email)
        {
            Email = email;
        }
    }
}