﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoD.WebApi.Models
{
    public class Permission
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
