﻿namespace CoD.WebApi.Models
{
    public class TopicOfWorkTable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SupervisorName { get; set; }
        public string SupervisorTitle { get; set; }
        public string Semester { get; set; }
        public string Faculty { get; set; }
        public bool IsReserved { get; set; }
        public bool IsHidden { get; set; }
        public int MaxSize { get; set; }
    }
}