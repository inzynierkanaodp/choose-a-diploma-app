﻿namespace CoD.WebApi.Models
{
    public class IssueSemesterInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}