﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.WebApi.Data;
using CoD.WebApi.Data.Classes;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Helpers;
using CoD.WebApi.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CoD.WebApi
{
    public class Startup
    {
        public IContainer ApplicationContainer { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var moduleLoader = new ModuleLoader();
            var modules = Configuration.GetSection("Modules").GetChildren().Select(c => new ModuleItem()
            {
                Path = c.GetValue<string>("File"),
                Configuration = c.GetSection("Configuration")
            }).Select(m => moduleLoader.Load(m)).ToArray();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .ConfigureApplicationPartManager(apm =>
                {
                    AddAspFeatureProviders(apm, modules.OfType<WebApiModule>());
                })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                });


            services.AddEntityFrameworkNpgsql().AddDbContext<DataContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
            }).BuildServiceProvider();


            services.AddCors();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IIssueSemesterRepository, IssueSemesterRepository>();
            services.AddScoped<IFacultyRepository, FacultyRepository>();
            services.AddScoped<IStudyFieldRepository, StudyFieldRepository>();
            services.AddScoped<ITopicOfWorkRepository, TopicOfWorkRepository>();
            services.AddScoped<IPermissionRepository, PermissionRepository>();
            services.AddScoped<IPermissionTicketRepository, PermissionTicketRepository>();
            services.AddScoped<IHelpRepository, HelpRepository>();
            services.AddScoped<IAccountStudyInfoRepository, AccountStudyInfoRepository>();
            services.AddScoped<IGroupRepository, GroupRepository>();
            services.AddScoped<IAccountsGroupsRepository, AccountsGroupsRepository>();
            services.AddScoped<IGroupsTopicsRepository, GroupsTopicsRepository>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt =>
            {
                opt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey =
                        new SymmetricSecurityKey(
                            Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });


            var builder = new ContainerBuilder();
            builder.Populate(services);

            builder.RegisterModule<NLogModule>();

            foreach (var module in modules)
            {
                builder.RegisterModule(module);
            }
            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }

        private static void AddAspFeatureProviders(Microsoft.AspNetCore.Mvc.ApplicationParts.ApplicationPartManager apm, IEnumerable<WebApiModule> modules)
        {
            foreach (var featureProvider in modules.SelectMany(d => d.ApplicationFeatureProviders))
            {
                apm.FeatureProviders.Add(featureProvider);
            }
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(builder =>
                {
                    builder.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            context.Response.AddApplicationError(error.Error.Message);

                            await context.Response.WriteAsync(error.Error.Message);
                        }
                    });
                });

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthentication();
            app.UseMvc();
        }

        private void ConfigureAutoMapper()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile(new AutoMapperProfiles());
            });

            var mapper = config.CreateMapper();
        }
    }
}
