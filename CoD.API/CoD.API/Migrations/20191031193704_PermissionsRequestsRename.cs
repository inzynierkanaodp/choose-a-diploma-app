﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoD.WebApi.Migrations
{
    public partial class PermissionsRequestsRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PermissionRequest_Permissions_IdPermission",
                table: "PermissionRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_PermissionRequest_Requests_IdRequest",
                table: "PermissionRequest");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PermissionRequest",
                table: "PermissionRequest");

            migrationBuilder.RenameTable(
                name: "PermissionRequest",
                newName: "PermissionsRequests");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionRequest_IdRequest",
                table: "PermissionsRequests",
                newName: "IX_PermissionsRequests_IdRequest");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionRequest_IdPermission",
                table: "PermissionsRequests",
                newName: "IX_PermissionsRequests_IdPermission");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PermissionsRequests",
                table: "PermissionsRequests",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionsRequests_Permissions_IdPermission",
                table: "PermissionsRequests",
                column: "IdPermission",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionsRequests_Requests_IdRequest",
                table: "PermissionsRequests",
                column: "IdRequest",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PermissionsRequests_Permissions_IdPermission",
                table: "PermissionsRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_PermissionsRequests_Requests_IdRequest",
                table: "PermissionsRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PermissionsRequests",
                table: "PermissionsRequests");

            migrationBuilder.RenameTable(
                name: "PermissionsRequests",
                newName: "PermissionRequest");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionsRequests_IdRequest",
                table: "PermissionRequest",
                newName: "IX_PermissionRequest_IdRequest");

            migrationBuilder.RenameIndex(
                name: "IX_PermissionsRequests_IdPermission",
                table: "PermissionRequest",
                newName: "IX_PermissionRequest_IdPermission");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PermissionRequest",
                table: "PermissionRequest",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionRequest_Permissions_IdPermission",
                table: "PermissionRequest",
                column: "IdPermission",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PermissionRequest_Requests_IdRequest",
                table: "PermissionRequest",
                column: "IdRequest",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
