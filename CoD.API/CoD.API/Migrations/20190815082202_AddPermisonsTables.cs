﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class AddPermisonsTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountGroup_Accounts_IdAccount",
                table: "AccountGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountGroup_Group_IdGroup",
                table: "AccountGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Group",
                table: "Group");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountGroup",
                table: "AccountGroup");

            migrationBuilder.RenameTable(
                name: "Group",
                newName: "Groups");

            migrationBuilder.RenameTable(
                name: "AccountGroup",
                newName: "AccountsGroups");

            migrationBuilder.RenameIndex(
                name: "IX_AccountGroup_IdGroup",
                table: "AccountsGroups",
                newName: "IX_AccountsGroups_IdGroup");

            migrationBuilder.RenameIndex(
                name: "IX_AccountGroup_IdAccount",
                table: "AccountsGroups",
                newName: "IX_AccountsGroups_IdAccount");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Groups",
                table: "Groups",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountsGroups",
                table: "AccountsGroups",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PermissionsAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdPermission = table.Column<int>(nullable: false),
                    IdAccount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionsAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PermissionsAccounts_Accounts_IdAccount",
                        column: x => x.IdAccount,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissionsAccounts_Permissions_IdPermission",
                        column: x => x.IdPermission,
                        principalTable: "Permissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_Name",
                table: "Permissions",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsAccounts_IdAccount",
                table: "PermissionsAccounts",
                column: "IdAccount");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsAccounts_IdPermission",
                table: "PermissionsAccounts",
                column: "IdPermission");

            migrationBuilder.AddForeignKey(
                name: "FK_AccountsGroups_Accounts_IdAccount",
                table: "AccountsGroups",
                column: "IdAccount",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountsGroups_Groups_IdGroup",
                table: "AccountsGroups",
                column: "IdGroup",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountsGroups_Accounts_IdAccount",
                table: "AccountsGroups");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountsGroups_Groups_IdGroup",
                table: "AccountsGroups");

            migrationBuilder.DropTable(
                name: "PermissionsAccounts");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Groups",
                table: "Groups");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountsGroups",
                table: "AccountsGroups");

            migrationBuilder.RenameTable(
                name: "Groups",
                newName: "Group");

            migrationBuilder.RenameTable(
                name: "AccountsGroups",
                newName: "AccountGroup");

            migrationBuilder.RenameIndex(
                name: "IX_AccountsGroups_IdGroup",
                table: "AccountGroup",
                newName: "IX_AccountGroup_IdGroup");

            migrationBuilder.RenameIndex(
                name: "IX_AccountsGroups_IdAccount",
                table: "AccountGroup",
                newName: "IX_AccountGroup_IdAccount");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Group",
                table: "Group",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountGroup",
                table: "AccountGroup",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AccountGroup_Accounts_IdAccount",
                table: "AccountGroup",
                column: "IdAccount",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountGroup_Group_IdGroup",
                table: "AccountGroup",
                column: "IdGroup",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
