﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class AddGroupsTopics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupsTopics",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ReservationTime = table.Column<DateTime>(nullable: false),
                    IsConfirmed = table.Column<bool>(nullable: false),
                    IdGroup = table.Column<int>(nullable: false),
                    IdTopicOfWork = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupsTopics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroupsTopics_Groups_IdGroup",
                        column: x => x.IdGroup,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupsTopics_TopicsOfWorks_IdTopicOfWork",
                        column: x => x.IdTopicOfWork,
                        principalTable: "TopicsOfWorks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupsTopics_IdGroup",
                table: "GroupsTopics",
                column: "IdGroup");

            migrationBuilder.CreateIndex(
                name: "IX_GroupsTopics_IdTopicOfWork",
                table: "GroupsTopics",
                column: "IdTopicOfWork");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupsTopics");
        }
    }
}
