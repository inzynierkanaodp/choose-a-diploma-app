﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoD.WebApi.Migrations
{
    public partial class StudyFieldsQuickFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_IssueSemesters_IdSemester",
                table: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_IdSemester",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "IdIssueSemester",
                table: "StudyFields");

            migrationBuilder.DropColumn(
                name: "IdSemester",
                table: "Accounts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdIssueSemester",
                table: "StudyFields",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdSemester",
                table: "Accounts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_IdSemester",
                table: "Accounts",
                column: "IdSemester");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_IssueSemesters_IdSemester",
                table: "Accounts",
                column: "IdSemester",
                principalTable: "IssueSemesters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
