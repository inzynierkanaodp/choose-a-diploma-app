﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class RequestsToTicketsRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountStudyInfo_Accounts_IdAccount",
                table: "AccountStudyInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountStudyInfo_IssueSemesters_IdSemester",
                table: "AccountStudyInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountStudyInfo_StudyFields_IdStudyField",
                table: "AccountStudyInfo");

            migrationBuilder.DropTable(
                name: "PermissionsRequests");

            migrationBuilder.DropTable(
                name: "Requests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountStudyInfo",
                table: "AccountStudyInfo");

            migrationBuilder.RenameTable(
                name: "AccountStudyInfo",
                newName: "AccountsStudiesInfos");

            migrationBuilder.RenameIndex(
                name: "IX_AccountStudyInfo_IdStudyField",
                table: "AccountsStudiesInfos",
                newName: "IX_AccountsStudiesInfos_IdStudyField");

            migrationBuilder.RenameIndex(
                name: "IX_AccountStudyInfo_IdSemester",
                table: "AccountsStudiesInfos",
                newName: "IX_AccountsStudiesInfos_IdSemester");

            migrationBuilder.RenameIndex(
                name: "IX_AccountStudyInfo_IdAccount",
                table: "AccountsStudiesInfos",
                newName: "IX_AccountsStudiesInfos_IdAccount");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountsStudiesInfos",
                table: "AccountsStudiesInfos",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Tickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TicketDateTime = table.Column<DateTime>(nullable: false),
                    Content = table.Column<string>(nullable: false),
                    Status = table.Column<string>(nullable: false),
                    IdTicket = table.Column<int>(nullable: false),
                    IdAccount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tickets_Accounts_IdAccount",
                        column: x => x.IdAccount,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PermissionsTickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdPermission = table.Column<int>(nullable: false),
                    IdTicket = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionsTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PermissionsTickets_Permissions_IdPermission",
                        column: x => x.IdPermission,
                        principalTable: "Permissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissionsTickets_Tickets_IdTicket",
                        column: x => x.IdTicket,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsTickets_IdPermission",
                table: "PermissionsTickets",
                column: "IdPermission");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsTickets_IdTicket",
                table: "PermissionsTickets",
                column: "IdTicket");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_IdAccount",
                table: "Tickets",
                column: "IdAccount");

            migrationBuilder.AddForeignKey(
                name: "FK_AccountsStudiesInfos_Accounts_IdAccount",
                table: "AccountsStudiesInfos",
                column: "IdAccount",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountsStudiesInfos_IssueSemesters_IdSemester",
                table: "AccountsStudiesInfos",
                column: "IdSemester",
                principalTable: "IssueSemesters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountsStudiesInfos_StudyFields_IdStudyField",
                table: "AccountsStudiesInfos",
                column: "IdStudyField",
                principalTable: "StudyFields",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountsStudiesInfos_Accounts_IdAccount",
                table: "AccountsStudiesInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountsStudiesInfos_IssueSemesters_IdSemester",
                table: "AccountsStudiesInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountsStudiesInfos_StudyFields_IdStudyField",
                table: "AccountsStudiesInfos");

            migrationBuilder.DropTable(
                name: "PermissionsTickets");

            migrationBuilder.DropTable(
                name: "Tickets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountsStudiesInfos",
                table: "AccountsStudiesInfos");

            migrationBuilder.RenameTable(
                name: "AccountsStudiesInfos",
                newName: "AccountStudyInfo");

            migrationBuilder.RenameIndex(
                name: "IX_AccountsStudiesInfos_IdStudyField",
                table: "AccountStudyInfo",
                newName: "IX_AccountStudyInfo_IdStudyField");

            migrationBuilder.RenameIndex(
                name: "IX_AccountsStudiesInfos_IdSemester",
                table: "AccountStudyInfo",
                newName: "IX_AccountStudyInfo_IdSemester");

            migrationBuilder.RenameIndex(
                name: "IX_AccountsStudiesInfos_IdAccount",
                table: "AccountStudyInfo",
                newName: "IX_AccountStudyInfo_IdAccount");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountStudyInfo",
                table: "AccountStudyInfo",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Requests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Content = table.Column<string>(nullable: false),
                    IdAccount = table.Column<int>(nullable: false),
                    RequestDateTime = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Requests_Accounts_IdAccount",
                        column: x => x.IdAccount,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PermissionsRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdPermission = table.Column<int>(nullable: false),
                    IdRequest = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionsRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PermissionsRequests_Permissions_IdPermission",
                        column: x => x.IdPermission,
                        principalTable: "Permissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissionsRequests_Requests_IdRequest",
                        column: x => x.IdRequest,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsRequests_IdPermission",
                table: "PermissionsRequests",
                column: "IdPermission");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsRequests_IdRequest",
                table: "PermissionsRequests",
                column: "IdRequest");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_IdAccount",
                table: "Requests",
                column: "IdAccount");

            migrationBuilder.AddForeignKey(
                name: "FK_AccountStudyInfo_Accounts_IdAccount",
                table: "AccountStudyInfo",
                column: "IdAccount",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountStudyInfo_IssueSemesters_IdSemester",
                table: "AccountStudyInfo",
                column: "IdSemester",
                principalTable: "IssueSemesters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountStudyInfo_StudyFields_IdStudyField",
                table: "AccountStudyInfo",
                column: "IdStudyField",
                principalTable: "StudyFields",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
