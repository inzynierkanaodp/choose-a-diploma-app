﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class AddIssueSemester : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IssueSemesters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Semester = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssueSemesters", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_IdSemester",
                table: "Accounts",
                column: "IdSemester");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_IssueSemesters_IdSemester",
                table: "Accounts",
                column: "IdSemester",
                principalTable: "IssueSemesters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_IssueSemesters_IdSemester",
                table: "Accounts");

            migrationBuilder.DropTable(
                name: "IssueSemesters");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_IdSemester",
                table: "Accounts");
        }
    }
}
