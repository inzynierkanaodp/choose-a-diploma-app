﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class PermissionsRequestsAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_Permissions_IdPermission",
                table: "Requests");

            migrationBuilder.DropIndex(
                name: "IX_Requests_IdPermission",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "IdPermission",
                table: "Requests");

            migrationBuilder.CreateTable(
                name: "PermissionRequest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdPermission = table.Column<int>(nullable: false),
                    IdRequest = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PermissionRequest_Permissions_IdPermission",
                        column: x => x.IdPermission,
                        principalTable: "Permissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissionRequest_Requests_IdRequest",
                        column: x => x.IdRequest,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PermissionRequest_IdPermission",
                table: "PermissionRequest",
                column: "IdPermission");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionRequest_IdRequest",
                table: "PermissionRequest",
                column: "IdRequest");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PermissionRequest");

            migrationBuilder.AddColumn<int>(
                name: "IdPermission",
                table: "Requests",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Requests_IdPermission",
                table: "Requests",
                column: "IdPermission");

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_Permissions_IdPermission",
                table: "Requests",
                column: "IdPermission",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
