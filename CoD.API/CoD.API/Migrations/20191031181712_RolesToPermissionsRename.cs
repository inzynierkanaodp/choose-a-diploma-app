﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class RolesToPermissionsRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_Roles_IdRole",
                table: "Requests");

            migrationBuilder.DropTable(
                name: "RolesAccounts");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.RenameColumn(
                name: "IdRole",
                table: "Requests",
                newName: "IdPermission");

            migrationBuilder.RenameIndex(
                name: "IX_Requests_IdRole",
                table: "Requests",
                newName: "IX_Requests_IdPermission");

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PermissionsAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdPermission = table.Column<int>(nullable: false),
                    IdAccount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionsAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PermissionsAccounts_Accounts_IdAccount",
                        column: x => x.IdAccount,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissionsAccounts_Permissions_IdPermission",
                        column: x => x.IdPermission,
                        principalTable: "Permissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_Name",
                table: "Permissions",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsAccounts_IdAccount",
                table: "PermissionsAccounts",
                column: "IdAccount");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionsAccounts_IdPermission",
                table: "PermissionsAccounts",
                column: "IdPermission");

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_Permissions_IdPermission",
                table: "Requests",
                column: "IdPermission",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Requests_Permissions_IdPermission",
                table: "Requests");

            migrationBuilder.DropTable(
                name: "PermissionsAccounts");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.RenameColumn(
                name: "IdPermission",
                table: "Requests",
                newName: "IdRole");

            migrationBuilder.RenameIndex(
                name: "IX_Requests_IdPermission",
                table: "Requests",
                newName: "IX_Requests_IdRole");

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RolesAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdAccount = table.Column<int>(nullable: false),
                    IdRole = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolesAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RolesAccounts_Accounts_IdAccount",
                        column: x => x.IdAccount,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolesAccounts_Roles_IdRole",
                        column: x => x.IdRole,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Roles_Name",
                table: "Roles",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RolesAccounts_IdAccount",
                table: "RolesAccounts",
                column: "IdAccount");

            migrationBuilder.CreateIndex(
                name: "IX_RolesAccounts_IdRole",
                table: "RolesAccounts",
                column: "IdRole");

            migrationBuilder.AddForeignKey(
                name: "FK_Requests_Roles_IdRole",
                table: "Requests",
                column: "IdRole",
                principalTable: "Roles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
