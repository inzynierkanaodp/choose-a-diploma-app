﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoD.WebApi.Migrations
{
    public partial class AccountTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "PasswordSalt",
                table: "Accounts",
                nullable: false,
                oldClrType: typeof(byte[]),
                oldNullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "PasswordHash",
                table: "Accounts",
                nullable: false,
                oldClrType: typeof(byte[]),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Login",
                table: "Accounts",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Accounts",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ScienceTitle",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondName",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IndexNumber",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Accounts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Notifications",
                table: "Accounts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "IdGroup",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdSemester",
                table: "Accounts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Email",
                table: "Accounts",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_IndexNumber",
                table: "Accounts",
                column: "IndexNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Login",
                table: "Accounts",
                column: "Login",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Accounts_Email",
                table: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_IndexNumber",
                table: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_Login",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "IdGroup",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "IdSemester",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "IndexNumber",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "Notifications",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "ScienceTitle",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "SecondName",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "Surname",
                table: "Accounts");

            migrationBuilder.AlterColumn<byte[]>(
                name: "PasswordSalt",
                table: "Accounts",
                nullable: true,
                oldClrType: typeof(byte[]));

            migrationBuilder.AlterColumn<byte[]>(
                name: "PasswordHash",
                table: "Accounts",
                nullable: true,
                oldClrType: typeof(byte[]));

            migrationBuilder.AlterColumn<string>(
                name: "Login",
                table: "Accounts",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
