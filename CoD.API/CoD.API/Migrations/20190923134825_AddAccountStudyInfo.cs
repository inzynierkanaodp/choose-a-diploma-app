﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class AddAccountStudyInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudyFields_IssueSemesters_IdIssueSemester",
                table: "StudyFields");

            migrationBuilder.DropIndex(
                name: "IX_StudyFields_IdIssueSemester",
                table: "StudyFields");

            migrationBuilder.CreateTable(
                name: "AccountStudyInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdAccount = table.Column<int>(nullable: false),
                    IdSemester = table.Column<int>(nullable: false),
                    IdStudyField = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountStudyInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountStudyInfo_Accounts_IdAccount",
                        column: x => x.IdAccount,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountStudyInfo_IssueSemesters_IdSemester",
                        column: x => x.IdSemester,
                        principalTable: "IssueSemesters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountStudyInfo_StudyFields_IdStudyField",
                        column: x => x.IdStudyField,
                        principalTable: "StudyFields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountStudyInfo_IdAccount",
                table: "AccountStudyInfo",
                column: "IdAccount");

            migrationBuilder.CreateIndex(
                name: "IX_AccountStudyInfo_IdSemester",
                table: "AccountStudyInfo",
                column: "IdSemester");

            migrationBuilder.CreateIndex(
                name: "IX_AccountStudyInfo_IdStudyField",
                table: "AccountStudyInfo",
                column: "IdStudyField");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountStudyInfo");

            migrationBuilder.CreateIndex(
                name: "IX_StudyFields_IdIssueSemester",
                table: "StudyFields",
                column: "IdIssueSemester");

            migrationBuilder.AddForeignKey(
                name: "FK_StudyFields_IssueSemesters_IdIssueSemester",
                table: "StudyFields",
                column: "IdIssueSemester",
                principalTable: "IssueSemesters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
