﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoD.WebApi.Migrations
{
    public partial class IssueSemestersToTopicOfWorksRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdSemester",
                table: "TopicsOfWorks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TopicsOfWorks_IdSemester",
                table: "TopicsOfWorks",
                column: "IdSemester");

            migrationBuilder.AddForeignKey(
                name: "FK_TopicsOfWorks_IssueSemesters_IdSemester",
                table: "TopicsOfWorks",
                column: "IdSemester",
                principalTable: "IssueSemesters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TopicsOfWorks_IssueSemesters_IdSemester",
                table: "TopicsOfWorks");

            migrationBuilder.DropIndex(
                name: "IX_TopicsOfWorks_IdSemester",
                table: "TopicsOfWorks");

            migrationBuilder.DropColumn(
                name: "IdSemester",
                table: "TopicsOfWorks");
        }
    }
}
