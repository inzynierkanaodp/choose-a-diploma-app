﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CoD.WebApi.Migrations
{
    public partial class AddStudyFieldAndTopicOfWorks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StudyFields",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    IdFaculty = table.Column<int>(nullable: false),
                    IdIssueSemester = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudyFields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudyFields_Faculties_IdFaculty",
                        column: x => x.IdFaculty,
                        principalTable: "Faculties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudyFields_IssueSemesters_IdIssueSemester",
                        column: x => x.IdIssueSemester,
                        principalTable: "IssueSemesters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TopicsOfWorks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    IsReserved = table.Column<bool>(nullable: false),
                    IsHidden = table.Column<bool>(nullable: false),
                    MaxSize = table.Column<int>(nullable: false),
                    IdAccount = table.Column<int>(nullable: false),
                    IdStudyField = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopicsOfWorks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TopicsOfWorks_Accounts_IdAccount",
                        column: x => x.IdAccount,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopicsOfWorks_StudyFields_IdStudyField",
                        column: x => x.IdStudyField,
                        principalTable: "StudyFields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudyFields_IdFaculty",
                table: "StudyFields",
                column: "IdFaculty");

            migrationBuilder.CreateIndex(
                name: "IX_StudyFields_IdIssueSemester",
                table: "StudyFields",
                column: "IdIssueSemester");

            migrationBuilder.CreateIndex(
                name: "IX_TopicsOfWorks_IdAccount",
                table: "TopicsOfWorks",
                column: "IdAccount");

            migrationBuilder.CreateIndex(
                name: "IX_TopicsOfWorks_IdStudyField",
                table: "TopicsOfWorks",
                column: "IdStudyField");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TopicsOfWorks");

            migrationBuilder.DropTable(
                name: "StudyFields");
        }
    }
}
