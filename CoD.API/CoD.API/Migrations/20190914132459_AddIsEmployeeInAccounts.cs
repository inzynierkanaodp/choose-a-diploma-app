﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoD.WebApi.Migrations
{
    public partial class AddIsEmployeeInAccounts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsEmployee",
                table: "Accounts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsEmployee",
                table: "Accounts");
        }
    }
}
