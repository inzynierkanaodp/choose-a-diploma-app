﻿namespace CoD.WebApi.Helpers
{
    public class Permissions
    {
        public const string Student = "Student";
        public const string GroupLeader = "GroupLeader";
        public const string Employee = "Employee";
        public const string Admin = "Admin";
        public const string SuperAdmin = "SuperAdmin";
    }
}