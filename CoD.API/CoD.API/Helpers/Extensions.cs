﻿using System.Collections.Generic;
using System.Linq;
using CoD.WebApi.DbModels;
using CoD.WebApi.Enums;
using CoD.WebApi.ModelsForDto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CoD.WebApi.Helpers
{
    public static class Extensions
    {
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin", "*");
        }

        public static string GetErrorMessages(ModelStateDictionary modelStateDictionary)
        {
            string messages = "";

            if (!modelStateDictionary.IsValid)
            {
                foreach (var modelState in modelStateDictionary.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        messages += error.ErrorMessage + " ";
                    }
                }
            }

            return messages;
        }

        public static string GetSemesterNames(this List<AccountStudyInfoDb> accountStudyInfos)
        {
            string semesterNames = "Brak semestru";

            if (accountStudyInfos.Any())
                semesterNames = string.Join(", ", accountStudyInfos.Select(x => x.Semester.Name));
            return semesterNames;
        }

        public static string GetStudyFieldNames(this List<AccountStudyInfoDb> accountStudyInfos)
        {
            string studyFieldNames = "Brak kierunku";

            if (accountStudyInfos.Any())
                studyFieldNames = string.Join(", ", accountStudyInfos.Select(x => x.StudyField.Name));
            return studyFieldNames;
        }

        public static string GetFacultyNames(this List<AccountStudyInfoDb> accountStudyInfos)
        {
            string facultyNames = "Brak wydziału";

            if (accountStudyInfos.Any())
                facultyNames = string.Join(", ", accountStudyInfos.Select(x => x.StudyField.Faculty.Name));

            return facultyNames;
        }

        public static string GetTopicNameForGroups(this List<GroupTopicDb> groupTopicDbs, int idGroup)
        {
            var topicNames = groupTopicDbs.FirstOrDefault(gt => gt.IdGroup == idGroup)?.TopicOfWork.Name;

            return string.IsNullOrEmpty(topicNames) ? "Brak" : topicNames;
        }

        public static string GetFacultyNameForGroups(this List<GroupTopicDb> groupTopicDbs, int idGroup)
        {
            var facultyName = groupTopicDbs.FirstOrDefault(gt => gt.IdGroup == idGroup)?.TopicOfWork.StudyField?.Faculty?.Name;

            return string.IsNullOrEmpty(facultyName) ? "Brak" : facultyName;
        }

        public static string GetSupervisorNameForGroups(this List<GroupTopicDb> groupTopicDbs, int idGroup)
        {
            var superVisorName = groupTopicDbs.FirstOrDefault(gt => gt.IdGroup == idGroup)?.TopicOfWork.Account.MakeFullName();

            return string.IsNullOrEmpty(superVisorName) ? "Brak" : superVisorName;
        }

        public static string GetFounderGroupName(this List<AccountGroupDb> accountGroupDbs, int idAccount)
        {
            var founderFullName = accountGroupDbs.FirstOrDefault(gt => gt.IdAccount == idAccount)?.Account.MakeFullName();

            return string.IsNullOrEmpty(founderFullName) ? "Brak" : founderFullName;
        }

        public static string GetPermissionName(this List<PermissionTicketDb> permissionTicketDbs, int idTicket)
        {
            var permissionName = permissionTicketDbs.FirstOrDefault(gt => gt.IdTicket == idTicket)?.Permission.Name ?? "";

            return string.IsNullOrEmpty(permissionName) ? "Brak" : permissionName;
        }
        public static string GetIssueSemesterName(this List<GroupTopicDb> groupTopicDbs, int idGroup)
        {
            var issueSemesterName = groupTopicDbs.FirstOrDefault(gt => gt.IdGroup == idGroup)?.TopicOfWork?.IssueSemesters?.Name;

            return string.IsNullOrEmpty(issueSemesterName) ? "Brak" : issueSemesterName;
        }
        public static string GetMaxQuantityOfStudents(this List<GroupTopicDb> groupTopicDbs, int idGroup)
        {
            var maxSize = groupTopicDbs.FirstOrDefault(gt => gt.IdGroup == idGroup)?.TopicOfWork.MaxSize;

            return string.IsNullOrEmpty(maxSize.ToString()) ? "Brak" : maxSize.ToString();
        }

        public static string GetTicketStatus(this string status)
        {
            int.TryParse(status, out var statusId);
            if (statusId == (int)TicketStatus.New)
                return "Nowy";
            if (statusId == (int)TicketStatus.Open)
                return "Otwarty";
            if (statusId == (int)TicketStatus.Closed)
                return "Zamkięty";
            return "Inny";
        }

        public static string MakeFullName(this AccountDb accountDb)
        {
            string fullname;

            if (accountDb?.SecondName != null)
                fullname = accountDb?.Name + " " + accountDb?.SecondName + " " + accountDb?.Surname;
            else
                fullname = accountDb?.Name + " " + accountDb?.Surname;

            if (string.IsNullOrEmpty(fullname) || string.IsNullOrWhiteSpace(fullname))
                return "Brak danych";

            return fullname;
        }
    }
}
