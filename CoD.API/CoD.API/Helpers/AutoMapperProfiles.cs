﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoD.WebApi.DbModels;
using CoD.WebApi.Migrations;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore.Design.Internal;

namespace CoD.WebApi.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<AccountDb, AccountForUpdate>()
                .ForMember(dest => dest.SemesterName,
                    opt => { opt.MapFrom(d => d.AccountStudyInfo.ToList().GetSemesterNames()); })
                .ForMember(dest => dest.StudyFieldName,
                    opt => { opt.MapFrom(d => d.AccountStudyInfo.ToList().GetStudyFieldNames()); })
                .ForMember(dest => dest.FacultyName,
                    opt => { opt.MapFrom(d => d.AccountStudyInfo.ToList().GetFacultyNames()); });
            CreateMap<AccountDb, AccountLoginDetails>();

            CreateMap<AccountForUpdate, AccountDb>();
            CreateMap<AccountForResetEmail, AccountDb>();
            CreateMap<AccountForResetLogin, AccountDb>();

            CreateMap<FacultyDb, FacultyInfo>();
            CreateMap<StudyFieldDb, StudyFieldInfo>();
            CreateMap<IssueSemesterDb, IssueSemesterInfo>();
            CreateMap<PermissionDb, PermissionInfo>();

            CreateMap<StudyFieldDb, StudyFieldWithFacultyInfo>()
                .ForMember(dest => dest.StudyFieldWithFacultyName,
                    opt => opt.MapFrom(d => d.Name + " - " + d.Faculty.Name));

            CreateMap<TopicOfWorkDb, TopicOfWorkTable>()
                .ForMember(dest => dest.SupervisorName,
                    opt =>
                    {
                        opt.MapFrom(d => d.Account.MakeFullName());
                    })
                .ForMember(dest => dest.Faculty,
                    opt => { opt.MapFrom(d => d.StudyField.Faculty.Name); })
                .ForMember(dest => dest.Semester,
                    opt => { opt.MapFrom(d => d.IssueSemesters.Name); })
                .ForMember(dest => dest.SupervisorTitle,
                    opt =>
                    {
                        opt.MapFrom(d => d.Account.ScienceTitle);
                    });

            CreateMap<GroupDb, GroupTable>()
                .ForMember(dest => dest.TopicOfWorkName,
                    opt =>
                    {
                        opt.MapFrom(d => d.GroupsTopics.ToList().GetTopicNameForGroups(d.Id) ?? "");
                    })
                .ForMember(dest => dest.Faculty,
                    opt =>
                    {
                        opt.MapFrom(d => d.GroupsTopics.ToList().GetFacultyNameForGroups(d.Id) ?? "");
                    })
                .ForMember(dest => dest.Founder,
                    opt => { opt.MapFrom(d => d.AccountsGroups.ToList().GetFounderGroupName(d.IdAccount)); })
                .ForMember(dest => dest.MembersQuantity, opt =>
                {
                    opt.MapFrom(d => d.AccountsGroups.Select(x => x.IdGroup == d.Id).Count());
                })
                .ForMember(dest => dest.SupervisorName,
                    opt =>
                    {
                        opt.MapFrom(d =>
                            d.GroupsTopics.ToList().GetSupervisorNameForGroups(d.Id) ?? "");
                    })
                .ForMember(dest => dest.Semester, opt =>
                {
                    opt.MapFrom(d => d.GroupsTopics.ToList().GetIssueSemesterName(d.Id) ?? "");
                })
                .ForMember(dest => dest.MaxQuantity, opt =>
                {
                    opt.MapFrom(d => d.GroupsTopics.ToList().GetMaxQuantityOfStudents(d.Id) ?? "");
                });


            CreateMap<TicketDb, TicketTable>()
                .ForMember(dest => dest.PermissionName,
                    opt =>
                    {
                        opt.MapFrom(d => d.PermissionsTickets.ToList().GetPermissionName(d.Id) ?? "");
                    })
                .ForMember(dest => dest.State,
                    opt =>
                    {
                        opt.MapFrom(d => d.Status.GetTicketStatus());
                    })
                .ForMember(dest => dest.TicketOwnerName,
                    opt =>
                    {
                        opt.MapFrom(d => d.Account.MakeFullName());
                    });
        }
    }
}