﻿namespace CoD.WebApi.Enums
{
    public enum PermissionName
    {
        SuperAdmin = 1,
        Admin = 2,
        Employee = 3,
        GroupLeader = 4,
        Student = 5
    }
}