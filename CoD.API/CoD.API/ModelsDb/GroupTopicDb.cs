﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoD.WebApi.ModelsForDto
{
    public class GroupTopicDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("ReservationTime", Order = 1)]
        public DateTime ReservationTime { get; set; }

        [Column("IsConfirmed", Order = 2)]
        public bool? IsConfirmed { get; set; }

        [Required]
        [Column("IdGroup", Order = 3)]
        public int IdGroup { get; set; }

        [Required]
        [Column("IdTopicOfWork", Order = 4)]
        public int IdTopicOfWork { get; set; }

        public GroupDb Group { get; set; }
        public TopicOfWorkDb TopicOfWork { get; set; }
    }
}
