﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.DbModels;

namespace CoD.WebApi.ModelsForDto
{
    public class PermissionAccountDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("IdPermission", Order = 1)]
        public int IdPermission { get; set; }

        [Required]
        [Column("IdAccount", Order = 2)]
        public int IdAccount { get; set; }

        public PermissionDb Permission { get; set; }
        public AccountDb Account { get; set; }
    }
}
