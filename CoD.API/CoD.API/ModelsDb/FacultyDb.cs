﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoD.WebApi.ModelsForDto
{
    public class FacultyDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("Name", Order = 1)]
        public string Name { get; set; }

        public IList<StudyFieldDb> StudyFields { get; set; }
    }
}
