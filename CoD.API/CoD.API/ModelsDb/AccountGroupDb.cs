﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.DbModels;

namespace CoD.WebApi.ModelsForDto
{
    public class AccountGroupDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("IdAccount", Order = 1)]
        public int IdAccount { get; set; }

        [Required]
        [Column("IdGroup", Order = 2)]
        public int IdGroup { get; set; }

        public AccountDb Account { get; set; }
        public GroupDb Group { get; set; }
    }
}
