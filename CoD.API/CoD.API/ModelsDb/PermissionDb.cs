﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoD.WebApi.ModelsForDto
{
    public class PermissionDb
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("Name", Order = 1)]
        public string Name { get; set; }

        public IList<PermissionAccountDb> PermissionsAccounts { get; set; }
        public IList<PermissionTicketDb> PermissionsTickets { get; set; }
    }
}
