﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoD.WebApi.ModelsForDto
{
    public class GroupDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("PasswordHash", Order = 1)]
        public byte[] PasswordHash { get; set; }

        [Required]
        [Column("PasswordSalt", Order = 2)]
        public byte[] PasswordSalt { get; set; }

        [Column("IsDeleted", Order = 3)]
        public bool? IsDeleted { get; set; }

        [Column("IsArchived", Order = 4)]
        public bool? IsArchived { get; set; }

        [Required]
        [Column("IdAccount", Order = 5)]
        public int IdAccount { get; set; }

        public IList<AccountGroupDb> AccountsGroups { get; set; }
        public IList<GroupTopicDb> GroupsTopics { get; set; }
    }
}
