﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.DbModels;

namespace CoD.WebApi.ModelsForDto
{
    public class AccountStudyInfoDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("IdAccount", Order = 1)]
        public int IdAccount { get; set; }

        [Required]
        [Column("IdSemester", Order = 2)]
        public int IdSemester { get; set; }

        [Required]
        [Column("IdStudyField", Order = 3)]
        public int IdStudyField { get; set; }

        public StudyFieldDb StudyField { get; set; }
        public AccountDb Account { get; set; }
        public IssueSemesterDb Semester { get; set; }
    }
}
