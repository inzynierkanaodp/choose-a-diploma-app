﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.DbModels;

namespace CoD.WebApi.ModelsForDto
{
    public class PermissionTicketDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("IdPermission", Order = 1)]
        public int IdPermission { get; set; }

        [Required]
        [Column("IdTicket", Order = 2)]
        public int IdTicket { get; set; }

        public PermissionDb Permission { get; set; }
        public TicketDb Ticket { get; set; }
    }
}
