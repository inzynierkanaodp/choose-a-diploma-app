﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.DbModels;

namespace CoD.WebApi.ModelsForDto
{
    public class TopicOfWorkDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("Name", Order = 1)]
        public string Name { get; set; }

        [Required]
        [Column("Description", Order = 2)]
        public string Description { get; set; }

        [Required]
        [Column("IsReserved", Order = 3)]
        public bool IsReserved { get; set; }

        [Required]
        [Column("IsHidden", Order = 4)]
        public bool IsHidden { get; set; }

        [Required]
        [Column("MaxSize", Order = 5)]
        public int MaxSize { get; set; }

        [Required]
        [Column("IdAccount", Order = 6)]
        public int IdAccount { get; set; }

        [Required]
        [Column("IdStudyField", Order = 7)]
        public int IdStudyField { get; set; }

        [Required]
        [Column("IdSemester", Order = 8)]
        public int IdSemester { get; set; }

        public AccountDb Account { get; set; }
        public StudyFieldDb StudyField { get; set; }
        public IssueSemesterDb IssueSemesters { get; set; }
        public IList<GroupTopicDb> GroupsTopics { get; set; }
    }
}
