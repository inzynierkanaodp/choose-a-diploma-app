﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoD.WebApi.ModelsForDto
{
    public class StudyFieldDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("Name", Order = 1)]
        public string Name { get; set; }

        [Required]
        [Column("IdFaculty", Order = 2)]
        public int IdFaculty { get; set; }

        public FacultyDb Faculty { get; set; }

        public IList<TopicOfWorkDb> TopicsOfWorks { get; set; }
        public IList<AccountStudyInfoDb> AccountStudyInfo { get; set; }
    }
}
