﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.DbModels;

namespace CoD.WebApi.ModelsForDto
{
    public class TicketDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("TicketDateTime", Order = 1)]
        public DateTime TicketDateTime { get; set; }

        [Required]
        [Column("Title", Order = 2)]
        public string Title { get; set; }

        [Required]
        [Column("Content", Order = 3)]
        public string Content { get; set; }

        [Required]
        [Column("Status", Order = 4)]
        public string Status { get; set; }

        [Required]
        [Column("IdAccount", Order = 5)]
        public int IdAccount { get; set; }

        public IList<PermissionTicketDb> PermissionsTickets { get; set; }
        public AccountDb Account { get; set; }
    }
}
