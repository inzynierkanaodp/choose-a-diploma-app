﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.DbModels
{
    public class AccountDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("Login", Order = 1)]
        public string Login { get; set; }

        [Required]
        [Column("PasswordHash", Order = 2)]
        public byte[] PasswordHash { get; set; }

        [Required]
        [Column("PasswordSalt", Order = 3)]
        public byte[] PasswordSalt { get; set; }

        [Required]
        [Column("Email", Order = 4)]
        public string Email { get; set; }

        [Column("ScienceTitle", Order = 5)]
        public string ScienceTitle { get; set; }

        [Column("Name", Order = 6)]
        public string Name { get; set; }

        [Column("SecondName", Order = 7)]
        public string SecondName { get; set; }

        [Column("Surname", Order = 8)]
        public string Surname { get; set; }

        [Column("IndexNumber", Order = 9)]
        public int? IndexNumber { get; set; }

        [Required]
        [Column("IsDeleted", Order = 10)]
        public bool IsDeleted { get; set; }

        [Required]
        [Column("IsActive", Order = 11)]
        public bool IsActive { get; set; }

        [Required]
        [Column("IsEmployee", Order = 12)]
        public bool IsEmployee { get; set; }

        [Required]
        [Column("Notifications", Order = 13)]
        public bool Notifications { get; set; }

        public IList<AccountGroupDb> AccountsGroups { get; set; }
        public IList<PermissionAccountDb> PermissionsAccounts { get; set; }
        public IList<TicketDb> Tickets { get; set; }
        public IList<TopicOfWorkDb> TopicsOfWorks { get; set; }
        public IList<AccountStudyInfoDb> AccountStudyInfo { get; set; }
    }
}
