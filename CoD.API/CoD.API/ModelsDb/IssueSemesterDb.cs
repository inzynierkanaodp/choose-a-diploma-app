﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoD.WebApi.DbModels;

namespace CoD.WebApi.ModelsForDto
{
    public class IssueSemesterDb
    {
        [Key]
        [Required]
        [Column("Id", Order = 0)]
        public int Id { get; set; }

        [Required]
        [Column("Name", Order = 1)]
        public string Name { get; set; }

        public IList<AccountStudyInfoDb> AccountStudyInfo { get; set; }
        public IList<TopicOfWorkDb> TopicsOfWorks { get; set; }
    }
}
