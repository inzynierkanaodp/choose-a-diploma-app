﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace CoD.WebApi
{
    public class ModuleItem
    {
        public string Path { get; set; }

        public IConfigurationSection Configuration { get; set; }
    }
}
