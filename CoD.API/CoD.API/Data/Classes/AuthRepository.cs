﻿using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;

        public AuthRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<AccountDb> Register(AccountDb account, string password)
        {
            CreatePasswordHash(password, out var passwordHash, out var passwordSalt);

            account.PasswordSalt = passwordSalt;
            account.PasswordHash = passwordHash;

            await _context.Accounts.AddAsync(account);
            await _context.SaveChangesAsync();

            return account;
        }

        public async Task<bool> SetNewPassword(AccountDb account, string password)
        {
            CreatePasswordHash(password, out var passwordHash, out var passwordSalt);

            account.PasswordSalt = passwordSalt;
            account.PasswordHash = passwordHash;

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<AccountDb> Login(string login, string password)
        {
            var user = await _context.Accounts.FirstOrDefaultAsync(x => x.Login == login);

            if (user == null)
                return null;

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        public async Task<bool> AccountExists(string login = null, string email = null)
        {
            if (await _context.Accounts.AnyAsync(x => x.Login == login || x.Email == email))
                return true;

            return false;
        }

        public async Task<AccountDb> GetAccountByLoginOrEmail(string login = null, string email = null)
        {
            return await _context.Accounts.FirstOrDefaultAsync(x => x.Login == login || x.Email == email);
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }

                return true;
            }
        }
    }
}