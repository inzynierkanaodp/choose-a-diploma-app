﻿using System.Threading.Tasks;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Classes
{
    public class PermissionTicketRepository : IPermissionTicketRepository
    {
        private readonly DataContext _context;

        public PermissionTicketRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<int> SavePermissionTicket(PermissionTicketDb permissionTicketDb)
        {
            await _context.PermissionsTickets.AddAsync(permissionTicketDb);
            await _context.SaveChangesAsync();

            return permissionTicketDb.Id;
        }
    }
}