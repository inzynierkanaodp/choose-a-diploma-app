﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class TopicOfWorkRepository : ITopicOfWorkRepository
    {
        private readonly DataContext _context;

        public TopicOfWorkRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TopicOfWorkDb>> GetTopicsOfWorks()
        {
            var topicOfWorks = await _context.TopicsOfWorks
                .Include(a => a.Account)
                .Include(sf => sf.StudyField)
                    .ThenInclude(f => f.Faculty).ToListAsync();

            return topicOfWorks;
        }

        public async Task<IEnumerable<TopicOfWorkDb>> GetTopicsOfWorks(int accountId)
        {
            var topicOfWorks = await _context.TopicsOfWorks
                .Include(a => a.Account)
                .Include(sf => sf.StudyField)
                .ThenInclude(f => f.Faculty).Where(x => x.IdAccount == accountId).ToListAsync();

            return topicOfWorks;
        }

        public async Task<TopicOfWorkDb> GetTopicOfWork(int id)
        {
            var topicOfWork = await _context.TopicsOfWorks
                .Include(a => a.Account)
                .Include(s => s.IssueSemesters)
                .Include(sf => sf.StudyField)
                .ThenInclude(f => f.Faculty).FirstOrDefaultAsync(tow => tow.Id == id);

            return topicOfWork;
        }

        public async Task<int> SaveTopicOfWork(TopicOfWorkDb topicOfWorkDb)
        {
            await _context.TopicsOfWorks.AddAsync(topicOfWorkDb);
            await _context.SaveChangesAsync();

            return topicOfWorkDb.Id;
        }

        public async Task<int> UpdateTopicOfWork(TopicOfWorkDb topicOfWorkDb)
        {
            await _context.SaveChangesAsync();

            return topicOfWorkDb.Id;
        }
    }
}