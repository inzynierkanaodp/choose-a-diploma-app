﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class StudyFieldRepository : IStudyFieldRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public StudyFieldRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<StudyFieldDb>> GetStudyFields()
        {
            var studyFields = await _context.StudyFields
                .Include(f => f.Faculty).ToListAsync();

            return studyFields;
        }
    }
}
