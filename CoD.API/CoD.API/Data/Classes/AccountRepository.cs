﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using AutoMapper;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Classes
{
    public class AccountRepository : IAccountRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public AccountRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<IEnumerable<AccountDb>> GetAccounts()
        {
            var users = await _context.Accounts.ToListAsync();

            return users;
        }

        public IEnumerable<AccountDb> GetAccountsN()
        {
            var users = _context.Accounts.ToList();

            return users;
        }

        public async Task<AccountDb> GetAccount(int id)
        {
            var user = await _context.Accounts.Where(u => u.Id == id)
                .Include(sem => sem.AccountStudyInfo)
                    .ThenInclude(s => s.Semester)
                .Include(sf => sf.AccountStudyInfo)
                    .ThenInclude(sf => sf.StudyField)
                        .ThenInclude(f => f.Faculty)
                .FirstOrDefaultAsync();

            return user;
        }

        public async Task<bool> GetIsAccountActive(int id)
        {
            var user = await _context.Accounts.FirstOrDefaultAsync(u => u.Id == id);

            return user.IsActive;
        }

        public async Task<IEnumerable<PermissionDb>> GetPermissionForAccount(int id)
        {
            List<PermissionDb> permissions = new List<PermissionDb>();
            var permissionAccount = await _context.PermissionsAccounts.FirstOrDefaultAsync(pa => pa.IdAccount == id);

            if(permissionAccount != null)
                permissions = await _context.Permissions.Where(p => p.Id == permissionAccount.IdPermission).ToListAsync();

            return permissions;
        }

        public async Task<AccountDb> ActiveAccount(int id)
        {
            var account = await GetAccount(id);
            account.IsActive = true;

            await SaveAll();

            return account;
        }
    }
}