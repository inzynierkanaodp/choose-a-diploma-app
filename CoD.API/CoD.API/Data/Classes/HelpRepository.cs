﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class HelpRepository : IHelpRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public HelpRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TicketDb>> GetTickets()
        {
            var ticketDbs = await _context.Tickets
                .Include(pt => pt.PermissionsTickets)
                    .ThenInclude(p => p.Permission)
                .Include(a => a.Account).ToListAsync();

            return ticketDbs;
        }

        public async Task<TicketDb> GetTicket(int id)
        {
            var ticketDb = await _context.Tickets
                .Include(pt => pt.PermissionsTickets)
                .ThenInclude(p => p.Permission)
                .Include(a => a.Account).FirstOrDefaultAsync(x => x.Id == id);

            return ticketDb;
        }

        public async Task<int> SaveTicket(TicketDb ticketDb)
        {
            await _context.Tickets.AddAsync(ticketDb);
            await _context.SaveChangesAsync();
            
            return ticketDb.Id;
        }
    }
}