﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class GroupsTopicsRepository : IGroupsTopicsRepository
    {
        private readonly DataContext _context;

        public GroupsTopicsRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GroupTopicDb>> GetGroupsTopics()
        {
            var groupTopicDbs = await _context.GroupsTopics.ToListAsync();

            return groupTopicDbs;
        }

        public async Task<int> ReserveTopicOfWork(GroupTopicDb groupTopicDb)
        {
            await _context.GroupsTopics.AddAsync(groupTopicDb);
            await _context.SaveChangesAsync();

            return groupTopicDb.Id;
        }

        public async Task<GroupTopicDb> GetGroupTopic(int topicId, int groupId)
        {
            return await _context.GroupsTopics.FirstOrDefaultAsync(x => x.IdTopicOfWork == topicId && x.IdGroup == groupId);
        }

        public async Task ResignGroupTopic(GroupTopicDb groupTopicDb)
        {
            _context.GroupsTopics.Remove(groupTopicDb);
            await _context.SaveChangesAsync();
        }
    }
}