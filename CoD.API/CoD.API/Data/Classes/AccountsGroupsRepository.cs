﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class AccountsGroupsRepository : IAccountsGroupsRepository
    {
        private readonly DataContext _context;

        public AccountsGroupsRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<AccountGroupDb>> GetAccountsGroups()
        {
            var accountGroupDbs = await _context.AccountsGroups.ToListAsync();

            return accountGroupDbs;
        }

        public async Task<AccountGroupDb> GetAccountGroup(int id)
        {
            var accountGroupDb = await _context.AccountsGroups.FirstOrDefaultAsync(x => x.Id == id);

            return accountGroupDb;
        }

        public async Task<AccountGroupDb> GetAccountGroupByAccountId(int accountId)
        {
            var accountGroupDb = await _context.AccountsGroups.FirstOrDefaultAsync(x => x.IdAccount == accountId);

            return accountGroupDb;
        }

        public async Task<AccountGroupDb> JoinGroup(
            AccountGroupDb accountGroupDb, 
            GroupDb groupDb = null, 
            string password = null)
        {
            if(groupDb != null && password !=null)
                if (!VerifyPasswordHash(
                    password, groupDb?.PasswordHash, 
                    groupDb?.PasswordSalt))
                    return null;

            await _context.AccountsGroups.AddAsync(accountGroupDb);
            await _context.SaveChangesAsync();

            return accountGroupDb;
        }

        public async Task<bool> CanJoinGroup(int accountId)
        {
            var accountGroup = await _context.AccountsGroups.FirstOrDefaultAsync(u => u.IdAccount == accountId);

            return accountGroup == null;
        }

        public async Task RemoveMember(AccountGroupDb accountGroupDb)
        {
            _context.AccountsGroups.Remove(accountGroupDb);
            await _context.SaveChangesAsync();
        }

        public bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }

                return true;
            }
        }
    }
}