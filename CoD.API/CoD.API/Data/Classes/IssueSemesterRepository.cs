﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class IssueSemesterRepository : IIssueSemesterRepository
    {
        private readonly DataContext _context;

        public IssueSemesterRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<IssueSemesterDb>> GetIssueSemesters()
        {
            var issueSemesters = await _context.IssueSemesters.ToListAsync();

            return issueSemesters;
        }
    }
}