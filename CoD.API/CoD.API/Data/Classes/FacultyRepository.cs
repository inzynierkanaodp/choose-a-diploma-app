﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class FacultyRepository : IFacultyRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public FacultyRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<FacultyDb>> GetFaculties()
        {
            var faculties = await _context.Faculties.ToListAsync();

            return faculties;
        }
    }
}