﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class GroupRepository : IGroupRepository
    {
        private readonly DataContext _context;

        public GroupRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GroupDb>> GetGroups()
        {
            var groups = await _context.Groups
                .Include(gt => gt.GroupsTopics)
                    .ThenInclude(tow => tow.TopicOfWork)
                        .ThenInclude(sf => sf.StudyField)
                            .ThenInclude(f => f.Faculty)
                .Include(ag => ag.AccountsGroups)
                    .ThenInclude(a => a.Account).ToListAsync();

            return groups;
        }

        public async Task<GroupDb> GetGroup(int id)
        {
            var group = await _context.Groups
                .Include(gt => gt.GroupsTopics)
                .ThenInclude(tow => tow.TopicOfWork)
                .ThenInclude(sf => sf.StudyField)
                .ThenInclude(f => f.Faculty)
                .Include(ag => ag.AccountsGroups)
                .ThenInclude(a => a.Account).FirstOrDefaultAsync(x => x.Id == id);

            return group;
        }

        public async Task<GroupDb> GetGroupByAccountId(int accountId)
        {
            var group = await _context.Groups
                .Include(gt => gt.GroupsTopics)
                .ThenInclude(tow => tow.TopicOfWork)
                .ThenInclude(sf => sf.StudyField)
                .ThenInclude(f => f.Faculty)
                .Include(ag => ag.AccountsGroups)
                .ThenInclude(a => a.Account).FirstOrDefaultAsync(x => x.IdAccount == accountId);

            return group;
        }

        public async Task<GroupDb> CreateGroup(GroupDb groupDb, string password)
        {
            CreatePasswordHash(password, out var passwordHash, out var passwordSalt);

            groupDb.PasswordSalt = passwordSalt;
            groupDb.PasswordHash = passwordHash;

            await _context.Groups.AddAsync(groupDb);
            await _context.SaveChangesAsync();

            return groupDb;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}