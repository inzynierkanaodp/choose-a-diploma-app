﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Classes
{
    public class AccountStudyInfoRepository : IAccountStudyInfoRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public AccountStudyInfoRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> SaveAccountStudyInfo(AccountStudyInfoDb accountStudyInfoDb)
        {
            await _context.AccountsStudiesInfos.AddAsync(accountStudyInfoDb);
            await _context.SaveChangesAsync();

            return accountStudyInfoDb.Id;
        }

        public async Task SaveAccountStudyInfos(List<AccountStudyInfoDb> accountStudyInfoDbs)
        {
            await _context.AccountsStudiesInfos.AddRangeAsync(accountStudyInfoDbs);
            await _context.SaveChangesAsync();
        }
    }
}