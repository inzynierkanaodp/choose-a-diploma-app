﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.Enums;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;

namespace CoD.WebApi.Data.Classes
{
    public class PermissionRepository : IPermissionRepository
    {
        private readonly DataContext _context;

        public PermissionRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<PermissionDb>> GetPermissions()
        {
            var roles = await _context.Permissions.ToListAsync();

            return roles;
        }

        public async Task<PermissionDb> GetPermission(PermissionName permissionName)
        {
            var role = await _context.Permissions.FirstOrDefaultAsync(x => x.Name == permissionName.ToString());

            return role;
        }

        public async Task<int> SavePermissionAccount(PermissionAccountDb permissionAccountDb)
        {
            await _context.PermissionsAccounts.AddAsync(permissionAccountDb);
            await _context.SaveChangesAsync();

            return permissionAccountDb.Id;
        }
    }
}