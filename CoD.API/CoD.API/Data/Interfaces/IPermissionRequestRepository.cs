﻿using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IPermissionTicketRepository
    {
        Task<int> SavePermissionTicket(PermissionTicketDb permissionTicketDb);
    }
}