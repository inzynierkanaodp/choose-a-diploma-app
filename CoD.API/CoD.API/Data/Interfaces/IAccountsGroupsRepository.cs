﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IAccountsGroupsRepository
    {
        Task<IEnumerable<AccountGroupDb>> GetAccountsGroups();
        Task<AccountGroupDb> GetAccountGroup(int id);
        Task<AccountGroupDb> GetAccountGroupByAccountId(int accountId);
        Task<AccountGroupDb> JoinGroup(AccountGroupDb accountGroupDb, GroupDb groupDb = null, string password = null);
        Task<bool> CanJoinGroup(int accountId);
        Task RemoveMember(AccountGroupDb accountGroupDb);
    }
}