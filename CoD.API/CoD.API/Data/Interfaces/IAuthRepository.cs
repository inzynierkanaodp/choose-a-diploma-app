﻿using System.Threading.Tasks;
using CoD.WebApi.DbModels;
using CoD.WebApi.Models;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IAuthRepository
    {
        Task<AccountDb> Register(AccountDb account, string password);
        Task<bool> SetNewPassword(AccountDb account, string password);
        Task<AccountDb> Login(string login, string password);
        Task<bool> AccountExists(string username = null, string email = null);
        Task<AccountDb> GetAccountByLoginOrEmail(string login = null, string email = null);
    }
}