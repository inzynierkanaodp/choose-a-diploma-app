﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IAccountStudyInfoRepository
    {
        Task<int> SaveAccountStudyInfo(AccountStudyInfoDb accountStudyInfoDb);
        Task SaveAccountStudyInfos(List<AccountStudyInfoDb> accountStudyInfoDbs);
    }
}