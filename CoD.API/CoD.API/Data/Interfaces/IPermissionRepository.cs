﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.Enums;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IPermissionRepository
    {
        Task<IEnumerable<PermissionDb>> GetPermissions();
        Task<PermissionDb> GetPermission(PermissionName permissionName);
        Task<int> SavePermissionAccount(PermissionAccountDb permissionAccountDb);
    }
}