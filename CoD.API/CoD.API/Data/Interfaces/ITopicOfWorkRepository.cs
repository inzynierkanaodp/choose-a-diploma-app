﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface ITopicOfWorkRepository
    {
        Task<IEnumerable<TopicOfWorkDb>> GetTopicsOfWorks();
        Task<IEnumerable<TopicOfWorkDb>> GetTopicsOfWorks(int accountId);
        Task<TopicOfWorkDb> GetTopicOfWork(int id);
        Task<int> SaveTopicOfWork(TopicOfWorkDb topicOfWorkDb);
        Task<int> UpdateTopicOfWork(TopicOfWorkDb topicOfWorkDb);
    }
}