﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IGroupsTopicsRepository
    {
        Task<IEnumerable<GroupTopicDb>> GetGroupsTopics();
        Task<int> ReserveTopicOfWork(GroupTopicDb groupTopicDb);
        Task<GroupTopicDb> GetGroupTopic(int topicId, int groupId);
        Task ResignGroupTopic(GroupTopicDb groupTopicDb);
    }
}