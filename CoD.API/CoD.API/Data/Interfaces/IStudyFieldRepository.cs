﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IStudyFieldRepository
    {
        Task<IEnumerable<StudyFieldDb>> GetStudyFields();
    }
}

