﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.DbModels;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IAccountRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<IEnumerable<AccountDb>> GetAccounts();
        IEnumerable<AccountDb> GetAccountsN();
        Task<AccountDb> GetAccount(int id);
        Task<bool> GetIsAccountActive(int id);
        Task<IEnumerable<PermissionDb>> GetPermissionForAccount(int id);
        Task<AccountDb> ActiveAccount(int id);
    }
}