﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IGroupRepository
    {
        Task<IEnumerable<GroupDb>> GetGroups();
        Task<GroupDb> GetGroup(int id);
        Task<GroupDb> CreateGroup(GroupDb groupDb, string password);
        Task<GroupDb> GetGroupByAccountId(int accountId);
        Task<bool> SaveAll();
    }
}