﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoD.WebApi.ModelsForDto;

namespace CoD.WebApi.Data.Interfaces
{
    public interface IHelpRepository
    {
        Task<IEnumerable<TicketDb>> GetTickets();
        Task<TicketDb> GetTicket(int id);
        Task<int> SaveTicket(TicketDb ticketDb);
    }
}