﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoD.WebApi.DbModels;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace CoD.WebApi.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
            
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Account

            builder.Entity<AccountDb>()
                .HasKey(c => c.Id);

            builder.Entity<AccountDb>()
                .HasIndex(a => a.Login)
                .IsUnique();

            builder.Entity<AccountDb>()
                .HasIndex(a => a.Email)
                .IsUnique();

            builder.Entity<AccountDb>()
                .HasIndex(a => a.IndexNumber)
                .IsUnique();

            #endregion

            #region IssueSemester

            builder.Entity<IssueSemesterDb>()
                .HasKey(c => c.Id);

            #endregion

            #region AccountGroup

            builder.Entity<AccountGroupDb>()
                .HasKey(ag => ag.Id);

            builder.Entity<AccountGroupDb>()
                .HasOne(ag => ag.Account)
                .WithMany(a => a.AccountsGroups)
                .HasForeignKey(ag => ag.IdAccount);

            builder.Entity<AccountGroupDb>()
                .HasOne(ag => ag.Group)
                .WithMany(g => g.AccountsGroups)
                .HasForeignKey(ag => ag.IdGroup);

            #endregion

            #region Permission

            builder.Entity<PermissionDb>()
                .HasKey(c => c.Id);

            builder.Entity<PermissionDb>()
                .HasIndex(p => p.Name)
                .IsUnique();

            #endregion

            #region PermissionsAccounts

            builder.Entity<PermissionAccountDb>()
                .HasKey(c => c.Id);

            builder.Entity<PermissionAccountDb>()
                .HasOne(pa => pa.Permission)
                .WithMany(p => p.PermissionsAccounts)
                .HasForeignKey(pa => pa.IdPermission);

            builder.Entity<PermissionAccountDb>()
                .HasOne(pa => pa.Account)
                .WithMany(a => a.PermissionsAccounts)
                .HasForeignKey(pa => pa.IdAccount);

            #endregion

            #region PermissionsTickets

            builder.Entity<PermissionTicketDb>()
                .HasKey(c => c.Id);

            builder.Entity<PermissionTicketDb>()
                .HasOne(pt => pt.Permission)
                .WithMany(p => p.PermissionsTickets)
                .HasForeignKey(pt => pt.IdPermission);

            builder.Entity<PermissionTicketDb>()
                .HasOne(pt => pt.Ticket)
                .WithMany(t => t.PermissionsTickets)
                .HasForeignKey(pt => pt.IdTicket);

            #endregion

            #region Faculties

            builder.Entity<FacultyDb>()
                .HasKey(c => c.Id);

            #endregion

            #region StudyFields

            builder.Entity<StudyFieldDb>()
                .HasKey(c => c.Id);

            builder.Entity<StudyFieldDb>()
                .HasOne(sf => sf.Faculty)
                .WithMany(f => f.StudyFields)
                .HasForeignKey(sf => sf.IdFaculty);

            #endregion

            #region TopicsOfWorks

            builder.Entity<TopicOfWorkDb>()
                .HasKey(c => c.Id);

            builder.Entity<TopicOfWorkDb>()
                .HasOne(tow => tow.Account)
                .WithMany(a => a.TopicsOfWorks)
                .HasForeignKey(tow => tow.IdAccount);

            builder.Entity<TopicOfWorkDb>()
                .HasOne(tow => tow.StudyField)
                .WithMany(sf => sf.TopicsOfWorks)
                .HasForeignKey(tow => tow.IdStudyField);

            builder.Entity<TopicOfWorkDb>()
                .HasOne(tow => tow.IssueSemesters)
                .WithMany(iss => iss.TopicsOfWorks)
                .HasForeignKey(tow => tow.IdSemester);

            #endregion

            #region Tickets

            builder.Entity<TicketDb>()
                .HasKey(c => c.Id);

            builder.Entity<TicketDb>()
                .HasOne(t => t.Account)
                .WithMany(a => a.Tickets)
                .HasForeignKey(t => t.IdAccount);

            #endregion

            #region GroupsTopics

            builder.Entity<GroupTopicDb>()
                .HasKey(c => c.Id);

            builder.Entity<GroupTopicDb>()
                .HasOne(gt => gt.TopicOfWork)
                .WithMany(tow => tow.GroupsTopics)
                .HasForeignKey(gt => gt.IdTopicOfWork);

            builder.Entity<GroupTopicDb>()
                .HasOne(gt => gt.Group)
                .WithMany(g => g.GroupsTopics)
                .HasForeignKey(gt => gt.IdGroup);

            #endregion

            #region Groups
            builder.Entity<GroupDb>()
                .HasKey(g => g.Id);

            builder.Entity<GroupDb>()
                .HasIndex(g => g.IdAccount)
                .IsUnique();

            #endregion

            #region Configuration

            builder.Entity<ConfigurationDb>()
                .HasKey(c => c.Id);

            builder.Entity<ConfigurationDb>()
                .HasIndex(c => c.Key)
                .IsUnique();

            builder.Entity<ConfigurationDb>()
                .HasIndex(c => c.Value)
                .IsUnique();

            #endregion

            #region AccountsStudiesInfos

            builder.Entity<AccountStudyInfoDb>()
                .HasKey(a => a.Id);

            builder.Entity<AccountStudyInfoDb>()
                .HasOne(sf => sf.StudyField)
                .WithMany(asi => asi.AccountStudyInfo)
                .HasForeignKey(sf => sf.IdStudyField);

            builder.Entity<AccountStudyInfoDb>()
                .HasOne(iss => iss.Semester)
                .WithMany(asi => asi.AccountStudyInfo)
                .HasForeignKey(iss => iss.IdSemester);

            builder.Entity<AccountStudyInfoDb>()
                .HasOne(a => a.Account)
                .WithMany(asi => asi.AccountStudyInfo)
                .HasForeignKey(a => a.IdAccount);

            #endregion
        }



        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //    => optionsBuilder.UseNpgsql("Host=localhost;port=57519;Database=CoD;Username=postgres;Password=admin1!");

        #region DB Tables

        //Table Name in DB
        public DbSet<AccountDb> Accounts { get; set; } 
        public DbSet<IssueSemesterDb> IssueSemesters { get; set; }
        public DbSet<AccountGroupDb> AccountsGroups { get; set; }
        public DbSet<GroupDb> Groups { get; set; }
        public DbSet<PermissionDb> Permissions { get; set; }
        public DbSet<PermissionAccountDb> PermissionsAccounts { get; set; }
        public DbSet<FacultyDb> Faculties { get; set; }
        public DbSet<StudyFieldDb> StudyFields { get; set; }
        public DbSet<TopicOfWorkDb> TopicsOfWorks { get; set; }
        public DbSet<TicketDb> Tickets { get; set; }
        public DbSet<GroupTopicDb> GroupsTopics { get; set; }
        public DbSet<ConfigurationDb> Configuration { get; set; }
        public DbSet<AccountStudyInfoDb> AccountsStudiesInfos { get; set; }
        public DbSet<PermissionTicketDb> PermissionsTickets { get; set; }
        #endregion
    }
}
