﻿using System;
using Autofac;
using CoD.Common.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CoD.Common
{
    public class CommonModule : ModuleBase
    {
        public IConfigurationSection Configuration { get; }

        public CommonModule(IConfigurationSection configuration) : base(configuration)
        {
            Configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {

        }
    }
}
