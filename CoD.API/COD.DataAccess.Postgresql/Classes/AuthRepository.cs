﻿using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CoD.Business.Interfaces;
using CoD.Business.Interfaces.Classes.Models;

namespace CoD.Business.Classes
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;

        public AuthRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Account> Register(Account account, string password)
        {
            CreatePasswordHash(password, out var passwordHash, out var passwordSalt);

            account.PasswordSalt = passwordSalt;
            account.PasswordHash = passwordHash;

            await _context.Accounts.AddAsync(account);
            await _context.SaveChangesAsync();

            return account;
        }

        public async Task<Account> Login(string login, string password)
        {
            var user = await _context.Accounts.FirstOrDefaultAsync(x => x.Login == login);

            if (user == null)
                return null;

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        public async Task<bool> AccountExists(string login)
        {
            if (await _context.Accounts.AnyAsync(x => x.Login == login))
                return true;

            return false;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }

                return true;
            }
        }
    }
}