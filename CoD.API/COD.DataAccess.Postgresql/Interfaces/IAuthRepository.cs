﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Models;

namespace CoD.Business.Interfaces
{
    public interface IAuthRepository
    {
        Task<Account> Register(Account account, string password);
        Task<Account> Login(string login, string password);
        Task<bool> AccountExists(string username);
    }
}