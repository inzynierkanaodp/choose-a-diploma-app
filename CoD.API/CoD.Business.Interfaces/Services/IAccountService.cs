﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;

namespace CoD.Business.Interfaces.Services
{
    public interface IAccountService
    {
        Task<IsActiveAccountResult> IsActiveAccount(IsActiveParameter isActiveParameter);
        Task<GetAccountResult> GetAccount(GetAccountParameter getAccountParameter);
        Task<GetAccountLoginDetailsResult> GetAccountLoginDetails(GetAccountParameter getAccountParameter);
        Task<UpdateAccountResult> UpdateAccount(UpdateAccountParameter updateAccountParameter);
        Task<AccountForResetPasswordResult> ActiveAccount(IsActiveParameter isActiveParameter);
    }
}