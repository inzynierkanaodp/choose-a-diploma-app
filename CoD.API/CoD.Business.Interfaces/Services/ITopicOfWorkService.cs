﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;

namespace CoD.Business.Interfaces.Services
{
    public interface ITopicOfWorkService
    {
        Task<GetTopicsOfWorksResult> GetTopicsOfWorks();
        Task<GetTopicsOfWorksResult> GetTopicsOfWorks(int accountId);
        Task<GetTopicOfWorkResult> GetTopicOfWork(int id);
        Task<SaveTopicOfWorkResult> SaveTopicOfWork(SaveTopicOfWorkParameter saveTicketParameter);
        Task<ReserveTopicOfWorkResult> ReserveTopicOfWork(ReserveTopicOfWorkParameter reserveTopicOfWorkParameter);
        Task<ReserveTopicOfWorkResult> ResignTopicOfWork(ReserveTopicOfWorkParameter reserveTopicOfWorkParameter);
        Task<IsActiveAccountResult> IsOwnerThisTopic(ReserveTopicOfWorkParameter reserveTopicOfWorkParameter);
    }
}