﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;

namespace CoD.Business.Interfaces.Services
{
    public interface IFacultyService
    {
        Task<GetFacultiesResult> GetFaculties();
    }
}