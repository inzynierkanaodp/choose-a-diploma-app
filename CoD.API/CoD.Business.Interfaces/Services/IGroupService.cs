﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;

namespace CoD.Business.Interfaces.Services
{
    public interface IGroupService
    {
        Task<GetGroupsResult> GetGroups();
        Task<GetGroupResult> GetGroup(int id);
        Task<GetAccountsResult> GetGroupMembers(int id);
        Task<GetAccountGroupIdResult> GetAccountGroupId(int accountId);
        Task<CreateGroupResult> CreateGroup(CreateGroupParameter createGroupParameter);
        Task<JoinGroupResult> JoinGroup(JoinGroupParameter createGroupParameter);
        Task<CanJoinGroupResult> CanJoinGroup(CanJoinGroupParameter canJoinGroupParameter);
        Task<IsActiveAccountResult> CanAccountReserveTopic(IsActiveParameter isActiveParameter);
        Task ArchiveGroup(int id);
        Task RemoveGroupMember(int accountId);
        Task<bool> IsGroupLeader(int accountId, int groupId);
    }
}