﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;

namespace CoD.Business.Interfaces.Services
{
    public interface IHelpService
    {
        Task<GetAdminPanelInfoResult> GetInformationForAdminPanel();
        Task<GetTicketsResult> GetTickets();
        Task<GetTicketsResult> GetTicket(int id);
        Task<SaveTicketResult> SaveTicket(SaveTicketParameter registerParameter);
        Task<SaveTicketResult> ChangeTicketStatus(ChangeTicketStatusParameter changeTicketStatusParameter);
    }
}