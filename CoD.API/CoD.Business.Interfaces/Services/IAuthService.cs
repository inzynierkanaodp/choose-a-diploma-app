﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;

namespace CoD.Business.Interfaces.Services
{
    public interface IAuthService
    {
        Task<RegisterResult> RegisterAccount(RegisterParameter registerParameter);
        Task<AccountResetPasswordResult> ResetAccountPassword(ResetAccountPasswordParameter resetParameter);
        Task<AccountResetEmailResult> ResetAccountEmail(ResetAccountEmailParameter resetAccountPasswordParameter);
        Task<AccountResetLoginResult> ResetAccountLogin(ResetAccountLoginParameter resetAccountLoginParameter);
        Task<LoginResult> LoginAccount(LoginParameter loginParameter);
    }
}