﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CoD.Business.Interfaces.Classes.Helpers
{
    public static class Extensions
    {
        public static string GetErrorMessages(ModelStateDictionary modelStateDictionary)
        {
            string messages = "";

            if (!modelStateDictionary.IsValid)
            {
                foreach (var modelState in modelStateDictionary.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        messages += error.ErrorMessage + " ";
                    }
                }
            }

            return messages;
        }
    }
}
