﻿using System.Collections.Generic;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class GetStudyFieldsWithFacultiesResult
    {
        public IList<StudyFieldWithFacultyInfo> StudyFieldWithFacultyInfo { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetStudyFieldsWithFacultiesResult(IList<StudyFieldWithFacultyInfo> studyFieldWithFacultyInfo, bool isSuccess, string info)
        {
            StudyFieldWithFacultyInfo = studyFieldWithFacultyInfo;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}