﻿using System;
using System.Collections.Generic;
using System.Text;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetGroupsResult
    {
        public IList<GroupTable> GroupTables { get; set; }
        public int QuantityOfGroups { get; set; }
        public int GroupsWithTopicQuantity { get; set; }
        public int GroupsWithoutTopicQuantity { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetGroupsResult(IList<GroupTable> groupTables, int quantityOfGroups, int groupsWithTopicQuantity, int groupsWithoutTopicQuantity, bool isSuccess, string info)
        {
            GroupTables = groupTables;
            QuantityOfGroups = quantityOfGroups;
            GroupsWithTopicQuantity = groupsWithTopicQuantity;
            GroupsWithoutTopicQuantity = groupsWithoutTopicQuantity;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}
