﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class AccountResetPasswordResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public AccountResetPasswordResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}