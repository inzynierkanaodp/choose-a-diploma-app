﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class AccountResetLoginResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public AccountResetLoginResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}