﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class UpdateAccountResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public UpdateAccountResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}