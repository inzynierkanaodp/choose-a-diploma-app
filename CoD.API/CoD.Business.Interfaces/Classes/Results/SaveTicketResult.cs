﻿using CoD.WebApi.DbModels;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class SaveTicketResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public SaveTicketResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}