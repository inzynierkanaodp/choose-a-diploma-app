﻿using System.Collections.Generic;
using CoD.WebApi.Migrations;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetAccountsResult
    {
        public List<AccountForUpdate> Accounts { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public GetAccountsResult(List<AccountForUpdate> accounts, bool isSuccess, string info)
        {
            Accounts = accounts;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}