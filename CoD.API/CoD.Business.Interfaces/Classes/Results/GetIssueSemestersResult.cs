﻿using System.Collections.Generic;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class GetIssueSemestersResult
    {
        public IList<IssueSemesterInfo> IssueSemesters { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetIssueSemestersResult(IList<IssueSemesterInfo> issueSemesters, bool isSuccess, string info)
        {
            IssueSemesters = issueSemesters;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}