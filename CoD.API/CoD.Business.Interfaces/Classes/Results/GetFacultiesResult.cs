﻿using System.Collections.Generic;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class GetFacultiesResult
    {
        public IList<FacultyInfo> Faculties { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetFacultiesResult(IList<FacultyInfo> faculties, bool isSuccess, string info)
        {
            Faculties = faculties;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}