﻿using System.Collections.Generic;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetPermissionsResult
    {
        public IList<PermissionInfo> PermissionInfos { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetPermissionsResult(IList<PermissionInfo> permissionInfos, bool isSuccess, string info)
        {
            PermissionInfos = permissionInfos;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}