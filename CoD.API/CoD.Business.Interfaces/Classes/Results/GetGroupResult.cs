﻿using System.Collections.Generic;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetGroupResult
    {
        public GroupTable GroupTable { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetGroupResult(GroupTable groupTable, bool isSuccess, string info)
        {
            GroupTable = groupTable;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}