﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetAccountGroupIdResult
    {
        public int? IdGroup { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public GetAccountGroupIdResult(int? idGroup, bool isSuccess, string info)
        {
            IdGroup = idGroup;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}