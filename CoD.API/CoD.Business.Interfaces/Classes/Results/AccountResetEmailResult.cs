﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class AccountResetEmailResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public AccountResetEmailResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}