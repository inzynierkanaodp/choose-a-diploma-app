﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class ReserveTopicOfWorkResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public ReserveTopicOfWorkResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}