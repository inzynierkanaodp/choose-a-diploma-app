﻿using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetAdminPanelInfoResult
    {
        public AdminPanelInfo AdminPanelInfo { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public GetAdminPanelInfoResult(AdminPanelInfo adminPanelInfo, bool isSuccess, string info)
        {
            AdminPanelInfo = adminPanelInfo;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}