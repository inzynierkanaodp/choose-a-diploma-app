﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class CanJoinGroupResult
    {
        public bool CanJoinGroup { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public CanJoinGroupResult(bool canJoinGroup, bool isSuccess, string info)
        {
            CanJoinGroup = canJoinGroup;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}