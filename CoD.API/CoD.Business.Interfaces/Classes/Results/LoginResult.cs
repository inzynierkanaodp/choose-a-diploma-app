﻿using CoD.WebApi.DbModels;
using Microsoft.IdentityModel.Tokens;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class LoginResult
    {
        public SecurityToken SecurityToken { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public LoginResult(SecurityToken securityToken, bool isSuccess, string info)
        {
            SecurityToken = securityToken;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}