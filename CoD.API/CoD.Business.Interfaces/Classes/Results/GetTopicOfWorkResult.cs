﻿using System.Collections.Generic;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetTopicOfWorkResult
    {
        public TopicOfWorkTable TopicOfWork { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetTopicOfWorkResult(TopicOfWorkTable topicOfWork, bool isSuccess, string info)
        {
            TopicOfWork = topicOfWork;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}