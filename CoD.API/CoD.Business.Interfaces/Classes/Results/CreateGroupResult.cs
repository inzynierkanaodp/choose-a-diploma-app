﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class CreateGroupResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public CreateGroupResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}