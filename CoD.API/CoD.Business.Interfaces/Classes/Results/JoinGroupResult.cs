﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class JoinGroupResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public JoinGroupResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}