﻿using CoD.WebApi.DbModels;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetAccountResult
    {
        public AccountForUpdate Account { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public GetAccountResult(AccountForUpdate account, bool isSuccess, string info)
        {
            Account = account;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}