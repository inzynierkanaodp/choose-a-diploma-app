﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class AccountForResetPasswordResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public AccountForResetPasswordResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}