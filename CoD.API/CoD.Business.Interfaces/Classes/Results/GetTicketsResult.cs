﻿using System.Collections.Generic;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetTicketsResult
    {
        public IList<TicketTable> TicketTables { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetTicketsResult(IList<TicketTable> ticketTables, bool isSuccess, string info)
        {
            TicketTables = ticketTables;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}