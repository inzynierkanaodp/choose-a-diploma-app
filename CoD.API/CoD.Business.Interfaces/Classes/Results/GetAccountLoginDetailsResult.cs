﻿using CoD.WebApi.DbModels;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetAccountLoginDetailsResult
    {
        public AccountLoginDetails Account { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public GetAccountLoginDetailsResult(AccountLoginDetails account, bool isSuccess, string info)
        {
            Account = account;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}