﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class IsActiveAccountResult
    {
        public bool IsActiveUser { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public IsActiveAccountResult(bool isActiveUser, bool isSuccess, string info)
        {
            IsActiveUser = isActiveUser;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}