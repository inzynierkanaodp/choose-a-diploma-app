﻿using System.Collections.Generic;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class GetStudyFieldsResult
    {
        public IList<StudyFieldInfo> StudyFieldInfos { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetStudyFieldsResult(IList<StudyFieldInfo> studyFieldInfos, bool isSuccess, string info)
        {
            StudyFieldInfos = studyFieldInfos;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}