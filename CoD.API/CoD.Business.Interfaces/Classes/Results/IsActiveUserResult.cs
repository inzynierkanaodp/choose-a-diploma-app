﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class IsActiveUserResult
    {
        public bool IsActiveUser { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public IsActiveUserResult(bool isActiveUser, bool isSuccess, string info)
        {
            IsActiveUser = isActiveUser;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}