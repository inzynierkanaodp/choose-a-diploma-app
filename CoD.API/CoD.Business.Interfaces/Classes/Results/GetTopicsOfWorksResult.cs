﻿using System.Collections.Generic;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class GetTopicsOfWorksResult
    {
        public IList<TopicOfWorkTable> TopicOfWorkTables { get; set; }
        public int QuantityOfUnhiddenTopics { get; set; }
        public int QuantityUniqueOfPromoters { get; set; }
        public bool IsSuccess { get; set; }
        public string Info { get; set; }

        public GetTopicsOfWorksResult(IList<TopicOfWorkTable> topicOfWorkTables, int quantityOfUnhiddenTopics, int quantityUniqueOfPromoters, bool isSuccess, string info)
        {
            TopicOfWorkTables = topicOfWorkTables;
            QuantityOfUnhiddenTopics = quantityOfUnhiddenTopics; 
            QuantityUniqueOfPromoters = quantityUniqueOfPromoters;
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}