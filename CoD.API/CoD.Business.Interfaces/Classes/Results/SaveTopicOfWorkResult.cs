﻿namespace CoD.Business.Interfaces.Classes.Results
{
    public class SaveTopicOfWorkResult
    {
        public bool IsSuccess { get; set; }
        public string Info { get; set; }
        public SaveTopicOfWorkResult(bool isSuccess, string info)
        {
            IsSuccess = isSuccess;
            Info = info;
        }
    }
}