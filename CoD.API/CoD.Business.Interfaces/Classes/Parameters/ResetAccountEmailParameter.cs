﻿using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Results
{
    public class ResetAccountEmailParameter
    {
        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
        public string ConfirmNewEmail { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public ResetAccountEmailParameter(string oldEmail, string newEmail, string confirmNewEmail, string password, string confirmPassword)
        {
            OldEmail = oldEmail;
            NewEmail = newEmail;
            ConfirmNewEmail = confirmNewEmail;
            Password = password;
            ConfirmPassword = confirmPassword;
        }
    }
}