﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoD.Business.Interfaces.Classes
{
    public class RegisterParameter
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }
        public bool IsEmployee { get; set; }

        public RegisterParameter(string login, string password, string confirmPassword, string email, bool isEmployee)
        {
            Login = login;
            Password = password;
            ConfirmPassword = confirmPassword;
            Email = email;
            IsEmployee = isEmployee;
        }
    }
}
