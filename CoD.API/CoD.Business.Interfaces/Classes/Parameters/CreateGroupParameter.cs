﻿namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class CreateGroupParameter
    {
        public int AccountId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public CreateGroupParameter(int accountId, string password, string confirmPassword)
        {
            AccountId = accountId;
            Password = password;
            ConfirmPassword = confirmPassword;  
        }
    }
}