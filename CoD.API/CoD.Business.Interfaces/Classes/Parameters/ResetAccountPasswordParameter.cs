﻿using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class ResetAccountPasswordParameter
    {
        public AccountForResetPassword AccountForResetPassword { get; set; }

        public ResetAccountPasswordParameter(AccountForResetPassword accountForResetPassword)
        {
            AccountForResetPassword = accountForResetPassword;
        }
    }
}
