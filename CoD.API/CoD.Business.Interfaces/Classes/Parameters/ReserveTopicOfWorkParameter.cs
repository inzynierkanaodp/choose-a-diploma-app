﻿namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class ReserveTopicOfWorkParameter
    {
        public int TopicOfWorkId { get; set; }
        public int AccountId { get; set; }

        public ReserveTopicOfWorkParameter(int topicOfWorkId, int accountId)
        {
            TopicOfWorkId = topicOfWorkId;
            AccountId = accountId;
        }
    }
}