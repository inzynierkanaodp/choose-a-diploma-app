﻿using CoD.WebApi.Enums;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class ChangeTicketStatusParameter
    {
        public int TicketId { get; set; }
        public TicketStatus TicketStatus { get; set; }

        public ChangeTicketStatusParameter(int ticketId, TicketStatus ticketStatus)
        {
            TicketId = ticketId;
            TicketStatus = ticketStatus;
        }
    }
}