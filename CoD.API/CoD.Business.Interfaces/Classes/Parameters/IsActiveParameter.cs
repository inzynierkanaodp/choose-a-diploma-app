﻿namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class IsActiveParameter
    {
        public int Id { get; set; }

        public IsActiveParameter(int id)
        {
            Id = id;
        }
    }
}