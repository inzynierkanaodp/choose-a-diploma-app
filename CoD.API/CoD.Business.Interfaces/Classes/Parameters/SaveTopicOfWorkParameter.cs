﻿using System.Collections.Generic;
using CoD.WebApi.Controllers.Models;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class SaveTopicOfWorkParameter
    {
        public ThesisForAdd ThesisForAdd { get; set; }
        public int AccountId { get; set; }
        public List<IssueSemesterInfo> IssueSemesterInfos { get; set; }
        public List<StudyFieldInfo> StudyFieldWithFacultyInfos { get; set; }

        public SaveTopicOfWorkParameter(ThesisForAdd thesisForAdd, int accountId, List<IssueSemesterInfo> issueSemesterInfos, List<StudyFieldInfo> studyFieldWithFacultyInfos)
        {
            ThesisForAdd = thesisForAdd;
            AccountId = accountId;
            IssueSemesterInfos = issueSemesterInfos;
            StudyFieldWithFacultyInfos = studyFieldWithFacultyInfos;
        }
    }
}
       