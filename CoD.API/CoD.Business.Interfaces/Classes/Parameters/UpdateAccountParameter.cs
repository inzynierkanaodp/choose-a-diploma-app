﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class UpdateAccountParameter
    {
        public int AccountId { get; set; }
        public AccountForUpdate AccountForUpdate { get; set; }
        public ClaimsPrincipal User { get; set; }
        public List<IssueSemesterInfo> IssueSemesterInfos { get; set; }
        public List<StudyFieldWithFacultyInfo> StudyFieldWithFacultyInfos { get; set; }

        public UpdateAccountParameter(int accountId, AccountForUpdate accountForUpdate, ClaimsPrincipal user,
            List<IssueSemesterInfo> issueSemesterInfos, List<StudyFieldWithFacultyInfo> studyFieldWithFacultyInfos)
        {
            AccountId = accountId;
            AccountForUpdate = accountForUpdate;
            User = user;
            IssueSemesterInfos = issueSemesterInfos;
            StudyFieldWithFacultyInfos = studyFieldWithFacultyInfos;
        }
    }
}
