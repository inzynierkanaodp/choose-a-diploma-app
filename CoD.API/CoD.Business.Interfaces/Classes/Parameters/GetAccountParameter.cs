﻿namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class GetAccountParameter
    {
        public int Id { get; set; }

        public GetAccountParameter(int id)
        {
            Id = id;
        }
    }
}