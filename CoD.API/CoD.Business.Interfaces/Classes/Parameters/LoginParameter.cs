﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class LoginParameter
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public LoginParameter(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}
