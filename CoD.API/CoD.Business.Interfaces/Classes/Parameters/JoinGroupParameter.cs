﻿namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class JoinGroupParameter
    {
        public int AccountId { get; set; }
        public int GroupId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public JoinGroupParameter(int accountId, int groupId, string password, string confirmPassword)
        {
            AccountId = accountId;
            GroupId = groupId;
            Password = password;
            ConfirmPassword = confirmPassword;
        }
    }
}