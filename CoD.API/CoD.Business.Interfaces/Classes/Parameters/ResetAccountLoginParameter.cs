﻿namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class ResetAccountLoginParameter
    {
        public string OldLogin { get; set; }
        public string NewLogin { get; set; }
        public string ConfirmNewLogin { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public ResetAccountLoginParameter(string oldLogin, string newLogin, string confirmNewLogin, string password, string confirmPassword)
        {
            OldLogin = oldLogin;
            NewLogin = newLogin;
            ConfirmNewLogin = confirmNewLogin;
            Password = password;
            ConfirmPassword = confirmPassword;
        }
    }
}