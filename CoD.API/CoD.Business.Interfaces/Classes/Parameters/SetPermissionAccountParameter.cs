﻿using CoD.WebApi.Enums;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class SetPermissionAccountParameter
    {
        public int TicketId { get; set; }
        public PermissionName PermissionName { get; set; }

        public SetPermissionAccountParameter(int ticketId, PermissionName permissionName)
        {
            TicketId = ticketId;
            PermissionName = permissionName;
        }
    }
}