﻿namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class CanJoinGroupParameter
    {
        public int AccountId { get; set; }

        public CanJoinGroupParameter(int accountId)
        {
            AccountId = accountId;
        }
    }
}