﻿using System.Collections.Generic;
using CoD.WebApi.Models;

namespace CoD.Business.Interfaces.Classes.Parameters
{
    public class SaveTicketParameter
    {
        public List<PermissionInfo> PermissionInfos { get; set; }
        public int AccountId { get; set; }
        public string Title { get; set; }
        public string HelpMessage { get; set; }

        public SaveTicketParameter(List<PermissionInfo> permissionInfos, int accountId, string helpMessage, string title)
        {
            PermissionInfos = permissionInfos;
            AccountId = accountId;
            HelpMessage = helpMessage;
            Title = title;
        }
    }
}