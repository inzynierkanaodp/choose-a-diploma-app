﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoD.Common.Interfaces;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;

namespace CoD.WebApi.Interfaces
{
    public abstract class WebApiModule : ModuleBase
    {
        protected WebApiModule(IConfigurationSection configuration) : base(configuration)
        {
        }

        public virtual IEnumerable<IApplicationFeatureProvider> ApplicationFeatureProviders => Enumerable.Empty<IApplicationFeatureProvider>();
    }
}
