﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;

namespace CoD.WebApi.Interfaces
{
    public class AssemblyControllersFeatureProvider : IApplicationFeatureProvider<ControllerFeature>
    {
        public AssemblyControllersFeatureProvider(Assembly assembly)
        {
            Assembly = assembly;
        }

        public Assembly Assembly { get; }

        public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
        {
            var controllers = Assembly.GetTypes().Where(t => typeof(ControllerBase).IsAssignableFrom(t)).Select(t => t.GetTypeInfo());
            foreach (var item in controllers)
            {
                feature.Controllers.Add(item);
            }
        }
    }
}
