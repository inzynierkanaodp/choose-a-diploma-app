﻿using System;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Module = Autofac.Module;

namespace CoD.Common.Interfaces
{
    public abstract class ModuleBase : Module
    {
        protected ModuleBase(IConfigurationSection configuration)
        {

        }
    }
}
