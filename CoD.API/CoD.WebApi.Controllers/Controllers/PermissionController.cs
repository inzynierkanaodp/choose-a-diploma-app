﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Controllers.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        public IPermissionService PermissionService { get; }

        public PermissionController(IPermissionService permissionService)
        {
            PermissionService = permissionService;
        }

        [HttpGet("GetPermissions")]
        public async Task<IActionResult> GetPermissions()
        {
            var getIssueSemestersResult = await PermissionService.GetPermissions();

            if (getIssueSemestersResult.IsSuccess)
                return Ok(getIssueSemestersResult.PermissionInfos);

            return NotFound(getIssueSemestersResult.Info);
        }

        [HttpPost("SetPermissionAccount")]
        public async Task<IActionResult> SetPermissionAccount(SetPermissionAccountModel setPermissionAccountModel)
        {
            var isActiveUserResult = await PermissionService.SetPermissionAccount(
                new SetPermissionAccountParameter(setPermissionAccountModel.TicketId, 
                setPermissionAccountModel.PermissionName));

            if (isActiveUserResult.IsSuccess)
                return Ok();

            return Conflict(isActiveUserResult.Info);
        }
    }
}