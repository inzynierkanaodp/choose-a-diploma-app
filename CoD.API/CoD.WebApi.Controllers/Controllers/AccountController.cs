﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Controllers.Models;
using CoD.WebApi.Controllers.Models.User;
using CoD.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public IAccountService AccountService { get; }
        public IGroupService GroupService { get; }

        public AccountController(IAccountService accountService, IGroupService groupService)
        {
            AccountService = accountService;
            GroupService = groupService;
        }

        [HttpGet("IsActiveAccount/{id}")]
        public async Task<IActionResult> IsActiveAccount(int id)
        {
            var isActiveUserResult = await AccountService.IsActiveAccount(new IsActiveParameter(id));

            if (isActiveUserResult.IsSuccess)
                return Ok(isActiveUserResult.IsActiveUser);

            return Conflict(isActiveUserResult.Info);
        }

        [HttpGet("GetAccount/{id}")]
        public async Task<IActionResult> GetAccount(int id)
        {
            var getAccountResult = await AccountService.GetAccount(new GetAccountParameter(id));

            if (getAccountResult.IsSuccess)
                return Ok(getAccountResult);

            return NotFound(getAccountResult.Info);
        }

        [HttpGet("GetAccountLoginDetails/{id}")]
        public async Task<IActionResult> GetAccountLoginDetails(int id)
        {
            var getAccountResult = await AccountService.GetAccountLoginDetails(new GetAccountParameter(id));

            if (getAccountResult.IsSuccess)
                return Ok(getAccountResult);

            return NotFound(getAccountResult.Info);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser(AccountForUpdateModel accountForUpdateModel)
        {
            var updateAccountResult = await AccountService.UpdateAccount(
                new UpdateAccountParameter(accountForUpdateModel.AccountId, accountForUpdateModel.AccountForUpdate, 
                    User, accountForUpdateModel.IssueSemesterInfos, accountForUpdateModel.StudyFieldWithFacultyInfos)
                );

            if (updateAccountResult.IsSuccess)
                return Ok();

            return Conflict(updateAccountResult.Info);
        }

        [HttpGet("CanAccountReserveTopic/{id}")]
        public async Task<IActionResult> CanAccountReserveTopic(int id)
        {
            var isActiveUserResult = await GroupService.CanAccountReserveTopic(new IsActiveParameter(id));

            if (isActiveUserResult.IsSuccess)
                return Ok(isActiveUserResult.IsActiveUser);

            return Conflict(isActiveUserResult.Info);
        }

        [HttpPut("ActiveAccount")]
        public async Task<IActionResult> ActiveAccount(ActivateAccountModel activateAccountModel)
        {
            var isActiveUserResult = await AccountService.ActiveAccount(new IsActiveParameter(activateAccountModel.TicketId));

            if (isActiveUserResult.IsSuccess)
                return Ok();

            return Conflict(isActiveUserResult.Info);
        }
    }
}

