﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FacultyController : ControllerBase
    {
        public IFacultyService FacultyService { get; }

        public FacultyController(IFacultyService facultyService)
        {
            FacultyService = facultyService;
        }

        [HttpGet("GetFaculties")]
        public async Task<IActionResult> GetFaculties()
        {
            var getFacultiesResult = await FacultyService.GetFaculties();

            if (getFacultiesResult.IsSuccess)
                return Ok(getFacultiesResult.Faculties);

            return NotFound(getFacultiesResult.Info);
        }
    }
}