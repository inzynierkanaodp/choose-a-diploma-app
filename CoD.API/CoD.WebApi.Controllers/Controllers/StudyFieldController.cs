﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StudyFieldController : ControllerBase
    {
        public IStudyFieldService StudyFieldService { get; }

        public StudyFieldController(IStudyFieldService studyFieldService)
        {
            StudyFieldService = studyFieldService;
        }

        [HttpGet("GetStudyFields")]
        public async Task<IActionResult> GetStudyFields()
        {
            var getStudyFieldsResult = await StudyFieldService.GetStudyFields();

            if (getStudyFieldsResult.IsSuccess)
                return Ok(getStudyFieldsResult.StudyFieldInfos);

            return NotFound(getStudyFieldsResult.Info);
        }

        [HttpGet("GetStudyFieldsWithFacultiesInfo")]
        public async Task<IActionResult> GetStudyFieldsWithFacultiesInfo()
        {
            var getStudyFieldsResult = await StudyFieldService.GetStudyFieldsWithFaculties();

            if (getStudyFieldsResult.IsSuccess)
                return Ok(getStudyFieldsResult.StudyFieldWithFacultyInfo);

            return NotFound(getStudyFieldsResult.Info);
        }
    }
}