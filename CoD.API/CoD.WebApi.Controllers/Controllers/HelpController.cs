﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Controllers.Models;
using CoD.WebApi.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HelpController : ControllerBase
    {
        public IHelpService HelpService { get; }


        public HelpController(IHelpService helpService)
        {
            HelpService = helpService;
        }

        [HttpGet("GetInformationForAdminPanel")]
        public async Task<IActionResult> GetInformationForAdminPanel()
        {
            var adminPanelInfoResult = await HelpService.GetInformationForAdminPanel();

            if (adminPanelInfoResult.IsSuccess)
                return Ok(adminPanelInfoResult);

            return NotFound(adminPanelInfoResult.Info);
        }

        [HttpGet("GetTickets")]
        public async Task<IActionResult> GetTickets()
        {
            var getTicketsResult = await HelpService.GetTickets();

            if (getTicketsResult.IsSuccess)
                return Ok(getTicketsResult);

            return NotFound(getTicketsResult.Info);
        }

        [HttpGet("GetTicket/{id}")]
        public async Task<IActionResult> GetTicket(int id)
        {
            var getTicketsResult = await HelpService.GetTicket(id);

            if (getTicketsResult.IsSuccess)
                return Ok(getTicketsResult);

            return NotFound(getTicketsResult.Info);
        }

        [HttpPost("SaveTicket")]
        public async Task<IActionResult> SaveTicket(TicketSaveModel ticketSaveModel)
        {
            var saveTicketResult = await HelpService.SaveTicket(new SaveTicketParameter(ticketSaveModel.PermissionInfos, ticketSaveModel.AccountId,
                ticketSaveModel.HelpMessage, ticketSaveModel.Title));

            if (saveTicketResult.IsSuccess)
                return Ok();

            return Conflict(saveTicketResult.Info);
        }

        [HttpPost("ChangeTicketStatus")]
        public async Task<IActionResult> ChangeTicketStatus(ChangeTicketStatusModel changeTicketStatusModel)
        {
            var saveTicketResult = await HelpService.ChangeTicketStatus(
                new ChangeTicketStatusParameter(changeTicketStatusModel.TicketId,
                    changeTicketStatusModel.TicketStatus));

            if (saveTicketResult.IsSuccess)
                return Ok();

            return Conflict(saveTicketResult.Info);
        }

    }
}