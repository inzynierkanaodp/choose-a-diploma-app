﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Controllers.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TopicOfWorkController : ControllerBase
    {
        public ITopicOfWorkService TopicOfWorkService { get; }

        public TopicOfWorkController(ITopicOfWorkService topicOfWorkService)
        {
            TopicOfWorkService = topicOfWorkService;
        }

        [HttpGet("GetTopicsOfWorks")]
        public async Task<IActionResult> GetTopicsOfWorks()
        {
            var getTopicsOfWorksResult = await TopicOfWorkService.GetTopicsOfWorks();

            if (getTopicsOfWorksResult.IsSuccess)
                return Ok(getTopicsOfWorksResult);

            return NotFound(getTopicsOfWorksResult.Info);
        }

        [HttpGet("GetTopicsOfWorks/{accountId}")]
        public async Task<IActionResult> GetTopicsOfWorks(int accountId)
        {
            var getTopicsOfWorksResult = await TopicOfWorkService.GetTopicsOfWorks(accountId);

            if (getTopicsOfWorksResult.IsSuccess)
                return Ok(getTopicsOfWorksResult.TopicOfWorkTables);

            return NotFound(getTopicsOfWorksResult.Info);
        }

        [HttpGet("GetTopicOfWork/{id}")]
        public async Task<IActionResult> GetTopicOfWork(int id)
        {
            var getTopicsOfWorksResult = await TopicOfWorkService.GetTopicOfWork(id);

            if (getTopicsOfWorksResult.IsSuccess)
                return Ok(getTopicsOfWorksResult.TopicOfWork);

            return NotFound(getTopicsOfWorksResult.Info);
        }

        [HttpPost("SaveTopicOfWork")]
        public async Task<IActionResult> SaveTopicOfWork(TopicOfWorkSaveModel saveTopicOfWorkModel)
        {
            var saveTopicOfWorkResult = await TopicOfWorkService.SaveTopicOfWork(
                new SaveTopicOfWorkParameter(
                saveTopicOfWorkModel.ThesisForAdd, 
                saveTopicOfWorkModel.AccountId, 
                saveTopicOfWorkModel.IssueSemesterInfos, 
                saveTopicOfWorkModel.StudyFieldWithFacultyInfos));

            if (saveTopicOfWorkResult.IsSuccess)
                return Ok();

            return Conflict(saveTopicOfWorkResult.Info);
        }

        [HttpPost("ReserveTopicOfWork")]
        public async Task<IActionResult> ReserveTopicOfWork(TopicOfWorkReserveModel topicOfWorkReserveModel)
        {
            var saveTopicOfWorkResult = await TopicOfWorkService.ReserveTopicOfWork(
                new ReserveTopicOfWorkParameter(
                    topicOfWorkReserveModel.TopicId, 
                    topicOfWorkReserveModel.AccountId));

            if (saveTopicOfWorkResult.IsSuccess)
                return Ok();

            return Conflict(saveTopicOfWorkResult.Info);
        }

        [HttpPost("ResignTopicOfWork")]
        public async Task<IActionResult> ResignTopicOfWork(TopicOfWorkResignModel topicOfWorkResignModel)
        {
            var saveTopicOfWorkResult = await TopicOfWorkService.ResignTopicOfWork(new ReserveTopicOfWorkParameter(topicOfWorkResignModel.TopicId, topicOfWorkResignModel.AccountId));

            if (saveTopicOfWorkResult.IsSuccess)
                return Ok();

            return Conflict(saveTopicOfWorkResult.Info);
            
        }

        [HttpPost("IsOwnerThisTopic")]
        public async Task<IActionResult> IsOwnerThisTopic(TopicOfWorkResignModel topicOfWorkResignModel)
        {
            var isActiveUserResult = await TopicOfWorkService.IsOwnerThisTopic(new ReserveTopicOfWorkParameter(topicOfWorkResignModel.TopicId, topicOfWorkResignModel.AccountId));

            if (isActiveUserResult.IsSuccess)
                return Ok(isActiveUserResult.IsActiveUser);

            return Conflict(isActiveUserResult.IsActiveUser);
        }
    }
}
