﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class IssueSemesterController : ControllerBase
    {
        public IIssueSemesterService IssueSemesterService { get; }

        public IssueSemesterController(IIssueSemesterService issueSemesterService)
        {
            IssueSemesterService = issueSemesterService;
        }

        [HttpGet("GetIssueSemesters")]
        public async Task<IActionResult> GetIssueSemesters()
        {
            var getIssueSemestersResult = await IssueSemesterService.GetIssueSemesters();

            if (getIssueSemestersResult.IsSuccess)
                return Ok(getIssueSemestersResult.IssueSemesters);

            return NotFound(getIssueSemestersResult.Info);
        }
    }
}