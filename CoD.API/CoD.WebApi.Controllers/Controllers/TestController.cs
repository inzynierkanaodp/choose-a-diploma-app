﻿using System;
using System.Collections.Generic;
using System.Text;
using CoD.Business.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/{controller}")]
    [ApiController]
    public class TestController : ControllerBase

    {
        public ITestService TestService { get; }

        public TestController(ITestService testService)
        {
            TestService = testService;
        }

    }
}
