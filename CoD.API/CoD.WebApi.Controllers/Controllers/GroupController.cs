﻿using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Controllers.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoD.WebApi.Controllers.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        public IGroupService GroupService { get; }

        public GroupController(IGroupService groupService)
        {
            GroupService = groupService;
        }

        [HttpGet("GetGroups")]
        public async Task<IActionResult> GetGroups()
        {
            var getGroupsResult = await GroupService.GetGroups();

            return Ok(getGroupsResult);
        }

        [HttpGet("GetGroup/{id}")]
        public async Task<IActionResult> GetGroup(int id)
        {
            var getGroupResult = await GroupService.GetGroup(id);

            return Ok(getGroupResult);
        }

        [HttpGet("GetGroupMembers/{id}")]
        public async Task<IActionResult> GetGroupMembers(int id)
        {
            var getGroupResult = await GroupService.GetGroupMembers(id);

            return Ok(getGroupResult);
        }

        [HttpGet("GetAccountGroup/{accountId}")]
        public async Task<IActionResult> GetAccountGroupId(int accountId)
        {
            var accountGroupIdResult = await GroupService.GetAccountGroupId(accountId);

            return Ok(accountGroupIdResult);
        }

        [HttpPost("CreateGroup")]
        public async Task<IActionResult> CreateGroup(CreateGroupModel createGroupModel)
        {
            var createGroupResult = await GroupService.CreateGroup(
                new CreateGroupParameter(createGroupModel.AccountId, 
                    createGroupModel.Password, 
                    createGroupModel.ConfirmPassword));

            if (createGroupResult.IsSuccess)
                return Ok();

            return Conflict(createGroupResult.Info);
        }

        [HttpPost("JoinGroup")]
        public async Task<IActionResult> JoinGroup(JoinGroupModel joinGroupModel)
        {
            var joinGroupResult = await GroupService.JoinGroup(new JoinGroupParameter(joinGroupModel.AccountId, joinGroupModel.GroupId, 
                joinGroupModel.Password, joinGroupModel.ConfirmPassword));

            if (joinGroupResult.IsSuccess)
                return Ok();

            return Conflict(joinGroupResult.Info);
        }

        [HttpGet("CanJoinGroup/{accountId}")]
        public async Task<IActionResult> CanJoinGroup(int accountId)
        {
            var canJoinGroupResult = await GroupService.CanJoinGroup(new CanJoinGroupParameter(accountId));

            if (canJoinGroupResult.IsSuccess)
                return Ok(canJoinGroupResult);

            return Conflict(canJoinGroupResult.Info);
        }

        [HttpGet("ArchiveGroup/{id}")]
        public async Task<IActionResult> ArchiveGroup(int id)
        {
            await GroupService.ArchiveGroup(id);

            return Ok();
        }

        [HttpGet("RemoveGroupMember/{accountId}")]
        public async Task<IActionResult> RemoveGroupMember(int accountId)
        {
            await GroupService.RemoveGroupMember(accountId);
            return Ok();
        }

        [HttpGet("IsGroupLeader/{accountId}/{groupId}")]
        public async Task<IActionResult> IsGroupLeader(int accountId, int groupId)
        {
            var isGroupLeader = await GroupService.IsGroupLeader(accountId, groupId);

            return Ok(isGroupLeader);
        }
    }
}