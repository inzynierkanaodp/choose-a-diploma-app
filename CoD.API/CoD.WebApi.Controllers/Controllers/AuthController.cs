﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CoD.Business.Interfaces.Classes;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Controllers.Models;
using CoD.WebApi.Controllers.Models.Auth;
using CoD.WebApi.Data;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Helpers;
using CoD.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CoD.WebApi.Controllers.Controllers
{

    [Route("api/{controller}")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public IAuthService AuthService { get; }

        public AuthController(IAuthService authService)
        {
            AuthService = authService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]AccountRegisterModel accountRegisterModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(Extensions.GetErrorMessages(ModelState));

            var registerResult = await AuthService.RegisterAccount(new RegisterParameter(accountRegisterModel.Login, accountRegisterModel.Password,
                accountRegisterModel.ConfirmPassword, accountRegisterModel.Email, accountRegisterModel.IsStudent));

            if (registerResult.IsSuccess)
                return StatusCode(201);

            return Conflict(registerResult.Info);
        }

        [HttpPut("reset")]
        public async Task<IActionResult> Reset(AccountResetModel accountResetModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(Extensions.GetErrorMessages(ModelState));

            var registerResult = await AuthService.ResetAccountPassword(
                new ResetAccountPasswordParameter(
                    new AccountForResetPassword(accountResetModel.Login, accountResetModel.NewPassword,
                        accountResetModel.ConfirmNewPassword, accountResetModel.Email)));

            if (registerResult.IsSuccess)
                return StatusCode(201);

            return Conflict(registerResult.Info);
        }

        [HttpPut("ChangeAccountEmail")]
        public async Task<IActionResult> ChangeAccountEmail(ChangeAccountEmailModel changeAccountEmailModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(Extensions.GetErrorMessages(ModelState));

            var accountResetEmailResult = await AuthService.ResetAccountEmail(new ResetAccountEmailParameter(
                changeAccountEmailModel.OldEmail, changeAccountEmailModel.NewEmail,
                changeAccountEmailModel.ConfirmNewEmail, changeAccountEmailModel.Password,
                changeAccountEmailModel.ConfirmPassword));

            if (accountResetEmailResult.IsSuccess)
                return StatusCode(201);

            return Conflict(accountResetEmailResult.Info);
        }

        [HttpPut("ChangeAccountLogin")]
        public async Task<IActionResult> ChangeAccountLogin(ChangeAccountLoginModel changeAccountLoginModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(Extensions.GetErrorMessages(ModelState));

            var accountResetLoginResult = await AuthService.ResetAccountLogin(new ResetAccountLoginParameter(
                changeAccountLoginModel.OldLogin, changeAccountLoginModel.NewLogin,
                changeAccountLoginModel.ConfirmNewLogin, changeAccountLoginModel.Password,
                changeAccountLoginModel.ConfirmPassword));

            if (accountResetLoginResult.IsSuccess)
                return StatusCode(201);

            return Conflict(accountResetLoginResult.Info);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(AccountLoginModel accountLoginModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(Extensions.GetErrorMessages(ModelState));

            var loginResult = await AuthService.LoginAccount(new LoginParameter(accountLoginModel.Login, accountLoginModel.Password));
            var tokenHandler = new JwtSecurityTokenHandler();

            if (loginResult.IsSuccess)
                return Ok(new
                {
                    token = tokenHandler.WriteToken(loginResult.SecurityToken)
                });

            return Unauthorized("Wystąpił błąd logowania.");
        }
    }
}