﻿using System;
using System.Collections.Generic;
using CoD.WebApi.Interfaces;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;

namespace CoD.WebApi.Controllers
{
    public class ControllersModule : WebApiModule
    {
        public override IEnumerable<IApplicationFeatureProvider> ApplicationFeatureProviders { get; }

        public ControllersModule(IConfigurationSection configuration) : base(configuration)
        {
            ApplicationFeatureProviders = new[]
            {
                new AssemblyControllersFeatureProvider(typeof(ControllersModule).Assembly)
            };
        }
    }
}
