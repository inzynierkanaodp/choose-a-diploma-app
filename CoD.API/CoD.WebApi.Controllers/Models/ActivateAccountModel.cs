﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models
{
    public class ActivateAccountModel
    {
        [Required]
        public int TicketId { get; set; }
    }
}