﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models
{
    public class CreateGroupModel
    {
        [Required(ErrorMessage = "TopicOfWorkId jest wymagany")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Podanie hasła jest obowiązkowe.")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Podanie powtórzenia hasła jest obowiązkowe.")]
        [MinLength(6, ErrorMessage = "Potwórzenie hasła musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}