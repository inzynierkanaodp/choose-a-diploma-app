﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoD.WebApi.Models;

namespace CoD.WebApi.Controllers.Models
{
    public class TicketSaveModel
    {
        [Required]
        public List<PermissionInfo> PermissionInfos { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string HelpMessage { get; set; }

        [Required]
        public int AccountId { get; set; }
    }
}