﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models
{
    public class JoinGroupModel
    {
        [Required(ErrorMessage = "AccountId is required")]
        public int GroupId { get; set; }

        [Required(ErrorMessage = "TopicOfWorkId is required")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "The password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}