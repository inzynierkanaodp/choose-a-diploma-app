﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models.User
{
    public class AccountForPermissionModel
    {
        [Required]
        public int Id { get; set; }
    }
}