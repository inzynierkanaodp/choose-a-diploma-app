﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoD.WebApi.Models;

namespace CoD.WebApi.Controllers.Models.User
{
    public class AccountForUpdateModel
    {
        [Required]
        public AccountForUpdate AccountForUpdate { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        public List<FacultyInfo> FacultyInfos { get; set; }

        [Required]
        public List<IssueSemesterInfo> IssueSemesterInfos { get; set; }

        [Required]
        public List<StudyFieldWithFacultyInfo> StudyFieldWithFacultyInfos { get; set; }
    }
}