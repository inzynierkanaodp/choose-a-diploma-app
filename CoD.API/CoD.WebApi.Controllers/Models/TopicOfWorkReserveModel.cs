﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models
{
    public class TopicOfWorkReserveModel
    {
        [Required]
        public int AccountId { get; set; }

        [Required]
        public int TopicId { get; set; }
    }
}