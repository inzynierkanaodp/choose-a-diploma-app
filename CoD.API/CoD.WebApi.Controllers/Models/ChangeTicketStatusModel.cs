﻿using System.ComponentModel.DataAnnotations;
using CoD.WebApi.Enums;

namespace CoD.WebApi.Controllers.Models
{
    public class ChangeTicketStatusModel
    {
        [Required]
        public int TicketId { get; set; }

        [Required]
        public TicketStatus TicketStatus { get; set; }
    }
}