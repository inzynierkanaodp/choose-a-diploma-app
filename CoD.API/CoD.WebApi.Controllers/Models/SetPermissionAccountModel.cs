﻿using System.ComponentModel.DataAnnotations;
using CoD.WebApi.Enums;

namespace CoD.WebApi.Controllers.Models
{
    public class SetPermissionAccountModel
    {
        [Required]
        public int TicketId { get; set; }

        [Required]
        public PermissionName PermissionName { get; set; }
    }
}