﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoD.WebApi.Models;

namespace CoD.WebApi.Controllers.Models
{
    public class TopicOfWorkSaveModel
    {
        [Required]
        public ThesisForAdd ThesisForAdd { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        public List<IssueSemesterInfo> IssueSemesterInfos { get; set; }

        [Required]
        public List<StudyFieldInfo> StudyFieldWithFacultyInfos { get; set; }
    }
}