﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models
{
    public class CanJoinGroupModel
    {
        [Required(ErrorMessage = "TopicOfWorkId is required")]
        public int AccountId { get; set; }
    }
}