﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models.Auth
{
    public class AccountLoginModel
    {
        [Required(ErrorMessage = "The login is required")]
        public string Login { get; set; }

        [Required(ErrorMessage = "The password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}