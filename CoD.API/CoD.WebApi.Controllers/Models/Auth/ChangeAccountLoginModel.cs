﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models.Auth
{
    public class ChangeAccountLoginModel
    {
        [Required(ErrorMessage = "The old login address is required")]
        public string OldLogin { get; set; }

        [Required(ErrorMessage = "The new login address is required")]
        public string NewLogin { get; set; }

        [Required(ErrorMessage = "The old login address is required")]
        [Compare("NewLogin")]
        public string ConfirmNewLogin { get; set; }

        [Required(ErrorMessage = "The password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}