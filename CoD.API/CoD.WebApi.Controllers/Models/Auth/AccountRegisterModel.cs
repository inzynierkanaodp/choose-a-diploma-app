﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models.Auth
{
    public class AccountRegisterModel
    {
        [Required(ErrorMessage = "The login is required")]
        public string Login { get; set; }

        [Required(ErrorMessage = "The password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        public bool IsStudent { get; set; }
    }
}