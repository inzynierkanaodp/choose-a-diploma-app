﻿using System.ComponentModel.DataAnnotations;

namespace CoD.WebApi.Controllers.Models.Auth
{
    public class ChangeAccountEmailModel
    {
        [Required(ErrorMessage = "The old email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string OldEmail { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Compare("NewPassword")]
        public string NewEmail { get; set; }

        [Required(ErrorMessage = "The confirm new email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Compare("NewEmail")]
        public string ConfirmNewEmail { get; set; }

        [Required(ErrorMessage = "The password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [MinLength(6, ErrorMessage = "Hasło musi być dłuższe niż 6 znaków")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}