﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoD.Tests.Helpers;
using CoD.WebApi.Data;
using CoD.WebApi.Data.Classes;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using Microsoft.EntityFrameworkCore;
using Moq;
using NSubstitute;
using NUnit.Framework;

namespace CoD.Tests.Tests
{
    [TestFixture]
    public class AccountRepositoryTests
    {
        [Test]
        public void GetIsAccountActive()
        {
            IQueryable<AccountDb> accountDbs = new List<AccountDb>
            {
                new AccountDb {Id = 1, IsActive = true},
                new AccountDb {Id = 2, IsActive = false},
                new AccountDb {Id = 3, IsActive = true},
                new AccountDb {Id = 4, IsActive = false}
            }.AsQueryable();

            var mockSet = new Mock<Microsoft.EntityFrameworkCore.DbSet<AccountDb>>();

            mockSet.As<IDbAsyncEnumerable<AccountDb>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<AccountDb>(accountDbs.GetEnumerator()));

            mockSet.As<IQueryable<AccountDb>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<AccountDb>(accountDbs.Provider));

            mockSet.As<IQueryable<AccountDb>>().Setup(m => m.Expression).Returns(accountDbs.Expression);
            mockSet.As<IQueryable<AccountDb>>().Setup(m => m.ElementType).Returns(accountDbs.ElementType);
            mockSet.As<IQueryable<AccountDb>>().Setup(m => m.GetEnumerator()).Returns(accountDbs.GetEnumerator);

            var mockContext = new Mock<DataContext>();
            mockContext.Setup(m => m.Accounts).Returns(mockSet.Object);

            var service = new AccountRepository(mockContext.Object, null);
            var accountsN = service.GetAccountsN(); //Problem z asyncami

            //var mock = new Mock<System.Data.Entity.DbSet<AccountDb>>();

            //mock.Setup()
        }
    }
}