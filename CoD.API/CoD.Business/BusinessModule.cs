﻿using System;
using System.Reflection.Emit;
using Autofac;
using CoD.Business.Services;
using CoD.Common.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CoD.Business
{
    public class BusinessModule : ModuleBase
    {
        public BusinessModule(IConfigurationSection configuration) : base(configuration)
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AccountService>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<IssueSemesterService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<FacultyService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<StudyFieldService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TopicOfWorkService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<PermissionService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<HelpService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<GroupService>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
