﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Enums;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Services
{
    public class HelpService : IHelpService
    {
        private readonly IHelpRepository _helpRepository;
        private readonly IPermissionTicketRepository _permissionTicketRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public HelpService(ILogger logger, IHelpRepository helpRepository, IPermissionTicketRepository permissionTicketRepository, IAccountRepository accountRepository, IMapper mapper)
        {
            _helpRepository = helpRepository;
            _permissionTicketRepository = permissionTicketRepository;
            _accountRepository = accountRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<GetAdminPanelInfoResult> GetInformationForAdminPanel()
        {
            AdminPanelInfo adminPanelInfo = new AdminPanelInfo();
            
            try
            {
                var tickets = await _helpRepository.GetTickets();
                var accounts = await _accountRepository.GetAccounts();

                adminPanelInfo.ClosedTicketsQuantity = tickets.Count(t => t.Status == TicketStatus.Closed.ToString());
                adminPanelInfo.OpenTicketsQuantity = tickets.Count(t => t.Status == TicketStatus.Open.ToString());

                adminPanelInfo.UsersWithRolesQuantity= accounts.Count(a => a.PermissionsAccounts != null);
                adminPanelInfo.UsersWithoutRolesQuantity = accounts.Count(a => a.PermissionsAccounts == null);

                adminPanelInfo.ActiveAccountsQuantity = accounts.Count(a => a.IsActive);
                adminPanelInfo.NonActiveAccountsQuantity = accounts.Count(a => a.IsActive == false);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetAdminPanelInfoResult(adminPanelInfo,  true, String.Empty);
        }

        public async Task<GetTicketsResult> GetTickets()
        {
            List<TicketTable> ticketTables;

            try
            {
                var tickets = await _helpRepository.GetTickets();
                ticketTables = _mapper.Map<List<TicketTable>>(tickets);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetTicketsResult(ticketTables, true, String.Empty);
        }

        public async Task<GetTicketsResult> GetTicket(int id)
        {
            List<TicketTable> ticketTables = new List<TicketTable>();

            try
            {
                var ticket = await _helpRepository.GetTicket(id);
                ticketTables.Add(_mapper.Map<TicketTable>(ticket));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetTicketsResult(ticketTables, true, String.Empty);
        }

        public async Task<SaveTicketResult> SaveTicket(SaveTicketParameter saveTicketParameter)
        {
            var ticket = new TicketDb
            {
                Title = saveTicketParameter.Title,
                Content = saveTicketParameter.HelpMessage,
                IdAccount = saveTicketParameter.AccountId,
                TicketDateTime = DateTime.Now,
                Status = ((int)TicketStatus.New).ToString()
            };

            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var ticketId = await _helpRepository.SaveTicket(ticket);

                    if (saveTicketParameter.PermissionInfos != null)
                    {
                        foreach (var permissionInfo in saveTicketParameter.PermissionInfos)
                        {
                            await _permissionTicketRepository.SavePermissionTicket(new PermissionTicketDb
                            {
                                IdPermission = permissionInfo.Id,
                                IdTicket = ticketId
                            });
                        }
                    }

                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new SaveTicketResult(false, "Problem with saving ticket in database.");
            }

            return new SaveTicketResult(true, String.Empty);
        }

        public async Task<SaveTicketResult> ChangeTicketStatus(ChangeTicketStatusParameter changeTicketStatusParameter)
        {
            try
            {
                var ticket = await _helpRepository.GetTicket(changeTicketStatusParameter.TicketId);

                ticket.Status = ((int)changeTicketStatusParameter.TicketStatus).ToString();
                await _accountRepository.SaveAll();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;

            }

            return new SaveTicketResult(true, String.Empty);
        }
    }
}