﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Services
{
    public class StudyFieldService : IStudyFieldService
    {
        private readonly IStudyFieldRepository _repository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public StudyFieldService(ILogger logger, IStudyFieldRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<GetStudyFieldsResult> GetStudyFields()
        {
            List<StudyFieldInfo> studyFields;
            try
            {
                var studyFieldFromRepo = await _repository.GetStudyFields();
                studyFields = _mapper.Map<List<StudyFieldInfo>>(studyFieldFromRepo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetStudyFieldsResult(studyFields, true, String.Empty);
        }

        public async Task<GetStudyFieldsWithFacultiesResult> GetStudyFieldsWithFaculties()
        {
            List<StudyFieldWithFacultyInfo> studyFieldWithFacultyInfos;
            try
            {
                var studyFieldFromRepo = await _repository.GetStudyFields();
                studyFieldWithFacultyInfos = _mapper.Map<List<StudyFieldWithFacultyInfo>>(studyFieldFromRepo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetStudyFieldsWithFacultiesResult(studyFieldWithFacultyInfos, true, String.Empty);
        }
    }
}