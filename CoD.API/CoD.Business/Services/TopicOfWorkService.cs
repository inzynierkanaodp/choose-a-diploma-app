﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;
using NLog.Web.LayoutRenderers;

namespace CoD.Business.Services
{

    public class TopicOfWorkService : ITopicOfWorkService
    {
        private readonly ITopicOfWorkRepository _topicOfWorkRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IGroupsTopicsRepository _groupsTopicsRepository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public TopicOfWorkService(ILogger logger, ITopicOfWorkRepository topicOfWorkRepository, IMapper mapper, IGroupsTopicsRepository groupsTopicsRepository, IGroupRepository groupRepository)
        {
            _topicOfWorkRepository = topicOfWorkRepository;
            _mapper = mapper;
            _groupsTopicsRepository = groupsTopicsRepository;
            _groupRepository = groupRepository;
            _logger = logger;
        }

        public async Task<GetTopicsOfWorksResult> GetTopicsOfWorks()
        {
            List<TopicOfWorkTable> topicOfWorkTables;
            int quantityOfUnhiddenTopics;
            int quantityUniqueOfPromoters;

            try
            {
                var topicsOfWorksFromRepo = await _topicOfWorkRepository.GetTopicsOfWorks();
                topicOfWorkTables = _mapper.Map<List<TopicOfWorkTable>>(topicsOfWorksFromRepo);

                quantityOfUnhiddenTopics = topicOfWorkTables.Count(t => t.IsHidden != true);
                quantityUniqueOfPromoters = topicsOfWorksFromRepo.Select(x => x.IdAccount).Distinct().Count();

                topicOfWorkTables.RemoveAll(t => t.IsHidden);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetTopicsOfWorksResult(topicOfWorkTables, quantityOfUnhiddenTopics, quantityUniqueOfPromoters, true, String.Empty);
        }

        public async Task<GetTopicsOfWorksResult> GetTopicsOfWorks(int accountId)
        {
            List<TopicOfWorkTable> topicOfWorkTables;
            int quantityOfUnhiddenTopics;
            int quantityUniqueOfPromoters;

            try
            {
                var topicsOfWorksFromRepo = await _topicOfWorkRepository.GetTopicsOfWorks(accountId);
                topicOfWorkTables = _mapper.Map<List<TopicOfWorkTable>>(topicsOfWorksFromRepo);

                quantityOfUnhiddenTopics = topicOfWorkTables.Select(t => t.IsHidden != true).Count();
                quantityUniqueOfPromoters = topicsOfWorksFromRepo.Select(x => x.IdAccount).Distinct().Count();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetTopicsOfWorksResult(topicOfWorkTables, quantityOfUnhiddenTopics, quantityUniqueOfPromoters, true, String.Empty);
        }

        public async Task<GetTopicOfWorkResult> GetTopicOfWork(int id)
        {
            TopicOfWorkTable topicOfWork;
            try
            {
                var topicsOfWorkFromRepo = await _topicOfWorkRepository.GetTopicOfWork(id);
                topicOfWork = _mapper.Map<TopicOfWorkTable>(topicsOfWorkFromRepo);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetTopicOfWorkResult(topicOfWork, true, String.Empty);
        }

        public async Task<SaveTopicOfWorkResult> SaveTopicOfWork(SaveTopicOfWorkParameter saveTopicOfWorkParameter)
        {
            var topicOfWorkDb = new TopicOfWorkDb
            {
                Name = saveTopicOfWorkParameter.ThesisForAdd.TopicName,
                Description = saveTopicOfWorkParameter.ThesisForAdd.Description,
                IdAccount = saveTopicOfWorkParameter.AccountId,
                MaxSize = saveTopicOfWorkParameter.ThesisForAdd.MaxSize,
                IsReserved = saveTopicOfWorkParameter.ThesisForAdd.IsReserved,
                IdSemester = saveTopicOfWorkParameter.IssueSemesterInfos.FirstOrDefault().Id,
                IdStudyField = saveTopicOfWorkParameter.StudyFieldWithFacultyInfos.FirstOrDefault().Id
            };

            try
            {
                await _topicOfWorkRepository.SaveTopicOfWork(topicOfWorkDb);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
                return new SaveTopicOfWorkResult(false, "Pojawił się błąd przy zapisie pracy.");
            }

            return new SaveTopicOfWorkResult(true, String.Empty);
        }

        public async Task<ReserveTopicOfWorkResult> ReserveTopicOfWork(ReserveTopicOfWorkParameter reserveTopicOfWorkParameter)
        {
            var groupDb = await _groupRepository.GetGroupByAccountId(reserveTopicOfWorkParameter.AccountId);
            var topicOfWorkDb = await _topicOfWorkRepository.GetTopicOfWork(reserveTopicOfWorkParameter.TopicOfWorkId);
            topicOfWorkDb.IsReserved = true;

            var groupTopicDb = new GroupTopicDb
            {
                IdGroup = groupDb.Id,
                IdTopicOfWork = reserveTopicOfWorkParameter.TopicOfWorkId,
                ReservationTime = DateTime.Now,
                IsConfirmed = true
            };

            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    await _groupsTopicsRepository.ReserveTopicOfWork(groupTopicDb);
                    await _topicOfWorkRepository.UpdateTopicOfWork(topicOfWorkDb);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return new ReserveTopicOfWorkResult(false, "Pojawił się błąd przy rezerwacji pracy.");
            }
            return new ReserveTopicOfWorkResult(true, String.Empty);
        }

        public async Task<ReserveTopicOfWorkResult> ResignTopicOfWork(ReserveTopicOfWorkParameter reserveTopicOfWorkParameter)
        {
            var groupDb = await _groupRepository.GetGroupByAccountId(reserveTopicOfWorkParameter.AccountId);

            try
            {
                var groupTopicDb = await _groupsTopicsRepository.GetGroupTopic(reserveTopicOfWorkParameter.TopicOfWorkId, groupDb.Id);
                await _groupsTopicsRepository.ResignGroupTopic(groupTopicDb);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return new ReserveTopicOfWorkResult(false, "Pojawił się błąd przy rezygnacji z pracy.");
            }
            return new ReserveTopicOfWorkResult(true, String.Empty);
        }

        public async Task<IsActiveAccountResult> IsOwnerThisTopic(ReserveTopicOfWorkParameter reserveTopicOfWorkParameter)
        {
            var groupDb = await _groupRepository.GetGroupByAccountId(reserveTopicOfWorkParameter.AccountId);

            if(groupDb == null)
                return new IsActiveAccountResult(false,false, "Pojawił się błąd przy rezygnacji z pracy.");
            try
            {
                var groupTopicDb = await _groupsTopicsRepository.GetGroupTopic(reserveTopicOfWorkParameter.TopicOfWorkId, groupDb.Id);
                if(groupTopicDb == null)
                    return new IsActiveAccountResult(false, false, "Pojawił się błąd przy rezygnacji z pracy.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return new IsActiveAccountResult(false, false, "Pojawił się błąd przy rezygnacji z pracy.");
            }
            return new IsActiveAccountResult(true, true, String.Empty);

        }
    }
}