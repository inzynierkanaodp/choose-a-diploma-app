﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Services
{
    public class IssueSemesterService : IIssueSemesterService
    {
        private readonly IIssueSemesterRepository _repository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public IssueSemesterService(ILogger logger, IIssueSemesterRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<GetIssueSemestersResult> GetIssueSemesters()
        {
            List<IssueSemesterInfo> issueSemesters;
            try
            {
                var issueSemesterFromRepo = await _repository.GetIssueSemesters();
                issueSemesters = _mapper.Map<List<IssueSemesterInfo>>(issueSemesterFromRepo);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetIssueSemestersResult(issueSemesters, true, String.Empty);
        }
    }
}