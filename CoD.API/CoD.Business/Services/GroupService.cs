﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Helpers;
using CoD.WebApi.Migrations;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Services
{
    public class GroupService : IGroupService
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IAccountsGroupsRepository _accountsGroupsRepository;
        private readonly IGroupsTopicsRepository _groupsTopicsRepository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public GroupService(ILogger logger, IGroupRepository groupRepository, IAccountsGroupsRepository accountsGroupsRepository,
            IGroupsTopicsRepository groupsTopicsRepository, IMapper mapper)
        {
            _groupRepository = groupRepository;
            _accountsGroupsRepository = accountsGroupsRepository;
            _groupsTopicsRepository = groupsTopicsRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<GetGroupsResult> GetGroups()
        {
            List<GroupTable> groupTables;

            int quantityOfGroups = 0;
            int groupsWithTopicQuantity = 0;
            int groupsWithoutTopicQuantity = 0;

            try
            {
                var groupDbs = await _groupRepository.GetGroups();
                groupTables = _mapper.Map<List<GroupTable>>(groupDbs);
                quantityOfGroups = groupTables.Count;

                var groupsTopics = await _groupsTopicsRepository.GetGroupsTopics();
                groupsWithTopicQuantity = groupsTopics.Count();
                groupsWithoutTopicQuantity = quantityOfGroups - groupsWithTopicQuantity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetGroupsResult(groupTables, quantityOfGroups, groupsWithTopicQuantity, groupsWithoutTopicQuantity, true, String.Empty);
        }

        public async Task<GetGroupResult> GetGroup(int id)
        {
            GroupTable groupTable;
            try
            {
                var groupDb = await _groupRepository.GetGroup(id);
                groupTable = _mapper.Map<GroupTable>(groupDb);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetGroupResult(groupTable, true, String.Empty);
        }

        public async Task<GetAccountsResult> GetGroupMembers(int id)
        {
            List<AccountForUpdate> accountsTables;
            try
            {
                var groupDb = await _groupRepository.GetGroup(id);

                var accounts = groupDb.AccountsGroups.Select(x => x.Account).ToList();
                accountsTables = _mapper.Map<List<AccountForUpdate>>(accounts);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetAccountsResult(accountsTables, true, String.Empty);
        }

        public async Task<GetAccountGroupIdResult> GetAccountGroupId(int accountId)
        {
            int? idGroup = null;
            try
            {
                var accountGroupDb = await _accountsGroupsRepository.GetAccountGroupByAccountId(accountId);

                if (accountGroupDb != null)
                    idGroup = accountGroupDb.IdGroup;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetAccountGroupIdResult(idGroup, true, String.Empty);
        }

        public async Task<CreateGroupResult> CreateGroup(CreateGroupParameter createGroupParameter)
        {
            var groupDb = new GroupDb
            {
                IdAccount = createGroupParameter.AccountId,
                IsArchived = false,
                IsDeleted = false
            };

            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var createdGroup = await _groupRepository.CreateGroup(groupDb, createGroupParameter.Password);

                    var accountGroupDb = new AccountGroupDb
                    {
                        IdAccount = createGroupParameter.AccountId,
                        IdGroup = createdGroup.Id
                    };

                    await _accountsGroupsRepository.JoinGroup(accountGroupDb);

                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new CreateGroupResult(false, "Problem podczas zakładania grupy.");
            }

            return new CreateGroupResult(true, String.Empty);
        }

        public async Task<JoinGroupResult> JoinGroup(JoinGroupParameter joinGroupParameter)
        {
            try
            {
                var accountGroupDb = new AccountGroupDb
                {
                    IdAccount = joinGroupParameter.AccountId,
                    IdGroup = joinGroupParameter.GroupId
                };

                var groupDb = await _groupRepository.GetGroup(joinGroupParameter.GroupId);

                if (groupDb != null)
                    await _accountsGroupsRepository.JoinGroup(accountGroupDb, groupDb, joinGroupParameter.Password);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new JoinGroupResult(true, String.Empty);
        }

        public async Task<CanJoinGroupResult> CanJoinGroup(CanJoinGroupParameter canJoinGroupParameter)
        {
            bool canJoinGroup = false;

            try
            {
                canJoinGroup = await _accountsGroupsRepository.CanJoinGroup(canJoinGroupParameter.AccountId);
            }
            catch (Exception)
            {
                _logger.Debug("[ERROR]: Problem while getting info about account permission.");

                return new CanJoinGroupResult(canJoinGroup, false, "Problem while getting info.");
            }

            return new CanJoinGroupResult(canJoinGroup, true, String.Empty);
        }

        public async Task<IsActiveAccountResult> CanAccountReserveTopic(IsActiveParameter isActiveParameter)
        {
            bool canAccountReserveTopic = false;

            try
            {
                var groupDb = await _groupRepository.GetGroupByAccountId(isActiveParameter.Id);

                if (groupDb != null)
                    canAccountReserveTopic = true;
            }
            catch (Exception)
            {
                _logger.Debug("[ERROR]: Problem while getting info about account permission.");

                return new IsActiveAccountResult(canAccountReserveTopic, false, "Problem while getting info about account permission.");
            }

            return new IsActiveAccountResult(canAccountReserveTopic, true, "Problem while getting info about account permission.");
        }

        public async Task RemoveGroupMember(int accountId)
        {
            try
            {
                var accountGroup = await _accountsGroupsRepository.GetAccountGroupByAccountId(accountId);
                await _accountsGroupsRepository.RemoveMember(accountGroup);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task ArchiveGroup(int id)
        { 
            try
            {
                var group = await _groupRepository.GetGroup(id);
                group.IsArchived = true;

                await _groupRepository.SaveAll();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public async Task<bool> IsGroupLeader(int accountId, int groupId)
        {
            try
            {
                var group = await _groupRepository.GetGroup(groupId);

                if (group.IdAccount == accountId)
                    return true;

                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}