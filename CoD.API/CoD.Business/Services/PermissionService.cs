﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Enums;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IHelpRepository _helpRepository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public PermissionService(ILogger logger, IPermissionRepository permissionRepository, IAccountRepository accountRepository, IMapper mapper, IHelpRepository helpRepository)
        {
            _permissionRepository = permissionRepository;
            _accountRepository = accountRepository;
            _mapper = mapper;
            _helpRepository = helpRepository;
            _logger = logger;
        }

        public async Task<GetPermissionsResult> GetPermissions()
        {
            List<PermissionInfo> permissionInfos;
            try
            {
                var permissionsFromRepo = await _permissionRepository.GetPermissions();
                permissionInfos = _mapper.Map<List<PermissionInfo>>(permissionsFromRepo);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetPermissionsResult(permissionInfos, true, String.Empty);
        }

        public async Task<AccountForResetPasswordResult> SetPermissionAccount(SetPermissionAccountParameter setPermissionAccountParameter)
        {
            try
            {
                var ticket = await _helpRepository.GetTicket(setPermissionAccountParameter.TicketId);
                var account = await _accountRepository.GetAccount(ticket.IdAccount);
                var permission = await _permissionRepository.GetPermission(setPermissionAccountParameter.PermissionName);

                PermissionAccountDb permissionAccount = new PermissionAccountDb
                {
                    IdAccount = account.Id,
                    IdPermission = permission.Id
                };

                await _permissionRepository.SavePermissionAccount(permissionAccount);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new AccountForResetPasswordResult(true, String.Empty);
        }
    }
}
