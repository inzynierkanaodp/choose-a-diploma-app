﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CoD.Business.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository _authRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public AuthService(ILogger logger, IAuthRepository authRepository, IConfiguration configuration, IAccountRepository accountRepository, IMapper mapper)
        {
            _authRepository = authRepository;
            _configuration = configuration;
            _accountRepository = accountRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<RegisterResult> RegisterAccount(RegisterParameter registerParameter)
        {
            AccountDb createdAccount = null;
            registerParameter.Login = registerParameter.Login.ToLower();

            if (await _authRepository.AccountExists(registerParameter.Login, registerParameter.Email))
                return new RegisterResult(null, false, "Konto o podanych danych już istnieje.");

            var accountToCreate = new AccountDb
            {
                Login = registerParameter.Login,
                Email = registerParameter.Email,
                IsEmployee = registerParameter.IsEmployee
            };

            try
            {
                createdAccount = await _authRepository.Register(accountToCreate, registerParameter.Password);
            }
            catch (Exception)
            {
                _logger.Debug("[ERROR]: Problem while saving account to database.");
            }

            return new RegisterResult(createdAccount, true, "Registration was successful");
        }

        public async Task<AccountResetPasswordResult> ResetAccountPassword(ResetAccountPasswordParameter resetAccountPasswordParameter)
        {
            if (!await _authRepository.AccountExists(resetAccountPasswordParameter.AccountForResetPassword.Login, resetAccountPasswordParameter.AccountForResetPassword.Email))
                return new AccountResetPasswordResult(false, "Konto o podanych danych nie istnieje."); 

            try
            {
                var accountDb = await _authRepository.GetAccountByLoginOrEmail(resetAccountPasswordParameter.AccountForResetPassword.Login,
                    resetAccountPasswordParameter.AccountForResetPassword.Email);
                
                if(accountDb != null)
                    if (await _authRepository.SetNewPassword(accountDb, resetAccountPasswordParameter.AccountForResetPassword.NewPassword))
                        return new AccountResetPasswordResult(true, String.Empty);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new AccountResetPasswordResult(false, $"Nie udało się zmienić danych.");
        }

        public async Task<AccountResetEmailResult> ResetAccountEmail(ResetAccountEmailParameter resetAccountPasswordParameter)
        {
            if (!await _authRepository.AccountExists(email: resetAccountPasswordParameter.OldEmail))
                return new AccountResetEmailResult(false, "Konto o podanych danych nie istnieje.");

            try
            {
                var accountDb = await _authRepository.GetAccountByLoginOrEmail(email: resetAccountPasswordParameter.OldEmail);

                if (accountDb != null)
                    _mapper.Map(new AccountForResetEmail(resetAccountPasswordParameter.NewEmail), accountDb);
                
                if (await _accountRepository.SaveAll())
                    return new AccountResetEmailResult(true, String.Empty);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new AccountResetEmailResult(false, $"Nie udało się zmienić danych.");
        }

        public async Task<AccountResetLoginResult> ResetAccountLogin(ResetAccountLoginParameter resetAccountLoginParameter)
        {
            if (!await _authRepository.AccountExists(resetAccountLoginParameter.OldLogin))
                return new AccountResetLoginResult(false, "Konto o podanych danych nie istnieje.");

            try
            {
                var accountDb = await _authRepository.GetAccountByLoginOrEmail(resetAccountLoginParameter.OldLogin);

                if (accountDb != null)
                    _mapper.Map(new AccountForResetLogin(resetAccountLoginParameter.NewLogin), accountDb);
                
                if (await _accountRepository.SaveAll())
                    return new AccountResetLoginResult(true, String.Empty);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new AccountResetLoginResult(false, $"Nie udało się zmienić danych.");
        }

        public async Task<LoginResult> LoginAccount(LoginParameter loginParameter)
        {
            var userFromRepo =
                await _authRepository.Login(loginParameter.Login.ToLower(), loginParameter.Password);

            if (userFromRepo == null)
                return new LoginResult(null, false, "User not found.");

            var userPermissions = await _accountRepository.GetPermissionForAccount(userFromRepo.Id);
            var userPermissionsNames = string.Join(", ", userPermissions.Select(u => u.Name));

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.Login),
                new Claim(ClaimTypes.Role, userPermissionsNames)
            };

            var key = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(30),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new LoginResult(token, true, "Successful logged in.");
        }
    }
}
