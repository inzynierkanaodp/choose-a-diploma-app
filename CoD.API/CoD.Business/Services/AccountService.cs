﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.DbModels;
using CoD.WebApi.Enums;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;
using Microsoft.Extensions.Configuration;

namespace CoD.Business.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IAccountStudyInfoRepository _accountStudyInfoRepository;
        private readonly IAccountsGroupsRepository _accountsGroupsRepository;
        private readonly IHelpRepository _helpRepository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public AccountService(ILogger logger, IAccountRepository accountRepository, 
            IAccountStudyInfoRepository accountStudyInfoRepository, IAccountsGroupsRepository accountsGroupsRepository, IMapper mapper, IHelpRepository helpRepository)
        {
            _accountRepository = accountRepository;
            _accountStudyInfoRepository = accountStudyInfoRepository;
            _mapper = mapper;
            _helpRepository = helpRepository;
            _accountsGroupsRepository = accountsGroupsRepository;
            _logger = logger;
        }

        public async Task<IsActiveAccountResult> IsActiveAccount(IsActiveParameter isActiveParameter)
        {
            bool isUserActive = false;

            try
            {
                isUserActive = await _accountRepository.GetIsAccountActive(isActiveParameter.Id);
            }
            catch (Exception)
            {
                _logger.Debug("[ERROR]: Problem while getting info about account permission.");

                return new IsActiveAccountResult(isUserActive, false, "Problem while getting info about account permission.");
            }

            return new IsActiveAccountResult(isUserActive, true, "Problem while getting info about account permission.");
        }

        public async Task<GetAccountResult> GetAccount(GetAccountParameter getAccountParameter)
        {
            AccountForUpdate account;

            try
            {
                var accountDb = await _accountRepository.GetAccount(getAccountParameter.Id);
                account = _mapper.Map<AccountForUpdate>(accountDb);

                if (await _accountsGroupsRepository.GetAccountGroupByAccountId(getAccountParameter.Id) == null)
                    account.CanCreateGroup = true;
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetAccountResult(account, true, String.Empty);
        }

        public async Task<GetAccountLoginDetailsResult> GetAccountLoginDetails(GetAccountParameter getAccountParameter)
        {
            AccountLoginDetails accountLoginDetails;

            try
            {
                var accountDb = await _accountRepository.GetAccount(getAccountParameter.Id);
                accountLoginDetails = _mapper.Map<AccountLoginDetails>(accountDb);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetAccountLoginDetailsResult(accountLoginDetails, true, String.Empty);
        }

        public async Task<UpdateAccountResult> UpdateAccount(UpdateAccountParameter updateAccountParameter)
        {
            if (updateAccountParameter.AccountId != int.Parse(updateAccountParameter.User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return new UpdateAccountResult(false, "Unauthorized");

            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var accountStudyInfoDbs = new List<AccountStudyInfoDb>();

                    foreach (var studyFieldInfo in updateAccountParameter.StudyFieldWithFacultyInfos)
                    {
                        foreach (var issueSemesterInfo in updateAccountParameter.IssueSemesterInfos)
                        {
                            var accountStudyInfoDb = new AccountStudyInfoDb
                            {
                                IdAccount = updateAccountParameter.AccountId,
                                IdSemester = issueSemesterInfo.Id,
                                IdStudyField = studyFieldInfo.Id
                            };

                            accountStudyInfoDbs.Add(accountStudyInfoDb);
                        }
                    }

                    await _accountStudyInfoRepository.SaveAccountStudyInfos(accountStudyInfoDbs);

                    var accountDb = await _accountRepository.GetAccount(updateAccountParameter.AccountId);
                    updateAccountParameter.AccountForUpdate.Id = accountDb.Id;
                    _mapper.Map(updateAccountParameter.AccountForUpdate, accountDb);

                    scope.Complete();
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            if (await _accountRepository.SaveAll())
                return new UpdateAccountResult(true, String.Empty);

            return new UpdateAccountResult(false, $"Updating user {updateAccountParameter.AccountId} failed.");
        }

        public async Task<AccountForResetPasswordResult> ActiveAccount(IsActiveParameter isActiveParameter)
        {
            try
            {
                var ticket = await _helpRepository.GetTicket(isActiveParameter.Id);
                var accountDb = await _accountRepository.ActiveAccount(ticket.IdAccount);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new AccountForResetPasswordResult(true, String.Empty);
        }
    }
}