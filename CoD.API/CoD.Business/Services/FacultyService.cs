﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extras.NLog;
using AutoMapper;
using CoD.Business.Interfaces.Classes.Parameters;
using CoD.Business.Interfaces.Classes.Results;
using CoD.Business.Interfaces.Services;
using CoD.WebApi.Data.Interfaces;
using CoD.WebApi.Models;
using CoD.WebApi.ModelsForDto;

namespace CoD.Business.Services
{
    public class FacultyService : IFacultyService
    {
        private readonly IFacultyRepository _repository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public FacultyService(ILogger logger, IFacultyRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<GetFacultiesResult> GetFaculties()
        {
            List<FacultyInfo> faculties;
            try
            {
                var facultyFromRepo = await _repository.GetFaculties();
                faculties = _mapper.Map<List<FacultyInfo>>(facultyFromRepo);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return new GetFacultiesResult(faculties, true, String.Empty);
        }
    }
}